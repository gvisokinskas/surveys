import React from 'react';
import { connect } from 'react-redux';
import R from 'ramda';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';

import Header from '../components/generic/header/header';
import Survey from '../components/survey/survey';

import { fetchSurvey } from '../redux/actions/surveys';

const mapDispatchToProps = (dispatch, { params }) => ({
  getSurvey: () => dispatch(fetchSurvey(params.code)),
});

const mapStateToProps = (state, { params }) => ({
  survey: !!state.surveys.length ? R.find(R.propEq('code', params.code), state.surveys) : {},
  currentUser: state.app.currentUser,
});

class AnswerSurveyPage extends React.Component {

  componentWillMount() {
    this.props.getSurvey();
  }

  render() {
    return (<App>
      <Header />
      <Box style={{ height: '100%' }} colorIndex="light-2" align="center" justify="center">
        <div style={{ minHeight: 600, padding: '5%' }}>
          {!R.isEmpty(this.props.survey)
            && <Survey
              survey={this.props.survey}
              currentUser={this.props.currentUser} />}
        </div>
      </Box>
    </App>);
  }
}

AnswerSurveyPage.propTypes = {
  getSurvey: React.PropTypes.func.isRequired,
  survey: React.PropTypes.object.isRequired,
  currentUser: React.PropTypes.object,
};

AnswerSurveyPage.defaultProps = {
  currentUser: null,
};

export default connect(mapStateToProps, mapDispatchToProps)(AnswerSurveyPage);
