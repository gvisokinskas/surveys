import React from 'react';
import { connect } from 'react-redux';
import R from 'ramda';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';
import Tab from 'grommet/components/Tab';
import Tabs from 'grommet/components/Tabs';
import Title from 'grommet/components/Title';

import Header from '../components/generic/header/header';
import UserTable from '../components/user-list/user-table';
import EmailTable from '../components/user-list/allowed-emails/email-table';
import UserActions from '../components/user-list/user-actions/user-actions';

import { fetchUsers } from '../redux/actions/users';
import { fetchAllowedEmails } from '../redux/actions/allowed-emails';

const mapStateToProps = state => ({
  users: state.users.length
    ? state.users.filter(user => user.username !== state.app.currentUser.sub)
    : [],
  allowedEmails: state.allowedEmails,
  usersForAllowedEmails: state.users && R.values(state.users),
});

const mapDispatchToProps = dispatch => ({
  fetchUsers: () => dispatch(fetchUsers()),
  fetchAllowedEmails: () => dispatch(fetchAllowedEmails()),
});

class Users extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
    };

    this.changeActiveTab = this.changeActiveTab.bind(this);
  }

  componentWillMount() {
    this.props.fetchUsers();
    this.props.fetchAllowedEmails();
  }

  changeActiveTab(index) {
    this.setState({ activeTab: index });
  }

  render() {
    const noUsers = (<Box align="center" pad="medium">
      <Title>You are here alone</Title>
      <img src="/statics/no-users.png" alt="No users" />
      <div>Soon you will be able to add more</div>
    </Box>);

    const usersTab = this.props.users.length
      ? (<UserTable
        users={this.props.users}
        allowedEmails={this.props.allowedEmails} />)
      : noUsers;

    const emailsTab = (<EmailTable
      allowedEmails={this.props.allowedEmails}
      users={this.props.usersForAllowedEmails} />);

    return (<App>
      <Header />
      <Tabs
        activeIndex={this.state.activeTab}
        justify="start"
        onActive={this.changeActiveTab}
        style={{ paddingLeft: 20 }}>
        <Tab title="Users">
          <UserActions />
          <Box full colorIndex="light-2" pad="medium">
            {usersTab}
          </Box>
        </Tab>
        <Tab title="Emails">
          <Box full colorIndex="light-2" pad="medium">
            {emailsTab}
          </Box>
        </Tab>
      </Tabs>
    </App>);
  }
}

Users.propTypes = {
  allowedEmails: React.PropTypes.array.isRequired,
  users: React.PropTypes.array.isRequired,
  fetchUsers: React.PropTypes.func.isRequired,
  fetchAllowedEmails: React.PropTypes.func.isRequired,
  usersForAllowedEmails: React.PropTypes.array.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
