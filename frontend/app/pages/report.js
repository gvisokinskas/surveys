import React from 'react';
import { connect } from 'react-redux';
import R from 'ramda';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';

import Header from '../components/generic/header/header';
import ReportTable from '../components/reports/report-table';

import { getSurveyReport } from '../redux/actions/reports';
import { fetchSurvey } from '../redux/actions/surveys';

const mapDispatchToProps = (dispatch, { params }) => ({
  getReport: () => dispatch(getSurveyReport(params.surveyCode)),
  getSurvey: () => dispatch(fetchSurvey(params.surveyCode)),
});

const mapStateToProps = (state, { params }) => ({
  report: state.reports[params.surveyCode],
  survey: !!state.surveys.length ? R.find(R.propEq('code', params.surveyCode), state.surveys) : {},
  currentUser: state.app.currentUser,
});

class ReportPage extends React.Component {

  componentWillMount() {
    R.isEmpty(this.props.report) && this.props.getReport();
    R.isEmpty(this.props.survey) && this.props.getSurvey();
  }

  render() {
    return (<App>
      <Header />
      <Box full colorIndex="light-2">
        {!R.isEmpty(this.props.report)
          && !R.isEmpty(this.props.survey)
          && <ReportTable
            report={this.props.report}
            survey={this.props.survey}
            currentUser={this.props.currentUser} />}
      </Box>
    </App>);
  }
}

ReportPage.propTypes = {
  getReport: React.PropTypes.func.isRequired,
  getSurvey: React.PropTypes.func.isRequired,
  report: React.PropTypes.object,
  survey: React.PropTypes.object,
  currentUser: React.PropTypes.object.isRequired,
};

ReportPage.defaultProps = {
  report: {},
  survey: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportPage);
