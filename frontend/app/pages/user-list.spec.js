import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import Users from './user-list';

describe('<UsersPage />', () => {
  xit('should show Header and UserList component', () => {
    const component = shallow(<Users />);
    expect(component.find('Header').length).to.equal(1);
    expect(component.find('UserList').length).to.equal(1);
  })
});
