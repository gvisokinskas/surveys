import React from 'react';
import { connect } from 'react-redux';
import R from 'ramda';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';

import Header from '../components/generic/header/header';
import Survey from '../components/survey/survey';

import { fetchSurveyByAnswer } from '../redux/actions/surveys';
import { fetchAnswer } from '../redux/actions/answers';

const mapDispatchToProps = (dispatch, { params }) => ({
  getSurvey: () => dispatch(fetchSurveyByAnswer(params.answerCode)),
  getSavedAnswer: () => dispatch(fetchAnswer(params.answerCode)),
});

const mapStateToProps = (state, { params }) => ({
  survey: !!state.surveys.length ? R.find(R.propEq('code', params.surveyCode), state.surveys) : {},
  answer: state.savedAnswers[params.answerCode],
});
class SavedAnswerPage extends React.Component {

  componentWillMount() {
    this.props.getSurvey();
    this.props.getSavedAnswer();
  }

  render() {
    return (<App>
      <Header />
      <Box full colorIndex="light-2" align="center" justify="center">
        {!R.isEmpty(this.props.survey)
          && <Survey survey={this.props.survey} editMode answer={this.props.answer} />}
      </Box>
    </App>);
  }
}

SavedAnswerPage.propTypes = {
  getSurvey: React.PropTypes.func.isRequired,
  getSavedAnswer: React.PropTypes.func.isRequired,
  survey: React.PropTypes.object,
  answer: React.PropTypes.object,
};

SavedAnswerPage.defaultProps = {
  survey: {},
  answer: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(SavedAnswerPage);
