import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';
import Tab from 'grommet/components/Tab';
import Tabs from 'grommet/components/Tabs';

import Header from '../components/generic/header/header';
import SurveyTable from '../components/surveys/survey-table';
import CreateSurveyForm from '../components/surveys/creation/create-survey-form';
import ImportSurvey from '../components/surveys/import-survey';
import UploadMessage from '../components/surveys/upload-message';

import { fetchSurveys } from '../redux/actions/surveys';
import { isCurrentUserAdmin } from '../utils/user-check';

const mapDispatchToProps = dispatch => ({
  getAllSurveys: () => dispatch(fetchSurveys()),
});

const mapStateToProps = state => ({
  surveys: state.surveys.filter(survey => survey.username !== state.app.currentUser.sub),
  filteredSurveys: state.surveys.filter(survey => survey.username === state.app.currentUser.sub),
  currentUser: state.app.currentUser,
  isAdmin: isCurrentUserAdmin(state.app.currentUser),
});

class SurveysPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
    };

    this.changeActiveTab = this.changeActiveTab.bind(this);
  }

  componentWillMount() {
    this.props.getAllSurveys();
  }

  changeActiveTab(index) {
    this.setState({ activeTab: index });
  }

  render() {
    return (<App>
      <Header />
      <Tabs
        activeIndex={this.state.activeTab}
        justify="start"
        onActive={this.changeActiveTab}
        style={{ paddingLeft: 20 }} >
        <Tab title={this.props.isAdmin ? 'All Surveys' : 'Public Surveys'}>
          <Box full colorIndex="light-2" pad="medium">
            {this.props.surveys.length > 0
              && this.props.currentUser
              && <SurveyTable
                surveys={this.props.surveys}
                currentUser={this.props.currentUser} key="1" />}
          </Box>
        </Tab>
        <Tab title="My Surveys">
          <Box full colorIndex="light-2" pad="medium">
            {this.props.filteredSurveys.length > 0
            && this.props.currentUser
            && <SurveyTable
              surveys={this.props.filteredSurveys}
              currentUser={this.props.currentUser} key="2" />}
          </Box>
        </Tab>
        <Tab title="Create Survey">
          <Box full colorIndex="light-2" pad="medium">
            {this.props.currentUser
            && <CreateSurveyForm currentUser={this.props.currentUser} />}
          </Box>
        </Tab>
        <Tab title="Import Survey">
          <Box full colorIndex="light-2" pad="medium">
            {this.props.currentUser
            && <ImportSurvey />}
          </Box>
        </Tab>
      </Tabs>
      <UploadMessage />
    </App>);
  }
}

SurveysPage.propTypes = {
  currentUser: PropTypes.object.isRequired,
  getAllSurveys: PropTypes.func.isRequired,
  surveys: PropTypes.array,
  filteredSurveys: PropTypes.array,
  isAdmin: PropTypes.bool.isRequired,
};

SurveysPage.defaultProps = {
  surveys: [],
  filteredSurveys: [],
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveysPage);
