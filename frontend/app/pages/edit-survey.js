import React from 'react';
import { connect } from 'react-redux';
import R from 'ramda';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';

import Header from '../components/generic/header/header';
import CreateSurveyForm from '../components/surveys/creation/create-survey-form';

import { fetchSurvey } from '../redux/actions/surveys';

const mapDispatchToProps = (dispatch, { params }) => ({
  getSurvey: () => dispatch(fetchSurvey(params.code)),
});

const mapStateToProps = (state, { params }) => ({
  survey: !!state.surveys.length ? R.find(R.propEq('code', params.code), state.surveys) : {},
  currentUser: state.app.currentUser,
});

class AnswerSurveyPage extends React.Component {

  componentWillMount() {
    this.props.getSurvey();
  }

  render() {
    return (<App>
      <Header />
      <Box full colorIndex="light-2" pad="medium">
        {!R.isEmpty(this.props.survey) &&
          <CreateSurveyForm
            survey={this.props.survey}
            currentUser={this.props.currentUser}
            editMode />}
      </Box>
    </App>);
  }
}

AnswerSurveyPage.propTypes = {
  getSurvey: React.PropTypes.func.isRequired,
  survey: React.PropTypes.object.isRequired,
  currentUser: React.PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(AnswerSurveyPage);
