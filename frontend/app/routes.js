import React from 'react';
import { Router, Route, Redirect, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { initAuth, refreshToken } from './redux/actions/app';
import Login from './pages/login';
import UserList from './pages/user-list';
import Surveys from './pages/surveys';
import AnswerSurvey from './pages/answer-survey';
import SavedSurvey from './pages/saved-survey';
import Report from './pages/report';
import EditSurvey from './pages/edit-survey';
import requireAuthentication from './auth-route';

const mapDispatchToProps = dispatch => ({
  initAuth: () => dispatch(initAuth()),
  refreshToken: () => dispatch(refreshToken()),
});

const mapStateToProps = (state) => {
  console.log(state);
  return {};
};

class Routes extends React.Component {
  componentWillMount() {
    this.props.initAuth();
    window.setInterval(() => this.props.refreshToken(), 1000 * 60 * 60 * 24);
  }

  render() {
    return (
      <Router history={browserHistory}>
        <div>
          <Route path="/login" component={Login} />
          <Route path="/user-list" component={requireAuthentication(UserList)} />
          <Route path="/surveys" component={requireAuthentication(Surveys)} />
          <Route path="/survey/:code" component={AnswerSurvey} />
          <Route path="/survey/:surveyCode/:answerCode" component={SavedSurvey} />
          <Route path="/surveys/edit/:code" component={requireAuthentication(EditSurvey)} />
          <Route path="/report/:surveyCode" component={requireAuthentication(Report)} />
          <Redirect from="*" to="/login" />
        </div>
      </Router>
    );
  }
}

Routes.propTypes = {
  initAuth: React.PropTypes.func.isRequired,
  refreshToken: React.PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
