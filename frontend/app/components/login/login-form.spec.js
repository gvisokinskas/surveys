import React from "react";
import { shallow } from "enzyme";
import { expect } from "chai";

import { PureLoginForm } from './login-form';

describe('<LoginForm />', () => {
  xit('should show LoginForm', () => {
    const mockProps = {
      login: v => v,
    };
    const component = shallow(<PureLoginForm {...mockProps} />);
    expect(component.find('LoginForm').length).to.equal(1);
    expect(component.find('Button').length).to.equal(1);
    expect(component.find('Button').props().label).to.equal('Register');
  })
});
