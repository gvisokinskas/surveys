import React from 'react';
import FontAwesome from 'react-fontawesome';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import App from 'grommet/components/App';
import Box from 'grommet/components/Box';
import LoginForm from 'grommet/components/LoginForm';
import Button from 'grommet/components/Button';
import Layer from 'grommet/components/Layer';
import Toast from 'grommet/components/Toast';

import { loginUser } from '../../redux/actions/app';
import RegistrationForm from '../user-list/user-actions/registration-form/form';

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(loginUser(data)),
});

const mapStateToProps = state => ({
  loginFailed: state.app.loginFailed,
  hideRegistrationModal: state.app.hideRegistrationModal,
});

const logo = (<FontAwesome
  name="question"
  size="5x"
  spin
/>);

const userPlusIcon = <FontAwesome name="user-plus" />;

class PureLoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      showRegistrationToast: false,
    };
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  componentWillMount() {
    if (window.localStorage.token) {
      browserHistory.push('surveys');
    }
  }

  componentWillReceiveProps(nextProps) {
    const { hideRegistrationModal } = nextProps;
    if (hideRegistrationModal) {
      this.setState({
        showRegistrationToast: true,
      });

      setTimeout(() => {
        this.setState({
          showRegistrationToast: false,
        });
      }, 3000);
    }
  }

  closeModal() {
    this.setState({
      modalVisible: false,
    });
    if (this.form) this.form.getWrappedInstance().resetForm();
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  errorMessage() {
    if (this.props.loginFailed) {
      return 'The username or password you have entered is invalid';
    }

    return '';
  }

  render() {
    const modal = (<div>
      <RegistrationForm
        ref={(form) => { this.form = form; }}
        closeModal={this.closeModal}
        checkEmails={false}
      />
    </div>);

    return (<App centered={false}>
      <Box align="center" full justify="center" colorIndex="light-2">
        <LoginForm
          logo={logo}
          title="Surveys"
          secondaryText="Please enter your login details below"
          align="center"
          usernameType="text"
          onSubmit={this.props.login}
          errors={[this.errorMessage()]}
        />
        <Button
          icon={userPlusIcon}
          label="Register"
          href="#"
          primary
          onClick={this.openModal}
        />
        <Layer
          onClose={this.closeModal}
          hidden={!this.state.modalVisible}
          closer>
          {modal}
        </Layer>
        {this.state.showRegistrationToast && <Toast status="ok">
          Registration completed successfully.
        </Toast>}
      </Box>
    </App>);
  }
}

PureLoginForm.propTypes = {
  login: React.PropTypes.func.isRequired,
  loginFailed: React.PropTypes.bool,
  hideRegistrationModal: React.PropTypes.bool,
};

PureLoginForm.defaultProps = {
  loginFailed: false,
  hideRegistrationModal: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(PureLoginForm);
