import React, { PropTypes } from 'react';
import { Field } from 'redux-form';

import TextField from '../../form-fields/text-field';
import RangeField from '../../form-fields/range-field';
import SelectionField from '../../form-fields/selection-field';
import MultipleSelectionField from '../../form-fields/multiple-selection-field';

import { QUESTION_TYPE } from '../../../utils/constants';

const Question = ({ question, index, startIndex }) => {
  switch (question.questionType) {
    case QUESTION_TYPE.TEXT:
      return (<Field
        name={`answers[${index + startIndex}].value`}
        component={TextField}
        label={question.label} />);
    case QUESTION_TYPE.RANGE:
      return (<Field
        name={`answers[${index + startIndex}].value`}
        component={RangeField}
        label={question.label}
        min={question.rangeStart}
        max={question.rangeEnd} />);
    case QUESTION_TYPE.SELECTION:
      if (question.multiple) {
        return (<Field
          name={`answers[${index + startIndex}].value`}
          component={MultipleSelectionField}
          index={index}
          label={question.label}
          options={question.options} />);
      }
      return (<Field
        name={`answers[${index + startIndex}].value`}
        component={SelectionField}
        label={question.label}
        options={question.options} />);
    default: return null;
  }
};

Question.propTypes = {
  question: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  startIndex: PropTypes.number.isRequired,
};

export default Question;
