import React, { PropTypes } from 'react';
import R from 'ramda';
import Label from 'grommet/components/Label';
import Question from '../question/question';

const showField = (formValues, question, change) => {
  let show = true;
  formValues && question.conditions.forEach((condition) => {
    const answer = R.find(R.propEq('questionCode', condition.questionCode), formValues.answers).value;
    if (!answer || (condition.valueCode !== '' && !(condition.valueCode === answer || answer.includes(condition.valueCode)))) {
      show = false;
      const index = R.findIndex(R.propEq('questionCode', question.code), formValues.answers);
      change(`answers[${index}].value`, '');
    }
  });
  return show;
};

const Page = ({ page, startIndex, formValues, change }) => (<div>
  <Label>{page.label}</Label>
  {page.questions.map((question, index) =>
    <div key={`${question.code}-div`}style={{ display: showField(formValues, question, change) ? '' : 'none' }}>
      <Question
        key={question.code}
        question={question}
        index={index}
        startIndex={startIndex} />
    </div>)}
</div>);

Page.propTypes = {
  page: PropTypes.object.isRequired,
  startIndex: PropTypes.number.isRequired,
  formValues: PropTypes.object,
  change: PropTypes.func.isRequired,
};

Page.defaultProps = {
  formValues: null,
};

export default Page;
