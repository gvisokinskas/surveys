import React, { PropTypes, Component } from 'react';
import { reduxForm, getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import R from 'ramda';
import uuid from 'uuid';
import { browserHistory } from 'react-router';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Form from 'grommet/components/Form';
import FormFields from 'grommet/components/FormFields';
import Footer from 'grommet/components/Footer';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import NextIcon from 'grommet/components/icons/base/Next';
import PreviousIcon from 'grommet/components/icons/base/Previous';
import Meter from 'grommet/components/Meter';
import Label from 'grommet/components/Label';

import { URL } from '../../utils/constants';
import { answerSurvey } from '../../redux/actions/answers';
import Page from './page/page';
import Status from '../generic/status/status';

const validate = (values) => {
  const errors = {};
  errors.answers = [];

  for (let i = 0; i < values.answers.length; i++) {
    if (values.answers[i].required && (!values.answers[i].value.length ||
      !values.answers[i].value[0].trim())) {
      errors.answers[i] = ({ value: 'Required' });
    }
  }
  return errors;
};

const mapStateToProps = (state, { survey, answer, editMode }) => {
  const merge = (obj1, obj2) => R.mergeWith(R.concat, obj1, obj2);
  const questions = survey.pages.reduce(merge).questions;
  let answers;


  let initialValues;
  if (editMode) {
    answers = questions.map((question, index) => {
      let foundAnswer = R.find(R.propEq('questionCode', question.code), answer.answers);
      if (foundAnswer) {
        foundAnswer = foundAnswer.value;

        if (!question.multiple) {
          foundAnswer = foundAnswer[0];
        }
      }

      return {
        questionCode: question.code,
        value: foundAnswer || '',
        transform: !(question.questionType === 'SELECTION' && question.multiple),
        required: question.required,
        touched: !!foundAnswer,
      };
    });
    initialValues = {
      code: answer.code,
      surveyCode: answer.surveyCode,
      answers,
    };
  } else {
    answers = questions.map(question => ({
      questionCode: question.code,
      value: '',
      transform: !(question.questionType === 'SELECTION' && question.multiple),
      required: question.required,
    }));
    initialValues = {
      code: uuid.v4(),
      answers,
    };
  }

  const formValues = getFormValues('survey')(state);
  return {
    initialValues,
    formValues,
  };
};

const mapDispatchToProps = (dispatch, { survey }) => ({
  postAnswer: (data, finished) => dispatch(answerSurvey(data, survey.code, finished)),
});

class Survey extends Component {

  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.state = {
      page: 0,
      saved: false,
      answerId: '',
      error: false,
      success: false,
    };
  }

  componentWillMount() {
    if (this.props.currentUser) {
      if (R.keys(this.props.survey.answersByUser).includes(this.props.currentUser.sub)) {
        this.setState({ error: true });
      }

      if (!this.props.survey.public && this.props.survey.username === this.props.currentUser.sub) {
        this.setState({ error: true });
      }
    }
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  render() {
    const { handleSubmit, postAnswer, survey, formValues, submitFailed, change } = this.props;
    const { page, saved, error, success } = this.state;

    const submitForm = () => {
      handleSubmit(formData => postAnswer(formData, true))()
      .then(v => this.setState({ success: true }))
      .catch(v => this.setState({ error: true }));
    };

    const saveForm = () => {
      postAnswer(formValues, false)
        .then(v => this.setState({ saved: true, answerId: v }))
        .catch(v => this.setState({ error: true }));
    };
    const next = <NextIcon />;
    const previous = <PreviousIcon />;

    const form = (<Box direction="row" align="center">
      <Box pad="large">
        <Button
          id="previous"
          icon={previous}
          onClick={this.previousPage}
          style={{ visibility: this.state.page === 0 ? 'hidden' : 'visible' }} />
      </Box>
      <Form>
        <Header>
          <Heading>{survey.label}</Heading>
        </Header>
        <FormFields>
          {survey.pages.map((p, index) =>
            <div key={`${index}-div`}style={{ display: index === this.state.page ? '' : 'none' }}>
              <Page
                key={survey.pages[index].code}
                page={survey.pages[index]}
                change={change}
                startIndex={index === 0 ? 0 : survey.pages[index - 1].questions.length}
                formValues={formValues} />
            </div>)}
        </FormFields>
        <Footer pad={{ vertical: 'medium' }} justify="center">
          <Box direction="column">
            {survey.pages.length > 1 && <Meter
              max={survey.pages.length}
              value={page + 1}
              size="medium" />}
            {submitFailed &&
            <Label style={{ color: '#FF324D' }}>
              There are validation errors!
            </Label>}
            <Box direction="row" pad={{ between: 'medium' }} justify="between">
              <Button
                label="Submit"
                primary
                onClick={submitForm} />
              <Button
                label="Save"
                onClick={saveForm} />
            </Box>
          </Box>
        </Footer>
      </Form>
      <Box pad="large">
        <Button
          icon={next}
          onClick={this.nextPage}
          style={{ visibility: survey.pages.length !== this.state.page + 1 ? 'visible' : 'hidden' }} />
      </Box>
    </Box>);

    const link = (<Status
      message="Your answers were saved successfully. You can continue on this page"
      secondaryMessage={`${URL}/survey/${survey.code}/${this.state.answerId}`}
      actionName="Copy to clipboard"
      action={() => window.prompt('Copy to clipboard: Ctrl+C, Enter',
      `${URL}/survey/${survey.code}/${this.state.answerId}`)}
    />);

    const goBack = (<Status
      message="Your answers were submitted successfully"
      actionName="Go to main page"
      action={() => browserHistory.push('./surveys')}
    />);

    const errorBlock = (<Status
      message="OOPS! something went terribly wrong! Maybe you have already answered this survey?"
      actionName="Go to main page"
      action={() => browserHistory.push('./surveys')}
    />);

    return (<Box>
      {!error && !success && !saved && form}
      {saved && link}
      {error && errorBlock}
      {success && goBack}
    </Box>);
  }
}

Survey.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  postAnswer: PropTypes.func.isRequired,
  survey: PropTypes.object.isRequired,
  formValues: PropTypes.object,
  currentUser: PropTypes.object,
  submitFailed: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
};

Survey.defaultProps = {
  formValues: null,
  answer: null,
  currentUser: null,
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'survey',
  validate,
})(Survey));
