import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import CheckBox from 'grommet/components/CheckBox';

const noBorderContainer = {
  background: 'transparent',
  border: 'none',
  overflow: 'hidden',
};

const overflowHidden = {
  overflow: 'hidden',
};

const CheckField = ({ input, meta, label, noBorder }) => (<FormField
  error={meta.touched && !!meta.error && meta.error}
  style={noBorder ? noBorderContainer : overflowHidden}>
  <CheckBox
    label={label}
    toggle
    onChange={input.onChange}
    checked={input.value || false}
    {...input} />
</FormField>);

CheckField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  noBorder: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
};

export default CheckField;
