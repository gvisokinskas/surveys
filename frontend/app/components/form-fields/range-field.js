import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';

const RangeField = ({ label, min, max, input, meta }) => (<FormField
  label={`${label} (${input.value})`}
  error={meta.touched && !!meta.error && meta.error}>
  <TextInput
    type="range"
    min={min}
    max={max}
    onDOMChange={input.onChange}
    {...input} />
</FormField>);

RangeField.propTypes = {
  label: PropTypes.string.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default RangeField;
