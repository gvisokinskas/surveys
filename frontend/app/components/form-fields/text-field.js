import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';

const noBorderTextInput = {
  border: 'none',
  background: 'transparent',
};

const noBorderContainer = {
  background: 'transparent',
  border: 'none',
};

const bottomBorderContainer = {
  background: 'transparent',
  border: 'none',
  borderBottom: '1px solid rgba(0, 0, 0, 0.15)',
};

const TextField = ({ input, meta, label, noBorder, placeholder, bottomBorder }) => (<FormField
  label={label}
  error={meta.touched && !!meta.error && meta.error}
  style={noBorder ? noBorderContainer : (bottomBorder && bottomBorderContainer)}>
  <TextInput
    placeHolder={placeholder}
    onDOMChange={input.onChange}
    defaultValue={input.value}
    style={noBorder && noBorderTextInput} />
</FormField>);

TextField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  noBorder: PropTypes.bool,
  bottomBorder: PropTypes.bool,
  label: PropTypes.string,
  placeholder: PropTypes.string,
};

TextField.defaultProps = {
  label: '',
  placeholder: '',
  bottomBorder: null,
  noBorder: null,
};

export default TextField;
