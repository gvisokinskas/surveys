import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import RadioButton from 'grommet/components/RadioButton';

const SelectionField = ({ label, options, input, meta }) => (<FormField
  label={label}
  error={meta.touched && !!meta.error && meta.error}>
  {options.map(option => (
    <RadioButton
      key={option.code}
      id={option.code}
      name="name"
      label={option.value}
      checked={option.value === input.value}
      {...input}
      onChange={v => v && input.onChange(option.value)} />))}
</FormField>);

SelectionField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
};

export default SelectionField;
