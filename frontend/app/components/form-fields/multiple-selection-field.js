import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import CheckBox from 'grommet/components/CheckBox';

const SelectionField = ({ label, options, input, meta }) => {
  const changeValue = (code, currentArray) => {
    const newArray = currentArray.constructor === Array ? currentArray.slice(0) : [];
    if (newArray.includes(code)) {
      newArray.splice(newArray.indexOf(code), 1);
    } else {
      newArray.push(code);
    }
    input.onChange(newArray);
  };

  return (<FormField label={label} error={meta.touched && !!meta.error && meta.error}>
    {options.map(option => (
      <CheckBox
        key={option.code}
        id={option.code}
        name="name"
        label={option.value} {...input}
        checked={input.value ? input.value.includes(option.value) : false}
        onBlur={v => v}
        onChange={() => changeValue(option.value, input.value)} />))}
  </FormField>);
};

SelectionField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
};

export default SelectionField;
