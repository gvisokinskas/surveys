import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import NumberInput from 'grommet/components/NumberInput';

const NumberField = ({ input, meta, label, noBorder, placeholder, bottomBorder, min, max }) =>
  (<FormField
    label={label}
    style={{ margin: '5px 0' }}
    error={meta.touched && !!meta.error && meta.error}>
    <NumberInput
      placeholder={placeholder}
      onChange={input.onChange}
      min={min}
      max={max}
      {...input} />
  </FormField>);

NumberField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  noBorder: PropTypes.bool,
  bottomBorder: PropTypes.bool,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
};

NumberField.defaultProps = {
  noBorder: false,
  bottomBorder: false,
  placeholder: '',
  min: 0,
  max: 100000,
};

export default NumberField;
