import React, { PropTypes } from 'react';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Title from 'grommet/components/Title';

const Status = ({ message, action, actionName, secondaryMessage }) => (
  <Box pad="large" align="center" direction="column">
    <Box pad="large" justify="center">
      <Title style={{ whiteSpace: 'normal' }}>{message}</Title>
    </Box>
    {secondaryMessage && <Box>
      {secondaryMessage}
    </Box>}
    <Button
      primary
      label={actionName}
      onClick={action} />
  </Box>
);

Status.propTypes = {
  message: PropTypes.string.isRequired,
  secondaryMessage: PropTypes.string,
  action: PropTypes.func.isRequired,
  actionName: PropTypes.string.isRequired,
};

Status.defaultProps = {
  secondaryMessage: '',
};

export default Status;
