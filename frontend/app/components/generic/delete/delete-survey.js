import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Label from 'grommet/components/Label';
import FormCheckmarkIcon from 'grommet/components/icons/base/FormCheckmark';
import FormCloseIcon from 'grommet/components/icons/base/FormClose';

import { deleteSurvey } from '../../../redux/actions/surveys';

const mapDispatchToProps = (dispatch, { surveyCode, redirect }) => ({
  delete: () => dispatch(deleteSurvey(surveyCode, redirect)),
});

class DeleteSurvey extends Component {

  constructor(props) {
    super(props);
    this.state = {
      confirmVisible: false,
    };

    this.showConfirmation = this.showConfirmation.bind(this);
    this.hideConfirmation = this.hideConfirmation.bind(this);
    this.delete = this.delete.bind(this);
  }

  showConfirmation() {
    this.setState({ confirmVisible: true });
  }

  hideConfirmation() {
    this.setState({ confirmVisible: false });
  }

  delete() {
    this.setState({ confirmVisible: false });
    this.props.delete();
    this.props.afterDelete();
  }

  render() {
    const deleteButton = this.props.hidden
      ? <Label onClick={this.showConfirmation}>Delete</Label>
      : <Button label="Delete Survey" secondary onClick={this.showConfirmation} />;

    return (<div>
      {this.state.confirmVisible ? <Box direction={`${this.props.hidden ? 'column' : 'row'}`}>
        <Box align="center" pad="none">
          <Label>Confirm</Label>
        </Box>
        <Box direction="row" justify="center">
          <Button
            icon={<FormCheckmarkIcon colorIndex="ok" />}
            plain
            onClick={this.delete} />
          <Button
            icon={<FormCloseIcon colorIndex="critical" />}
            plain
            onClick={this.hideConfirmation} />
        </Box>
      </Box> : deleteButton}
    </div>);
  }
}

DeleteSurvey.propTypes = {
  delete: PropTypes.func.isRequired,
  hidden: PropTypes.bool,
  afterDelete: PropTypes.func,
};

DeleteSurvey.defaultProps = {
  redirect: false,
  hidden: false,
  afterDelete: () => (null),
};

export default connect(null, mapDispatchToProps)(DeleteSurvey);
