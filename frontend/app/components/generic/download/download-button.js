import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import Button from 'grommet/components/Button';
import Label from 'grommet/components/Label';
import DownloadIcon from 'grommet/components/icons/base/Download';

import { downloadSurvey } from '../../../redux/actions/excel';

const mapDispatchToProps = dispatch => ({
  download: data => dispatch(downloadSurvey(data)),
});

const DownloadButton = ({ download, survey, hidden }) => {
  const downloadButton = hidden
    ? <Label onClick={() => download(survey)}>Download Report</Label>
    : <Button onClick={() => download(survey)} icon={<DownloadIcon />}>Download Report</Button>;

  return (
    <div>
      {downloadButton}
    </div>
  );
};

DownloadButton.propTypes = {
  survey: PropTypes.object.isRequired,
  download: PropTypes.func.isRequired,
  hidden: PropTypes.bool,
};

DownloadButton.defaultProps = {
  hidden: false,
};

export default connect(null, mapDispatchToProps)(DownloadButton);
