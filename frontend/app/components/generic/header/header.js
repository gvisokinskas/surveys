import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import R from 'ramda';

import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import Box from 'grommet/components/Box';
import Menu from 'grommet/components/Menu';
import Anchor from 'grommet/components/Anchor';
import FontAwesome from 'react-fontawesome';

import { isCurrentUserAdmin } from '../../../utils/user-check';
import { logoutUser } from '../../../redux/actions/app';

const mapStateToProps = state => ({
  user: state.app.currentUser,
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutUser()),
});

const menuIcon = (<FontAwesome
  name="bars"
/>);

const logo = (<FontAwesome
  name="question"
  size="2x"
/>);

const menu = (user, logout) => (<Menu
  label={user.sub}
  icon={menuIcon}
  dropAlign={{ right: 'right' }}>
  {isCurrentUserAdmin(user) && <Anchor
    href="#"
    className="active"
    onClick={() => browserHistory.push('/user-list')}>
    User management
  </Anchor>}
  {isCurrentUserAdmin(user) && <Anchor
    href="#"
    className="active"
    onClick={() => browserHistory.push('/surveys')}>
    Surveys
  </Anchor>}
  <Anchor
    href="#"
    className="active"
    onClick={logout}>
    Logout
  </Anchor>
</Menu>);

const goToSurveys = () => browserHistory.push('/surveys');

export const MainHeader = props => (<Header size="large">
  <Box pad="small">
    <Title onClick={goToSurveys}>{logo}{props.title}</Title>
  </Box>
  <Box
    flex
    justify="end"
    direction="row"
    responsive>
    {props.user && menu(props.user, props.logout)}
  </Box>
</Header>);

MainHeader.propTypes = {
  user: React.PropTypes.object,
  logout: React.PropTypes.func.isRequired,
  title: React.PropTypes.string,
};

MainHeader.defaultProps = {
  title: 'Surveys',
  user: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(MainHeader);
