import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import Header from './header';

describe('<Header />', () => {
  xit('should render header component', () => {
    const component = shallow(<Header />);
    expect(component.find('Header').length).to.equal(1);
    expect(component.find('Title').length).to.equal(1);
    expect(component.find('Menu').length).to.equal(1);
    expect(component.find('Box').length).to.equal(2);
    expect(component.find('Anchor').length).to.equal(1);
  });
});
