import React from 'react';

import Anchor from 'grommet/components/Anchor';
import Button from 'grommet/components/Button';
import Layer from 'grommet/components/Layer';
import Menu from 'grommet/components/Menu';
import UserAddIcon from 'grommet/components/icons/base/UserAdd';

import RegistrationForm from './registration-form/form';

class UserActions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  closeModal() {
    this.setState({ modalVisible: false });
    if (this.form) this.form.getWrappedInstance().resetForm();
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  render() {
    const modal = (<div>
      <RegistrationForm
        ref={(form) => { this.form = form; }}
        closeModal={this.closeModal} checkEmails />
    </div>);

    return (<div>
      <Menu responsive inline direction="row" primary>
        <Anchor className="active">
          <Button icon={<UserAddIcon />} plain label="New User" onClick={this.openModal} />
        </Anchor>
      </Menu>
      <Layer
        onClose={this.closeModal}
        hidden={!this.state.modalVisible}
        closer>
        {modal}
      </Layer>
    </div>);
  }
}

export default UserActions;
