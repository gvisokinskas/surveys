import React from 'react';
import R from 'ramda';
import { connect } from 'react-redux';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Footer from 'grommet/components/Footer';
import Form from 'grommet/components/Form';
import FormField from 'grommet/components/FormField';
import FormFields from 'grommet/components/FormFields';
import FormCheckmarkIcon from 'grommet/components/icons/base/FormCheckmark';
import FormCloseIcon from 'grommet/components/icons/base/FormClose';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import TextInput from 'grommet/components/TextInput';

import { addAllowedEmail } from '../../../../redux/actions/allowed-emails';
import { registerUser } from '../../../../redux/actions/users';
import { clearRegistrationErrors } from '../../../../redux/actions/app';

const mapStateToProps = state => ({
  allowedEmails: state.allowedEmails,
  registrationErrors: state.app.registrationErrors,
  hideRegistrationModal: state.app.hideRegistrationModal,
});

const mapDispatchToProps = dispatch => ({
  registerNewEmail: data => dispatch(addAllowedEmail(data)),
  registerUser: data => dispatch(registerUser(data)),
  clearRegistrationErrors: () => dispatch(clearRegistrationErrors()),
});

const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class RegisterForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      confirmPassword: '',
      email: '',
      emailAllowed: true,
      passwordMatch: true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkAllowedEmail = this.checkAllowedEmail.bind(this);
    this.addAllowedEmail = this.addAllowedEmail.bind(this);
    this.clearEmail = this.clearEmail.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { hideRegistrationModal } = nextProps;

    if (hideRegistrationModal) {
      this.props.closeModal();
    }

    if (this.props.checkEmails && this.state.email.match(regex)) {
      this.setState({
        emailAllowed: this.state.email.length > 0 ? !!R.filter(R.propEq('value', this.state.email), R.values(nextProps.allowedEmails)).length : true,
      });
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    const passwordMatch = this.state.confirmPassword === this.state.password;
    this.setState({ passwordMatch });

    this.checkAllowedEmail();
    if (this.state.emailAllowed && passwordMatch) {
      this.props.registerUser({
        username: this.state.username,
        password: this.state.password,
        email: this.state.email,
      });
    } else {
      return;
    }

    event.preventDefault();
  }

  checkAllowedEmail() {
    if (this.props.checkEmails && this.state.email.match(regex)) {
      this.setState({ emailAllowed:
        this.state.email.length > 0 ? !!R.filter(R.propEq('value', this.state.email), R.values(this.props.allowedEmails)).length : true });
    }
  }

  addAllowedEmail() {
    this.props.registerNewEmail({ value: this.state.email });
    this.checkAllowedEmail();
  }

  clearEmail() {
    this.setState({ email: '' });
    this.checkAllowedEmail();
    this.setState({ emailAllowed: true });
  }

  resetForm() {
    this.setState({
      email: '',
      username: '',
      password: '',
      confirmPassword: '',
      passwordMatch: true,
    });
    this.props.clearRegistrationErrors();
  }

  render() {
    return (<Form>
      <Header style={{ marginTop: '15px' }}>
        <Heading>New User: {this.state.username}</Heading>
      </Header>
      <FormFields>
        <FormField label="username" error={this.props.registrationErrors.username}>
          <TextInput
            name="username"
            value={this.state.username}
            onDOMChange={this.handleChange} />
        </FormField>
        <FormField label="email" error={this.props.registrationErrors.email}>
          <TextInput
            name="email"
            value={this.state.email}
            onDOMChange={this.handleChange}
            onBlur={this.checkAllowedEmail} />
        </FormField>
        <FormField label="password" error={this.props.registrationErrors.password}>
          <TextInput
            type="password"
            name="password"
            value={this.state.password}
            onDOMChange={this.handleChange} />
        </FormField>
        <FormField label="confirm password" error={this.state.passwordMatch ? '' : 'passwords must match'}>
          <TextInput
            type="password"
            name="confirmPassword"
            value={this.state.confirmPassword}
            onDOMChange={this.handleChange} />
        </FormField>
      </FormFields>
      <Footer pad={{ vertical: 'medium' }}>
        <Button
          label="Submit"
          primary
          onClick={this.handleSubmit} />
        {!this.state.emailAllowed && <Box direction="row" pad="medium" align="center">
          <div>
            Email is not allowed. Do you want to add it?
          </div>
          <div>
            <Button
              icon={<FormCheckmarkIcon colorIndex="ok" />}
              href="#"
              plain
              label="Add"
              onClick={this.addAllowedEmail} />
            <Button
              icon={<FormCloseIcon colorIndex="critical" />}
              href="#"
              plain
              label="Clear"
              onClick={this.clearEmail} />
          </div>
        </Box>}
      </Footer>
    </Form>
    );
  }
}

RegisterForm.propTypes = {
  closeModal: React.PropTypes.func.isRequired,
  checkEmails: React.PropTypes.bool.isRequired,
  registerNewEmail: React.PropTypes.func.isRequired,
  registerUser: React.PropTypes.func.isRequired,
  allowedEmails: React.PropTypes.array.isRequired,
  clearRegistrationErrors: React.PropTypes.func.isRequired,
  registrationErrors: React.PropTypes.object,
  hideRegistrationModal: React.PropTypes.bool,
};

RegisterForm.defaultProps = {
  registrationErrors: {},
  hideRegistrationModal: false,
};

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(RegisterForm);
