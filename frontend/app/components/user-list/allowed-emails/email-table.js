import React from 'react';
import R from 'ramda';
import { connect } from 'react-redux';

import Button from 'grommet/components/Button';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import TextInput from 'grommet/components/TextInput';
import AddIcon from 'grommet/components/icons/base/Add';

import EmailRow from './email-row/email-row';

import { addAllowedEmail } from '../../../redux/actions/allowed-emails';

const inputStyle = {
  border: 'none',
  paddingLeft: 0,
};

const errorStyle = {
  paddingLeft: 0,
  border: 'red',
  color: 'red',
};

const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const mapDispatchToProps = dispatch => ({
  addEmail: email => dispatch(addAllowedEmail(email)),
});

class EmailTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      newEmail: '',
      error: false,
    };
    this.changeNewEmailValue = this.changeNewEmailValue.bind(this);
    this.addEmail = this.addEmail.bind(this);
  }

  changeNewEmailValue(event) {
    this.setState({ newEmail: event.target.value });
  }

  addEmail() {
    if (this.state.newEmail.match(regex) &&
      !R.contains(this.state.newEmail, this.props.allowedEmails.map(e => e.value))) {
      this.props.addEmail({ value: this.state.newEmail });
      this.setState({ newEmail: '' });
    } else {
      this.setState({ error: true });
      setTimeout(() => {
        this.setState({ error: false });
        this.setState({ newEmail: '' });
      }, 300);
    }
  }

  render() {
    const { allowedEmails, users } = this.props;

    const isActive = email => !!R.filter(R.propEq('email', email), users).length;

    return (<List>
      <ListItem>
        <TextInput
          placeHolder="Add new email"
          style={this.state.error ? errorStyle : inputStyle}
          value={this.state.newEmail}
          onDOMChange={this.changeNewEmailValue} />
        <Button
          icon={<AddIcon />}
          plain
          label="Add"
          onClick={this.addEmail} />
      </ListItem>
      {allowedEmails.map(email =>
        <EmailRow key={email.value} email={email} active={isActive(email.value)} />)}
    </List>);
  }
}

EmailTable.propTypes = {
  users: React.PropTypes.array.isRequired,
  allowedEmails: React.PropTypes.array.isRequired,
  addEmail: React.PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(EmailTable);
