import React from 'react';

import Button from 'grommet/components/Button';
import ListItem from 'grommet/components/ListItem';
import FormTrashIcon from 'grommet/components/icons/base/FormTrash';
import FormCheckmarkIcon from 'grommet/components/icons/base/FormCheckmark';
import FormCloseIcon from 'grommet/components/icons/base/FormClose';
import { connect } from 'react-redux';

import { removeEmail } from '../../../../redux/actions/allowed-emails';

const mapDispatchToProps = (dispatch, { email }) => ({
  removeEmail: () => dispatch(removeEmail(email)),
});

class EmailRow extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      confirmation: false,
    };
    this.openConfirmation = this.openConfirmation.bind(this);
    this.closeConfirmation = this.closeConfirmation.bind(this);

    this.removeEmail = this.removeEmail.bind(this);
  }

  removeEmail() {
    this.props.removeEmail();
  }

  openConfirmation() {
    this.setState({ confirmation: true });
  }

  closeConfirmation() {
    this.setState({ confirmation: false });
  }

  render() {
    const { email, active } = this.props;

    const confirm = (<div>
      Are you sure you want to remove user named {email.value} ??
      <Button
        icon={<FormCheckmarkIcon colorIndex="ok" />}
        href="#"
        plain
        label="Yes"
        onClick={this.removeEmail} />
      <Button
        icon={<FormCloseIcon colorIndex="critical" />}
        href="#"
        plain
        label="No"
        onClick={this.closeConfirmation} />
    </div>);

    const removeButton = (<Button
      icon={<FormTrashIcon />}
      href="#"
      plain
      label="Remove"
      onClick={this.openConfirmation} />);

    return (
      <ListItem
        key={email.value}
        justify="between"
        separator="horizontal">
        <span
          style={{
            color: active && 'green',
            fontWeight: active && 'bold',
          }}>
          {email.value}
        </span>
        {!active && <span className="secondary">
          {this.state.confirmation ? confirm : removeButton}
        </span>}
      </ListItem>);
  }
}

EmailRow.propTypes = {
  active: React.PropTypes.bool.isRequired,
  email: React.PropTypes.shape({
    value: React.PropTypes.string.isRequired,
  }).isRequired,
  removeEmail: React.PropTypes.func.isRequired,
};

// export default EmailRow;
export default connect(null, mapDispatchToProps)(EmailRow);
