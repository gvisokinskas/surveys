import 'jsdom-global/register';
import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import UserTable from './user-table';
import users from '../../redux/reducers/users';

export const TestDriver = () => {

  const store = applyMiddleware(thunk)(createStore)(combineReducers({
      users,
  }), {});

  const mockProps = {
    users: [{
      "id": 1,
      "username": "admin",
      "email": "admin@admin.com",
      "enabled": true,
      "authorities": [{ authorityName: "USER_ROLE" }]
    },
    {
      "id": 2,
      "username": "user",
      "email": "user@user.com",
      "enabled": false,
      "authorities": [{ authorityName: "USER_ROLE" }]
    }
  ]};

  const component = mount(<Provider store={store}>
      <UserTable {...mockProps} />
    </Provider>
  );

  return {
    getComponent: () => component,
    getRowCount: () => component.find('TableRow').length,
  };
}
