import React from 'react';

import TableRow from 'grommet/components/TableRow';
import Button from 'grommet/components/Button';
import CheckBox from 'grommet/components/CheckBox';
import Layer from 'grommet/components/Layer';
import FormCheckmarkIcon from 'grommet/components/icons/base/FormCheckmark';
import FormCloseIcon from 'grommet/components/icons/base/FormClose';
import FormTrashIcon from 'grommet/components/icons/base/FormTrash';

import { connect } from 'react-redux';

import { disableEnableUser, removeUser } from '../../../redux/actions/users';

import { isAdmin } from '../../../utils/user-check';

const mapDispatchToProps = dispatch => ({
  disableEnableUser: (username, enabled) => dispatch(disableEnableUser(username, enabled)),
  removeUser: username => dispatch(removeUser(username)),
});

class UserRow extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      enabled: this.props.user.enabled,
      modalVisible: false,
    };
    this.disableEnableUser = this.disableEnableUser.bind(this);
    this.removeUser = this.removeUser.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  disableEnableUser() {
    this.setState({
      enabled: !this.state.enabled,
    });
    this.props.disableEnableUser(this.props.user.username, this.state.enabled);
  }

  removeUser() {
    this.props.removeUser(this.props.user.username);
  }

  closeModal() {
    this.setState({ modalVisible: false });
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  render() {
    const { user } = this.props;

    const modal = (<div>
      Are you sure you want to remove user named {this.props.user.username} ??
      <Button
        icon={<FormCheckmarkIcon colorIndex="ok" />}
        href="#"
        plain
        label="Yes"
        onClick={this.removeUser}
      />
      <Button
        icon={<FormCloseIcon colorIndex="critical" />}
        href="#"
        plain
        label="No"
        onClick={this.closeModal}
      />
    </div>);

    return (
      <TableRow>
        <td>{user.username}</td>
        <td>{user.email}</td>
        <td>{isAdmin(user) ? 'Admin' : 'User'}</td>
        <td className="secondary">
          <CheckBox
            disabled={isAdmin(user)}
            label={this.state.enabled ? 'enabled' : 'disabled'}
            toggle
            checked={this.state.enabled}
            onChange={this.disableEnableUser}
          />
        </td>
        <td>
          {!isAdmin(user)
            && <Button
              icon={<FormTrashIcon />}
              href="#"
              plain
              label="Remove"
              onClick={this.openModal} />}
          <Layer
            onClose={this.closeModal}
            hidden={!this.state.modalVisible}>
            {modal}
          </Layer>
        </td>
      </TableRow>);
  }
}

UserRow.propTypes = {
  user: React.PropTypes.shape({
    username: React.PropTypes.string,
    email: React.PropTypes.string,
    enabled: React.PropTypes.bool,
    authorities: React.PropTypes.array,
  }).isRequired,
  disableEnableUser: React.PropTypes.func.isRequired,
  removeUser: React.PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(UserRow);
