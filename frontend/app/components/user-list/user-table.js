import React from 'react';

import Table from 'grommet/components/Table';

import UserRow from './user-row/user-row';

const UserTable = ({ users }) =>
  (<div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
    <Table>
      <thead>
        <tr>
          <th>Username</th>
          <th>E-Mail</th>
          <th>Type</th>
          <th>Enable/Disable</th>
          <th>Remove</th>
        </tr>
      </thead>
      <tbody>
        {users.map(user => (
          <UserRow user={user} key={user.username} />
        ))}
      </tbody>
    </Table>
  </div>);


UserTable.propTypes = {
  users: React.PropTypes.array.isRequired,
};

export default UserTable;
