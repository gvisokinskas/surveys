import { expect } from 'chai';

import { TestDriver } from './user-table.driver';

describe('<UserTable />', () => {

  let driver;

  before(() => {
    driver = TestDriver();
  });

  it('should load table with two rows', () => {
    expect(driver.getRowCount()).to.equal(2);
  });

});
