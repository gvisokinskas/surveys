import React, { PropTypes, Component } from 'react';
import R from 'ramda';

import Button from 'grommet/components/Button';
import Box from 'grommet/components/Box';
import Label from 'grommet/components/Label';
import Search from 'grommet/components/Search';
import SurveyCard from './survey-card/survey-card';

const gridStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  overflowX: 'hidden',
  overflowY: 'auto',
};

const pagesStyle = {
  height: 200,
  display: 'flex',
  alignItems: 'baseline',
  flexDirection: 'row',
  justifyContent: 'center',
};

class SurveyTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      currentPage: 1,
      surveysPerPage: 9,
    };

    this.changeSearch = this.changeSearch.bind(this);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  componentDidMount() {
    document.getElementById('search').parentElement.style.marginBottom = '15px';
    document.getElementById('box').parentElement.style.height = '100vh';
  }

  componentWillUnmount() {
    document.getElementById('box').parentElement.style.height = '';
  }

  changeSearch(event) {
    this.setState({ search: event.target.value });
  }

  handlePageClick(number) {
    this.setState({
      currentPage: number,
    });
  }

  render() {
    const { surveys, currentUser } = this.props;
    const { currentPage, surveysPerPage, search } = this.state;

    const filteredSurveys = surveys
    .filter(survey =>
      survey.label.toLowerCase().includes(this.state.search.toLowerCase()));

    const indexOfLastSurvey = currentPage * surveysPerPage;
    const indexOfFirstSurvey = indexOfLastSurvey - surveysPerPage;
    const currentSurveys = filteredSurveys.slice(indexOfFirstSurvey, indexOfLastSurvey);

    const renderSurveys = currentSurveys.map((survey, index) => (<SurveyCard
      key={index}
      survey={survey}
      currentUser={currentUser} />));

    const pageNumbers = R.range(1, Math.ceil(filteredSurveys.length / surveysPerPage) + 1);

    const renderPageNumbers = (<div style={pagesStyle}>
      <Label style={{ marginRight: 20 }}>Page</Label>
      {pageNumbers.map(number => (
        <Button
          key={number}
          onClick={() => this.handlePageClick(number)}
          plain={currentPage !== number}
          label={number} />))}
    </div>);

    return (<Box id="box" style={{ overflow: 'hidden' }}>
      <Search
        id="search"
        placeHolder="Search"
        inline
        responsive={false}
        value={this.state.search}
        onDOMChange={this.changeSearch} />
      <div style={gridStyle}>
        {renderSurveys}
      </div>
      {pageNumbers.length > 1 && renderPageNumbers}
    </Box>);
  }
}

SurveyTable.propTypes = {
  surveys: PropTypes.array.isRequired,
  currentUser: PropTypes.object,
};

SurveyTable.defaultProps = {
  currentUser: {},
};

export default SurveyTable;
