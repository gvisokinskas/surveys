import React, { Component, PropTypes } from 'react';
import R from 'ramda';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import Article from 'grommet/components/Article';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Form from 'grommet/components/Form';
import Footer from 'grommet/components/Footer';
import FormFields from 'grommet/components/FormFields';
import FormField from 'grommet/components/FormField';
import Button from 'grommet/components/Button';

import Status from '../generic/status/status';

import { importSurvey } from '../../redux/actions/excel';

const mapDispatchToProps = dispatch => ({
  importSurvey: data => dispatch(importSurvey(data)),
});

class ImportSurvey extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onFileChange = this.onFileChange.bind(this);

    this.state = {
      errors: {},
      excel: {},
      submitted: false,
    };
  }

  onSubmit(event) {
    event.preventDefault();
    const errors = {};
    let noErrors = true;
    const file = this.file.files[0];
    if (!file) {
      errors.file = 'required';
      noErrors = false;
    }
    if (noErrors) {
      this.props.importSurvey(file);
      this.setState({ submitted: true });
    } else {
      this.setState({ errors });
    }
  }

  onFileChange() {
    const file = this.file.files[0];
    if (file) {
      const excel = R.clone(this.state.excel);
      excel.name = file.name;
      this.setState({ excel });
    }
    this.setState({ fileName: file.name });
  }

  render() {
    const mainContent = (<Article align="center" pad={{ horizontal: 'medium' }} primary>
      <Form onSubmit={this.onSubmit}>
        <Header size="large" justify="between" pad="none">
          <Heading tag="h2" margin="none" strong>
            Select the survey
          </Heading>
        </Header>

        <FormFields>
          <fieldset>
            <FormField label="File" htmlFor="file" error={this.state.errors.file}>
              <input
                ref={(c) => { this.file = c; }}
                id="file"
                name="file"
                type="file"
                onChange={this.onFileChange} />
            </FormField>
          </fieldset>
        </FormFields>

        <Footer pad={{ vertical: 'medium' }} justify="between">
          <Button
            type="submit"
            primary
            label="Submit"
            onClick={this.onSubmit} />
        </Footer>
      </Form>
    </Article>);

    const success = (
      <Status
        message="Survey is being processed."
        actionName="Go to main page"
        action={() => browserHistory.push('./surveys')}
      />);

    return (
      <div>
        {this.state.submitted && success}
        {!this.state.submitted && mainContent}
      </div>
    );
  }
}

ImportSurvey.propTypes = {
  importSurvey: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(ImportSurvey);
