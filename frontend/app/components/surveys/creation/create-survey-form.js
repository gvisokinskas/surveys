import React, { PropTypes, Component } from 'react';
import R from 'ramda';
import { reduxForm, Field, FieldArray, arrayPush, getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import uuid from 'uuid';
import { browserHistory } from 'react-router';

import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Form from 'grommet/components/Form';
import FormFields from 'grommet/components/FormFields';
import Footer from 'grommet/components/Footer';
import Header from 'grommet/components/Header';

import Status from '../../generic/status/status';
import TextField from '../../form-fields/text-field';
import CheckField from '../../form-fields/check-field';
import Toolbox from './toolbox';
import PageArray from './fields/page-array';
import { createSurvey, editSurvey } from '../../../redux/actions/surveys';
import { validate } from './validate';

const mapDispatchToProps = dispatch => ({
  submitSurvey: data => dispatch(createSurvey(data)),
  submitEditSurvey: data => dispatch(editSurvey(data)),
});

const mapStateToProps = (state, { currentUser, editMode, survey }) => {
  let initialValues;
  if (editMode) {
    const merge = (obj1, obj2) => R.mergeWith(R.merge, obj1, obj2);
    const questions = survey.pages.reduce(merge).questions;

    survey.pages.forEach((page, pIndex) => {
      page.questions.forEach((question, qIndex) => {
        if (question.conditions) {
          const fixedConditions = question.conditions.map((condition) => {
            const lol = condition && R.find(R.propEq('code', condition.questionCode), survey.pages[pIndex].questions);
            if (lol) {
              return {
                questionCode: {
                  value: lol && lol.code,
                  label: lol && lol.label,
                  rangeStart: lol && lol.rangeStart,
                  rangeEnd: lol && lol.rangeEnd,
                  options: lol && lol.options && lol.options.map(o => o.value),
                  type: lol && lol.questionType,
                },
                valueCode: condition.valueCode ? condition.valueCode : '',
              };
            }
            return undefined;
          });
          if (fixedConditions[0] !== undefined) {
            question.conditions = fixedConditions;
          }
        }
      });
    });


    initialValues = {
      username: survey.username,
      label: survey.label,
      public: survey.public,
      code: survey.code,
      pages: survey.pages,
      version: survey.version,
    };
  } else {
    initialValues = {
      username: currentUser.sub,
      public: false,
      code: uuid.v4(),
      pages: [],
      version: 1,
    };
  }

  const formValues = getFormValues('createSurvey')(state);
  return {
    formValues,
    initialValues,
    currentUser,
  };
};

class CreateSurveyForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      success: false,
      error: false,
      collision: false,
    };
  }

  render() {
    const {
      submitSurvey,
      submitEditSurvey,
      handleSubmit,
      dispatch,
      formValues,
      editMode,
      initialValues,
    } = this.props;

    const submitForm = () => {
      if (editMode) {
        handleSubmit(formData => submitEditSurvey(formData))()
          .then((res) => {
            if (res.status === 200) this.setState({ success: true });
            else if (res.status === 412) this.setState({ collision: true });
            else this.setState({ error: true });
          });
      } else {
        handleSubmit(formData => submitSurvey(formData))()
          .then(v => this.setState({ success: true }))
          .catch(v => this.setState({ error: true }));
      }
    };

    const addPage = () => {
      dispatch(arrayPush('createSurvey', 'pages', { label: '', code: uuid.v4() }));
    };

    const addQuestion = (type) => {
      dispatch(arrayPush('createSurvey',
        `pages[${formValues.pages.length - 1}].questions`,
        {
          label: '',
          required: false,
          questionType: type,
          code: uuid.v4(),
        }));
    };

    const form = (<Form style={{ width: '75%' }}>
      <Toolbox addPage={addPage} addQuestion={addQuestion} vertical alignWith="form-fields" />
      <FormFields id="form-fields">
        <Header>
          <Field name="label" component={TextField} bottomBorder placeholder="Survey Name" />
          <Field name="public" component={CheckField} label="Public" noBorder />
        </Header>
        <FieldArray name="pages" component={PageArray} pages={formValues && formValues.pages || initialValues.pages} />
      </FormFields>
      <Footer pad={{ vertical: 'medium' }}>
        <Button
          label="Submit"
          primary
          onClick={submitForm} />
      </Footer>
    </Form>);


    const goBack = (<Status
      message={editMode ? 'Survey was edited successfully!' : 'Survey was created successfully!'}
      actionName="Go to main page"
      action={() => browserHistory.push('./surveys')} />);

    const errorBlock = (<Status
      message="An error has occurred! Confused programmers are investigating"
      actionName="Go to main page"
      action={() => browserHistory.push('./surveys')} />);

    const collisionBlock = (<Status
      message="This survey was already updated. Changes cannot be saved! Refresh the page to get the latest version."
      actionName="Refresh"
      action={() => window.location.reload()}
    />);

    return (
      <Box pad="large" direction="column" align="center">
        {this.state.success && goBack}
        {this.state.error && errorBlock}
        {this.state.collision && collisionBlock}
        {!this.state.error && !this.state.success && !this.state.collision && form}
      </Box>);
  }
}

CreateSurveyForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitSurvey: PropTypes.func.isRequired,
  submitEditSurvey: PropTypes.func.isRequired,
  formValues: PropTypes.object,
  initialValues: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  editMode: PropTypes.bool,
};

CreateSurveyForm.defaultProps = {
  formValues: null,
  initialValues: null,
  editMode: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'createSurvey',
  validate,
})(CreateSurveyForm));
