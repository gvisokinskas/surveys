import React, { PropTypes, Component } from 'react';
import { FieldArray, Field } from 'redux-form';

import Box from 'grommet/components/Box';
import FormTrashIcon from 'grommet/components/icons/base/FormTrash';
import CaretNextIcon from 'grommet/components/icons/base/CaretNext';
import CaretDownIcon from 'grommet/components/icons/base/CaretDown';
import Header from 'grommet/components/Header';

import QuestionArray from './question-array';
import TextField from '../../../form-fields/text-field';

class Page extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showContent: false,
    };

    this.toggleContent = this.toggleContent.bind(this);
  }

  toggleContent() {
    this.setState({ showContent: !this.state.showContent });
  }

  render() {
    const { index, allQuestions } = this.props;

    const label = () => (
      <Box direction="row" align="center" full="horizontal" responsive={false}>
        <Box onClick={this.props.removePage}><FormTrashIcon colorIndex="error" /></Box>
        <Field name={`pages[${index}].label`} component={TextField} placeholder="Page title" noBorder />
      </Box>
    );

    return (
      <Box>
        <div>
          <Header>
            <Box
              flex
              justify="between"
              align="center"
              direction="row"
              responsive={false}
              pad="small"
              style={{ borderBottom: '1px solid rgba(0, 0, 0, 0.15)', paddingBottom: 0 }}>
              {label(index)}
              <div style={{ cursor: 'pointer' }} onClick={this.toggleContent}>
                {!this.state.showContent && <CaretNextIcon />}
                {this.state.showContent && <CaretDownIcon />}
              </div>
            </Box>
          </Header>
          <div style={{ display: this.state.showContent ? '' : 'none' }}>
            <FieldArray
              name={`pages[${index}].questions`}
              component={QuestionArray}
              allQuestions={allQuestions}
              pageIndex={index} />
          </div>
        </div>
      </Box>
    );
  }
}

Page.propTypes = {
  index: PropTypes.number.isRequired,
  allQuestions: PropTypes.array.isRequired,
  removePage: PropTypes.func.isRequired,
};

export default Page;
