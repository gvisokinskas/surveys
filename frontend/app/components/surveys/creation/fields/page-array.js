import React, { PropTypes } from 'react';
import uuid from 'uuid';

import Box from 'grommet/components/Box';
import Toolbox from '../toolbox';
import Page from './page';

const PageArray = ({ fields, meta, pages }) =>
(
  <Box>
    {fields.map((page, index) => (
      <Page
        index={index}
        key={index}
        allQuestions={pages[index].questions}
        removePage={() => fields.remove(index)} />
    ))}
    {!fields.length && <Box pad="medium" align="center">Currently there are no pages. Add one!</Box>}
    <Toolbox addPage={() => fields.push({ label: '', code: uuid.v4() })} />
    {meta.touched && !fields.length && <Box style={{ color: 'red' }} align="center">{meta.error}</Box>}
  </Box>
);

PageArray.propTypes = {
  fields: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  pages: PropTypes.array.isRequired,
};

export default PageArray;
