import React, { PropTypes, Component } from 'react';
import { FieldArray, arrayPush } from 'redux-form';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Select from 'grommet/components/Select';
import AddIcon from 'grommet/components/icons/base/Add';

import ConditionsArray from './condition-fields/conditions-array';

class ConditionsField extends Component {

  constructor(props) {
    super(props);
    this.state = {
      boxVisible: false,
      values: [],
    };
    this.showBox = this.showBox.bind(this);
    this.addQuestion = this.addQuestion.bind(this);
  }

  showBox() {
    this.setState({ boxVisible: true });
  }

  addQuestion() {
    this.props.dispatch(arrayPush('createSurvey', `pages[${this.props.pageIndex}.questions[${this.props.questionIndex}]].conditions`, {}));
  }

  render() {
    const { questions, questionIndex, pageIndex } = this.props;

    const source = questions
      .filter((q, index) => index < questionIndex)
      .map(q => ({
        value: q.code,
        label: !q.label.length ? '<< no title >>' : q.label,
        rangeStart: q.rangeStart,
        rangeEnd: q.rangeEnd,
        options: q.options && q.options.map(o => o.value),
        type: q.questionType,
      }));

    return this.props.questionIndex > 0
      ? (<Box>
        {!this.state.boxVisible && <Anchor onClick={this.showBox}>Show dependencies</Anchor>}
        {this.state.boxVisible
          && (<Box>
            <FieldArray
              name={`pages[${pageIndex}].questions[${questionIndex}].conditions`}
              component={ConditionsArray}
              source={source}
              pageIndex={pageIndex}
              questionIndex={questionIndex} />
            <Button plain label="Add Question" icon={<AddIcon />} onClick={this.addQuestion} />
          </Box>)}
      </Box>)
      : null;
  }
}

ConditionsField.propTypes = {
  questions: PropTypes.object.isRequired,
  questionIndex: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(ConditionsField);
