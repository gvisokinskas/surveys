import React, { PropTypes } from 'react';
import { Field, FieldArray } from 'redux-form';
import Box from 'grommet/components/Box';
import FormTrashIcon from 'grommet/components/icons/base/FormTrash';

import TextField from '../../../form-fields/text-field';
import CheckField from '../../../form-fields/check-field';
import OptionsArray from './options-array';
import Conditions from './conditions';
import { boxStyle } from './style';

const SelectionQuestionField = ({ pageIndex, index, removeQuestion, allQuestions }) => (<Box style={boxStyle} direction="column">
  <Box direction="row" pad="medium">
    <Box onClick={removeQuestion} justify="center">
      <FormTrashIcon colorIndex="error" />
    </Box>
    <Box direction="column" justify="between" align="center" pad="medium" style={{ width: '100%' }}>
      <Field name={`pages[${pageIndex}].questions[${index}].label`} component={TextField} label="Question" />
      <FieldArray
        name={`pages[${pageIndex}].questions[${index}].options`}
        component={OptionsArray}
        pageIndex={pageIndex}
        questionIndex={index} />
      <Box direction="row" justify="between">
        <Field
          name={`pages[${pageIndex}].questions[${index}].multiple`}
          component={CheckField}
          label="Multiple choice"
          noBorder
        />
        <Field
          name={`pages[${pageIndex}].questions[${index}].required`}
          component={CheckField}
          label="Required"
          noBorder />
      </Box>
    </Box>
  </Box>
  <Box pad="medium">
    <Conditions questions={allQuestions} questionIndex={index} pageIndex={pageIndex} />
  </Box>
</Box>);

SelectionQuestionField.propTypes = {
  index: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  removeQuestion: PropTypes.func.isRequired,
  allQuestions: PropTypes.object.isRequired,
};

export default SelectionQuestionField;
