import React, { PropTypes } from 'react';
import { Field } from 'redux-form';
import FormField from 'grommet/components/FormField';
import Select from 'grommet/components/Select';
import ValueConditionField from './value-condition-field';
import NumberField from '../../../../form-fields/number-field';


const TextConditionField = ({ input, meta, source, pageIndex, questionIndex, conditionIndex }) =>
  (<div>
    <FormField error={meta.touched && !!meta.error && meta.error}>
      <Select
        placeHolder="Question"
        options={source}
        value={input.value}
        onChange={v => input.onChange(v.value)} />
    </FormField>
    {input.value.options && <Field
      name={`pages[${pageIndex}].questions[${questionIndex}].conditions[${conditionIndex}].valueCode`}
      source={input.value.options}
      component={ValueConditionField} />}
    {input.value.type === 'RANGE' && <Field
      name={`pages[${pageIndex}].questions[${questionIndex}].conditions[${conditionIndex}].valueCode`}
      source={input.value.options}
      component={NumberField}
      min={input.value.rangeStart}
      max={input.value.rangeEnd} />}
  </div>);

TextConditionField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  source: PropTypes.array.isRequired,
  questionIndex: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  conditionIndex: PropTypes.number.isRequired,
};

export default TextConditionField;
