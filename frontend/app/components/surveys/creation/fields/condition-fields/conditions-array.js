import React, { PropTypes } from 'react';
import { Field } from 'redux-form';

import Box from 'grommet/components/Box';
import FormCloseIcon from 'grommet/components/icons/base/FormClose';

import TextConditionField from './text-condition-field';

const xStyle = {
  zIndex: 1,
  position: 'absolute',
  right: 0,
  marginLeft: 10,
  marginTop: -3,
  marginRight: 50,
};

const boxStyle = {
  paddingRight: 20,
  paddingLeft: 50,
};

const ConditionsArray = ({ fields, meta, source, pageIndex, questionIndex }) =>
(
  <Box>
    {fields.map((condition, index) => (
      <Box style={boxStyle}>
        <Field
          name={`pages[${pageIndex}].questions[${questionIndex}].conditions[${index}].questionCode`}
          component={TextConditionField}
          source={source}
          pageIndex={pageIndex}
          questionIndex={questionIndex}
          conditionIndex={index} />
        <div style={xStyle}>
          <FormCloseIcon key={`${index}q`} colorIndex="error" onClick={() => fields.remove(index)} />
        </div>
      </Box>
    ))}
  </Box>
);

ConditionsArray.propTypes = {
  fields: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  source: PropTypes.array.isRequired,
  pageIndex: PropTypes.number.isRequired,
  questionIndex: PropTypes.number.isRequired,
};

export default ConditionsArray;
