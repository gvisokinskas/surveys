import React, { PropTypes } from 'react';

import FormField from 'grommet/components/FormField';
import Select from 'grommet/components/Select';

const ValueConditionField = ({ input, meta, source }) => (<FormField
  error={meta.touched && !!meta.error && meta.error}>
  <Select
    placeHolder="Option"
    options={source}
    value={input.value}
    onChange={v => input.onChange(v.value)} />
</FormField>);

ValueConditionField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  source: PropTypes.array.isRequired,
};

export default ValueConditionField;
