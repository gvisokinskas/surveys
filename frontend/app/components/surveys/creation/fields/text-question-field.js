import React, { PropTypes } from 'react';
import { Field } from 'redux-form';
import Box from 'grommet/components/Box';
import FormTrashIcon from 'grommet/components/icons/base/FormTrash';

import TextField from '../../../form-fields/text-field';
import CheckField from '../../../form-fields/check-field';
import Conditions from './conditions';
import { boxStyle } from './style';

const TextQuestionField = ({ pageIndex, index, removeQuestion, allQuestions }) => (<Box direction="column" style={boxStyle}>
  <Box direction="row" pad="medium">
    <Box onClick={removeQuestion} justify="center">
      <FormTrashIcon colorIndex="error" />
    </Box>
    <Box direction="column" pad="medium" style={{ width: '100%' }}>
      <Field name={`pages[${pageIndex}].questions[${index}].label`} component={TextField} label="Question" />
      <Field name={`pages[${pageIndex}].questions[${index}].required`} component={CheckField} label="Required" noBorder />
    </Box>
  </Box>
  <Box pad="medium">
    <Conditions questions={allQuestions} questionIndex={index} pageIndex={pageIndex} />
  </Box>
</Box>);

TextQuestionField.propTypes = {
  index: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  removeQuestion: PropTypes.func.isRequired,
  allQuestions: PropTypes.array.isRequired,
};

export default TextQuestionField;
