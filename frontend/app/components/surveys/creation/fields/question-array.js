import React, { PropTypes } from 'react';
import { Field } from 'redux-form';
import uuid from 'uuid';

import Box from 'grommet/components/Box';
import Anchor from 'grommet/components/Anchor';
import ContactIcon from 'grommet/components/icons/base/Contact';
import ShiftIcon from 'grommet/components/icons/base/Shift';
import SortIcon from 'grommet/components/icons/base/Sort';

import QuestionField from './question-field';
import Toolbox from '../toolbox';

const QuestionArray = ({ fields, pageIndex, meta, allQuestions }) => {
  const addQuestion = (type) => {
    fields.push({
      label: '',
      required: false,
      questionType: type,
      code: uuid.v4(),
    });
  };

  return (
    <Box>
      <Box>
        {fields.map((question, index) => (
          <QuestionField
            type={fields.get(index).questionType}
            pageIndex={pageIndex}
            index={index}
            key={index}
            allQuestions={allQuestions}
            removeQuestion={() => fields.remove(index)} />))}
      </Box>
      {!fields.length
        && <Box align="center" pad="medium">There are no questions in this page</Box>}
      <Toolbox addQuestion={addQuestion} />
      {meta.touched && meta.error && <Box style={{ color: 'red' }} align="center">{meta.error}</Box>}
    </Box>);
};

QuestionArray.propTypes = {
  fields: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  pageIndex: PropTypes.number.isRequired,
  allQuestions: PropTypes.array.isRequired,
};

export default QuestionArray;
