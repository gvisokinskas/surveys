import React, { PropTypes } from 'react';
import { Field } from 'redux-form';
import uuid from 'uuid';

import Box from 'grommet/components/Box';
import FormCloseIcon from 'grommet/components/icons/base/FormClose';

import QuestionField from './question-field';
import TextField from '../../../form-fields/text-field';

const xStyle = {
  marginLeft: -25,
  zIndex: 1,
};

const OptionsArray = ({ fields, pageIndex, questionIndex, meta }) => (
  <Box style={{ width: '100%' }}>
    {fields.map((question, index) => <Box key={`${index}b`} direction="row">
      <Field
        name={`pages[${pageIndex}].questions[${questionIndex}].options[${index}].value`}
        component={TextField}
        key={index}
        label={`Option ${index + 1}`} />
      <FormCloseIcon key={`${index}q`} colorIndex="error" onClick={() => fields.remove(index)} style={xStyle} />
    </Box>)}
    <Box onClick={() => fields.push({ code: uuid.v4(), label: '' })}>Add option</Box>
    {meta.touched && meta.error && <Box align="center" style={{ color: 'red' }}>{meta.error}</Box>}
  </Box>);

OptionsArray.propTypes = {
  fields: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  pageIndex: PropTypes.number.isRequired,
  questionIndex: PropTypes.number.isRequired,
};

export default OptionsArray;
