import React, { PropTypes } from 'react';

import TextQuestionField from './text-question-field';
import RangeQuestionField from './range-question-field';
import SelectionQuestionField from './selection-question-field';

import { QUESTION_TYPE } from '../../../../utils/constants';

const Question = ({ index, pageIndex, removeQuestion, type, allQuestions }) => {
  switch (type) {
    case QUESTION_TYPE.TEXT:
      return (<TextQuestionField
        index={index}
        pageIndex={pageIndex}
        removeQuestion={removeQuestion}
        allQuestions={allQuestions} />);
    case QUESTION_TYPE.RANGE:
      return (<RangeQuestionField
        index={index}
        pageIndex={pageIndex}
        removeQuestion={removeQuestion}
        allQuestions={allQuestions} />);
    case QUESTION_TYPE.SELECTION:
      return (<SelectionQuestionField
        index={index}
        pageIndex={pageIndex}
        removeQuestion={removeQuestion}
        allQuestions={allQuestions} />);
    default:
      return null;
  }
};

Question.propTypes = {
  index: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  removeQuestion: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  allQuestions: PropTypes.array.isRequired,
};

export default Question;
