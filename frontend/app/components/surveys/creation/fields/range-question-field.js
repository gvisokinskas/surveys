import React, { PropTypes } from 'react';
import { Field } from 'redux-form';
import Box from 'grommet/components/Box';
import FormTrashIcon from 'grommet/components/icons/base/FormTrash';

import TextField from '../../../form-fields/text-field';
import NumberField from '../../../form-fields/number-field';
import CheckField from '../../../form-fields/check-field';
import Conditions from './conditions';
import { boxStyle } from './style';

const RangeQuestionField = ({ pageIndex, index, removeQuestion, allQuestions }) => (<Box direction="column" style={boxStyle}>
  <Box direction="row" pad="medium">
    <Box onClick={removeQuestion} justify="center">
      <FormTrashIcon colorIndex="error" />
    </Box>
    <Box direction="column" justify="between" align="center" style={{ width: '100%' }} pad="medium">
      <Field name={`pages[${pageIndex}].questions[${index}].label`} component={TextField} label="Question" />
      <Box direction="row" style={{ width: '100%' }} justify="between">
        <Field name={`pages[${pageIndex}].questions[${index}].rangeStart`} component={NumberField} label="Start" />
        <Field name={`pages[${pageIndex}].questions[${index}].rangeEnd`} component={NumberField} label="End" />
      </Box>
      <Field name={`pages[${pageIndex}].questions[${index}].required`} component={CheckField} label="Required" noBorder />
    </Box>
  </Box>
  <Box pad="medium">
    <Conditions questions={allQuestions} questionIndex={index} pageIndex={pageIndex} />
  </Box>
</Box>);

RangeQuestionField.propTypes = {
  index: PropTypes.number.isRequired,
  pageIndex: PropTypes.number.isRequired,
  removeQuestion: PropTypes.func.isRequired,
  allQuestions: PropTypes.array.isRequired,
};

export default RangeQuestionField;
