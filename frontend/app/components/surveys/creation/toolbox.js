import React, { PropTypes, Component } from 'react';
import R from 'ramda';

import Box from 'grommet/components/Box';
import Anchor from 'grommet/components/Anchor';
import Tip from 'grommet/components/Tip';
import AddChapterIcon from 'grommet/components/icons/base/AddChapter';
import ContactIcon from 'grommet/components/icons/base/Contact';
import ShiftIcon from 'grommet/components/icons/base/Shift';
import SortIcon from 'grommet/components/icons/base/Sort';

const padding = 10;

const verticalStyle = {
  position: 'fixed',
  background: 'white',
  borderRadius: 2,
  boxShadow: '10px 10px 5px #888888',
  zIndex: 19,
};

const horizontalStyle = {
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'row',
};

class Toolbox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visibleTip: undefined,
      tipText: undefined,
      position: { left: 0 },
    };
    this.setTipTarget = this.setTipTarget.bind(this);
    this.hideTip = this.hideTip.bind(this);
    this.align = this.align.bind(this);
  }

  componentDidMount() {
    this.align();
    window.addEventListener('resize', this.align);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.align);
  }

  setTipTarget(target, text) {
    this.props.vertical && this.setState({
      visibleTip: target,
      tipText: text,
    });
  }

  hideTip() {
    this.setState({ visibleTip: undefined });
  }

  align() {
    const anchor = document.getElementById(this.props.alignWith);
    if (!anchor) return;
    const bounds = anchor.getBoundingClientRect();
    this.setState({
      position: {
        left: bounds.left + bounds.width + padding,
      },
    });
  }

  render() {
    const { addPage, addQuestion, vertical } = this.props;

    const pageIcon = (<AddChapterIcon
      onMouseEnter={() => this.setTipTarget('page', 'New Page')}
      onMouseLeave={this.hideTip} />);

    const textIcon = (<ContactIcon
      onMouseEnter={() => this.setTipTarget('text', 'New Text Question')}
      onMouseLeave={this.hideTip} />);

    const rangeIcon = (<ShiftIcon
      onMouseEnter={() => this.setTipTarget('range', 'New Range Question')}
      onMouseLeave={this.hideTip} />);

    const selectionIcon = (<SortIcon
      onMouseEnter={() => this.setTipTarget('selection', 'New Selection Question')}
      onMouseLeave={this.hideTip} />);

    return (<Box style={vertical ? R.merge(verticalStyle, this.state.position) : horizontalStyle}>
      {this.state.visibleTip
        && <Tip target={this.state.visibleTip} onClose={() => null}>
          {this.state.tipText}
        </Tip>}
      {addPage && <Anchor
        id="page"
        icon={pageIcon}
        onClick={addPage} />}
      {addQuestion && <Anchor
        id="text"
        icon={textIcon}
        onClick={() => addQuestion('TEXT')} />}
      {addQuestion && <Anchor
        id="range"
        icon={rangeIcon}
        onClick={() => addQuestion('RANGE')} />}
      {addQuestion && <Anchor
        id="selection"
        icon={selectionIcon}
        onClick={() => addQuestion('SELECTION')} />}
    </Box>);
  }
}

Toolbox.propTypes = {
  addPage: PropTypes.func,
  addQuestion: PropTypes.func,
  vertical: PropTypes.bool.isRequired,
  alignWith: PropTypes.string,
};

Toolbox.defaultProps = {
  addPage: null,
  addQuestion: null,
  vertical: false,
  alignWith: null,
};

export default Toolbox;
