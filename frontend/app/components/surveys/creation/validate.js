export const validate = (values) => {
  const errors = {};

  if (!values.label || !values.label.trim()) {
    errors.label = 'Required';
  } else if (values.label.length < 4) {
    errors.label = 'Name must be at least 4 characters long';
  } else if (values.label.length > 100) {
    errors.label = 'Name must be not longer than 100 characters';
  }

  if (!values.pages || !values.pages.length) {
    errors.pages = { _error: 'At least one member must be entered' };
  } else {
    const pagesArrayErrors = [];

    values.pages.forEach((page, index) => {
      const pageErrors = {};

      if (!page || !page.label || !page.label.trim()) {
        pageErrors.label = 'Required';
      } else if (page.label.length < 4) {
        pageErrors.label = 'Name must be at least 4 characters long';
      } else if (page.label.length > 100) {
        pageErrors.label = 'Name must be not longer than 100 characters';
      }

      pagesArrayErrors[index] = pageErrors;

      if (!page || !page.questions || !page.questions.length) {
        pageErrors.questions = { _error: 'Page must contain at least one question' };
        pagesArrayErrors[index] = pageErrors;
      } else {
        const questionsArrayErrors = [];

        page.questions.forEach((question, qIndex) => {
          const questionErrors = {};
          if (!question || !question.label || !question.label.trim()) {
            questionErrors.label = 'Required';
          } else if (question.label.length < 4) {
            questionErrors.label = 'Name must be at least 4 characters long';
          } else if (question.label.length > 100) {
            questionErrors.label = 'Name must be not longer than 100 characters';
          }

          if (question.questionType === 'RANGE' && question.rangeStart === undefined) {
            questionErrors.rangeStart = 'Required';
          } else if (question.rangeStart < 0) {
            questionErrors.rangeStart = 'Value must be greater than -1';
          }

          if (question.questionType === 'RANGE' && !question.rangeEnd) {
            questionErrors.rangeEnd = 'Required';
          } else if (+question.rangeEnd <= +question.rangeStart) {
            questionErrors.rangeEnd = 'Should be higher than start';
          }

          questionsArrayErrors[qIndex] = questionErrors;

          if (question.questionType === 'SELECTION') {
            if (!question.options || !question.options.length) {
              questionErrors.options = { _error: 'Question must contain at least one option' };
              questionsArrayErrors[qIndex] = questionErrors;
            } else {
              const optionsArrayErrors = [];
              question.options.forEach((option, oIndex) => {
                const optionErrors = {};
                if (!option || !option.value || !option.value.trim()) {
                  optionErrors.value = 'Required';
                  optionsArrayErrors[oIndex] = optionErrors;
                }
              });
              questionErrors.options = optionsArrayErrors;
            }
          }
        });
        pageErrors.questions = questionsArrayErrors;
      }
    });

    if (pagesArrayErrors.length) {
      errors.pages = pagesArrayErrors;
    }
  }

  return errors;
};
