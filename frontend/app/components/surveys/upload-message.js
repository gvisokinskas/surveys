import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import Toast from 'grommet/components/Toast';

const mapStateToProps = state => ({
  uploadMessage: state.excel.uploadMessage,
  uploadTimestamp: state.excel.uploadTimestamp,
});

class UploadMessage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showToast: false,
      uploadTimestamp: new Date(),
      timeoutId: null,
    };
  }

  componentWillReceiveProps(props) {
    const { uploadMessage, uploadTimestamp } = props;

    if (uploadMessage && (uploadTimestamp.getTime() >= this.state.uploadTimestamp.getTime())) {
      this.setState({
        showToast: true,
        uploadTimestamp,
        timeoutId: setTimeout(() => this.setState({
          showToast: false,
        }), 3000),
      });
    }
  }

  componentWillUnmount() {
    if (this.state.timeoutId) {
      clearTimeout(this.state.timeoutId);
    }
  }

  render() {
    return (
      <div>
        {this.state.showToast && <Toast status="unknown">
          {this.props.uploadMessage}
        </Toast>}
      </div>);
  }
}

UploadMessage.propTypes = {
  uploadMessage: PropTypes.string,
  uploadTimestamp: PropTypes.object,
};

UploadMessage.defaultProps = {
  uploadMessage: null,
  uploadTimestamp: new Date(),
};

export default connect(mapStateToProps)(UploadMessage);
