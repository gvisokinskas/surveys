import React, { PropTypes, Component } from 'react';
import R from 'ramda';

import TextInput from 'grommet/components/TextInput';
import Button from 'grommet/components/Button';
import ShareIcon from 'grommet/components/icons/base/Share';

import { URL } from '../../../utils/constants';

class ShareButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      style: {
        position: 'absolute',
        border: '4px solid rgb(208, 208, 208)',
        backgroundColor: '#f5f5f5',
        padding: 15,
        zIndex: 500,
        display: 'none',
        alignItems: 'center',
      },
    };

    this.toggle = this.toggle.bind(this);
    this.resize = this.resize.bind(this);
    this.getPosition = this.getPosition.bind(this);
    this.hide = this.hide.bind(this);
  }

  componentDidMount() {
    const tooltip = this.tooltip;
    const container = this.container;

    container.removeChild(tooltip);
    document.body.appendChild(tooltip);

    this.resize();

    window.addEventListener('resize', this.resize);
    window.addEventListener('click', this.hide);
    this.tooltip.addEventListener('click', e => e.stopPropagation());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize);
    window.removeEventListener('click', this.hide);
    document.body.removeChild(this.tooltip);
  }

  getPosition() {
    const displayProp = this.tooltip.style.display;
    this.tooltip.style.display = '';
    const anchor = this.container.getBoundingClientRect();
    const tooltip = this.tooltip.getBoundingClientRect();

    const top = window.scrollY + anchor.top + anchor.height / 2 - tooltip.height / 2;
    const left = window.scrollX + anchor.left + anchor.width;
    this.tooltip.style.display = displayProp;
    return { top, left };
  }

  resize() {
    const pos = this.getPosition();
    const existingStyle = this.state.style;
    const newStyle = R.merge(existingStyle, { top: pos.top, left: pos.left });
    this.setState({ style: newStyle });
  }

  toggle(e) {
    e.stopPropagation();
    const existingStyle = R.clone(this.state.style);

    if (existingStyle.display !== 'none') {
      existingStyle.display = 'none';
    } else {
      existingStyle.display = 'flex';
    }

    const pos = this.getPosition();
    existingStyle.top = pos.top;
    existingStyle.left = pos.left;

    this.setState({ style: existingStyle });
  }

  hide() {
    const existingStyle = R.clone(this.state.style);
    existingStyle.display = 'none';
    this.setState({ style: existingStyle });
  }

  render() {
    return (
      <div ref={(c) => { this.container = c; }} style={{ position: 'relative' }}>
        <Button icon={<ShareIcon />} onClick={this.toggle} />
        <div ref={(c) => { this.tooltip = c; }} style={this.state.style}>
          <div style={{ marginRight: 5 }}> Copy the link: </div>
          <TextInput style={{ background: 'white' }} value={`${URL}/survey/${this.props.survey.code}`} />
        </div>
      </div>
    );
  }
}

ShareButton.propTypes = {
  survey: PropTypes.object.isRequired,
};

export default ShareButton;
