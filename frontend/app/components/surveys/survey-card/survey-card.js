import React, { PropTypes } from 'react';
import { browserHistory } from 'react-router';
import R from 'ramda';

import Button from 'grommet/components/Button';
import Label from 'grommet/components/Label';
import Title from 'grommet/components/Title';
import EditIcon from 'grommet/components/icons/base/Edit';
import PieChartIcon from 'grommet/components/icons/base/PieChart';
import Box from 'grommet/components/Box';
import GroupIcon from 'grommet/components/icons/base/Group';
import MoreIcon from 'grommet/components/icons/base/More';
import Menu from 'grommet/components/Menu';
import Anchor from 'grommet/components/Anchor';

import { isCurrentUserAdmin } from '../../../utils/user-check';
import ShareButton from './share-button';
import DeleteSurvey from '../../generic/delete/delete-survey';
import DownloadButton from '../../generic/download/download-button';

const cardStyle = {
  margin: 10,
  width: 300,
  height: 200,
  position: 'relative',
};

const coloredBoxStyle = {
  height: 10,
  position: 'absolute',
  top: 0,
  right: 0,
  width: '100%',
};

const SurveyCard = ({ survey, currentUser }) => {
  const goToAnsweringSurvey = () => browserHistory.push(`/survey/${survey.code}`);
  const goToSurveyReport = () => browserHistory.push(`/report/${survey.code}`);
  const goToSurveyEdit = () => browserHistory.push(`/surveys/edit/${survey.code}`);

  const answered = Object.keys(survey.answersByUser || {}).includes(currentUser.sub);
  const owned = survey.username === currentUser.sub;

  const allowEdit = (owned || isCurrentUserAdmin(currentUser))
  && R.isEmpty(survey.answersByUser)
  && R.isEmpty(survey.anonymousAnswers);
  const allowView = (owned || isCurrentUserAdmin(currentUser) || survey.public)
    && survey.answersByUser;
  const allowDelete = (owned || isCurrentUserAdmin(currentUser));

  const editButton = <Button icon={<EditIcon />} onClick={goToSurveyEdit} />;
  const viewButton = <Button icon={<PieChartIcon />} onClick={goToSurveyReport} />;
  const shareButton = <ShareButton survey={survey} />;

  const answersButton = (<Button
    icon={<GroupIcon />}
    plain
    label={R.values(survey.answersByUser || {}).length +
    (survey.anonymousAnswers || []).length} />);

  const actions = (<div style={{ display: 'flex', flexDirection: 'row' }}>
    {answersButton}
    {allowEdit && editButton}
    {allowView && viewButton}
    {shareButton}
  </div>);

  let menuRef;
  const menu = (<div style={{ position: 'absolute', right: 0, top: 0 }}>
    <Menu
      ref={(ref) => { menuRef = ref; }}
      responsive={false}
      icon={<MoreIcon />}
      closeOnClick={false}>
      {allowDelete && <Anchor>
        <DeleteSurvey
          surveyCode={survey.code}
          hidden
          afterDelete={() => setTimeout(menuRef._onClose, 1)} />
      </Anchor>}
      <Anchor>
        <DownloadButton survey={survey} hidden />
      </Anchor>
    </Menu>
  </div>);


  const clickableTitle = (<Title onClick={owned ? goToSurveyReport : goToAnsweringSurvey}>
    {survey.label}
  </Title>);
  const normalTitle = <Title>{survey.label}</Title>;

  return (
    <Box
      style={cardStyle}
      colorIndex={answered ? 'unknown' : 'light-1'}
      pad="medium"
      direction="column">
      <Box style={coloredBoxStyle} colorIndex="unknown" />
      {menu}
      <Box>
        <Label>{survey.username}</Label>
        {answered ? normalTitle : clickableTitle}
      </Box>
      <div>
        {actions}
      </div>
    </Box>
  );
};

SurveyCard.propTypes = {
  survey: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
};

export default SurveyCard;
