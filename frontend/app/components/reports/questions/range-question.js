import React, { PropTypes, Component } from 'react';
import R from 'ramda';

import Value from 'grommet/components/Value';
import Chart, {
  Axis,
  Grid,
  Area,
  Base,
  Layers,
  MarkerLabel,
  Marker,
  HotSpots,
} from 'grommet/components/chart/Chart';
import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';

import percentile from '../../../utils/percentile';

class RangeQuestion extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeIndex: undefined,
      mean: undefined,
      median: undefined,
      mode: undefined,
      p25: undefined,
      p75: undefined,
    };

    this.setActiveSpot = this.setActiveSpot.bind(this);
    this.calculateStatistics = this.calculateStatistics.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
  }

  componentWillMount() {
    this.calculateStatistics();
  }

  setActiveSpot(index) {
    this.setState({ activeIndex: index });
  }

  calculateStatistics() {
    const { result } = this.props;
    const values = R.values(result.countableAnswers);
    const exploded = R.sort((a, b) => a - b,
      R.flatten(values.map(a => R.times(() => +a.optionValue, a.answerCount))));

    const mean = R.mean(exploded);
    const median = R.median(exploded);

    let mode;
    let max = 1;
    values.forEach((a) => {
      const bigger = +a.answerCount > max;
      if (bigger) {
        max = +a.answerCount;
        mode = a.optionValue;
      }
    });

    const p25 = percentile(exploded, 0.25);
    const p75 = percentile(exploded, 0.75);

    this.setState({ mean, median, mode, p25, p75 });
  }

  render() {
    const { result } = this.props;

    const values = R.values(result.countableAnswers).map(answer => answer.answerCount);

    const range = R.values(result.countableAnswers).map(answer => answer.optionValue);

    const count = range[range.length - 1] - range[0] + 1;

    const maxValue = Math.max(...values);

    const xLabels = [
      {
        index: 0,
        label: range[0],
      },
      {
        index: Math.round(count / 2) - 1,
        label: range[Math.round(count / 2) - 1],
      },
      {
        index: count - 1,
        label: range[count - 1],
      }];

    const yLabels = [
      {
        index: 0,
        label: 0,
      },
      {
        index: maxValue / 2,
        label: maxValue / 2,
      },
      {
        index: maxValue,
        label: maxValue,
      },
    ];

    return (<Box direction="row" justify="between" align="center">
      <Box pad="medium">
        <Chart>
          <Axis
            count={maxValue + 1}
            labels={yLabels}
            vertical />
          <Chart vertical>
            <MarkerLabel
              count={count}
              index={this.state.activeIndex}
              label={
                <Value
                  value={this.state.activeIndex ? `${range[this.state.activeIndex]} - ${values[this.state.activeIndex]}` : '0'}
                  style={!this.state.activeIndex ? { visibility: 'hidden' } : {}}
                />}
            />
            <Base />
            <Layers>
              <Grid
                rows={maxValue >= 1 ? 2 : (maxValue / 5) + 1}
                columns={(range.length / 5) + 1} />
              <Area
                values={values}
                colorIndex="graph-1"
                activeIndex={this.state.activeIndex}
                max={maxValue} />
              <Marker
                colorIndex="graph-2"
                count={count}
                vertical
                index={this.state.activeIndex} />
              <HotSpots
                count={count}
                activeIndex={this.state.activeIndex}
                onActive={this.setActiveSpot} />
            </Layers>
            <Axis
              count={count}
              labels={xLabels} />
          </Chart>
        </Chart>
      </Box>
      <Box pad="medium">
        <List>
          <ListItem separator="horizontal">
            <span>
              {`Mean ${(this.state.mean && this.state.mean.toFixed(4)) || '-'}`}
            </span>
          </ListItem>
          <ListItem separator="horizontal">
            <span>
              {`Median ${this.state.median || '-'}`}
            </span>
          </ListItem>
          <ListItem separator="horizontal">
            <span>
              {`Mode ${this.state.mode || '-'}`}
            </span>
          </ListItem>
          <ListItem separator="horizontal">
            <span>
              {`25th percentile ${this.state.p25 || '-'}`}
            </span>
          </ListItem>
          <ListItem separator="horizontal">
            <span>
              {`75th percentile ${this.state.p75 || '-'}`}
            </span>
          </ListItem>
        </List>
      </Box>
    </Box>
    );
  }
}

RangeQuestion.propTypes = {
  result: PropTypes.object.isRequired,
};

export default RangeQuestion;
