import React, { PropTypes } from 'react';
import WordCloud from 'react-d3-cloud';
import R from 'ramda';

import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';

const data = (textAnswers) => {
  const stringOfWords = textAnswers.join(' ');
  const arrayOfWords = stringOfWords.split(' ').filter(word => word.length >= 4);
  const preparedData = [];

  for (let i = 0; i < arrayOfWords.length; i++) {
    if (!R.find(R.propEq('text', arrayOfWords[i]), preparedData)) {
      const obj = {
        text: arrayOfWords[i],
        value: 2,
      };
      preparedData.push(obj);
    } else {
      R.find(R.propEq('text', arrayOfWords[i]), preparedData).value += 1;
    }
  }
  return preparedData;
};

const fontSizeMapper = word => Math.log2(word.value) * 10;
const rotate = word => word.value % 360;

const TextQuestion = ({ result }) => <Box direction="row" justify="between">
  <WordCloud
    data={data(result.textAnswers)}
    fontSizeMapper={fontSizeMapper}
    rotate={rotate}
    width={400}
    height={400} />
  <Box style={{ overflow: 'auto', height: 400 }} pad="medium">
    <List>
      {result.textAnswers.map((answer, index) =>
        (<ListItem
          justify="between"
          separator="horizontal"
          key={index}>
          <span>
            {answer}
          </span>
        </ListItem>))}
    </List>
  </Box>
</Box>;

TextQuestion.propTypes = {
  result: PropTypes.object.isRequired,
};

export default TextQuestion;
