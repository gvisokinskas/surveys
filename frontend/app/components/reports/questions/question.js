import React, { PropTypes } from 'react';

import TextQuestion from './text-question';
import RangeQuestion from './range-question';
import SelectionQuestion from './selection-question';

import { QUESTION_TYPE } from '../../../utils/constants';

const Question = ({ result }) => {
  switch (result.questionType) {
    case QUESTION_TYPE.TEXT:
      return (<TextQuestion result={result} />);
    case QUESTION_TYPE.RANGE:
      return (<RangeQuestion result={result} />);
    case QUESTION_TYPE.SELECTION:
      return (<SelectionQuestion result={result} />);
    default:
      return null;
  }
};

Question.propTypes = {
  result: PropTypes.object.isRequired,
};

export default Question;
