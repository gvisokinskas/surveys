import React, { PropTypes, Component } from 'react';
import R from 'ramda';

import Box from 'grommet/components/Box';
import Value from 'grommet/components/Value';
import AnnotatedMeter from 'grommet-addons/components/AnnotatedMeter';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';

import Chart, {
  Axis,
  Grid,
  Bar,
  Base,
  Layers,
  MarkerLabel,
  Marker,
  HotSpots,
} from 'grommet/components/chart/Chart';


class SelectionQuestion extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeIndex: undefined,
    };

    this.setActiveSpot = this.setActiveSpot.bind(this);
  }

  setActiveSpot(index) {
    this.setState({ activeIndex: index });
  }

  render() {
    const { result } = this.props;

    const values = R.values(result.countableAnswers).map(answer => answer.answerCount);

    const dataForSun = {
      max: R.sum(values),
      source: R.values(result.countableAnswers).map(answer =>
        ({ label: answer.optionValue, value: answer.answerCount })),
    };

    const maxValue = Math.max(...values);

    const xLabels = R.values(result.countableAnswers).map((answer, index) =>
      ({ index, label: <div style={{ transform: 'rotate(90deg)', maxWidth: '10px' }}>{answer.optionValue}</div> }));
    const yLabels = [
      {
        index: 0,
        label: 0,
      },
      {
        index: maxValue / 2,
        label: maxValue / 2,
      },
      {
        index: maxValue,
        label: maxValue,
      },
    ];

    const count = values.length;

    return (<Box direction="row" justify="between" align="center">
      <Box pad="medium">
        <Chart>
          <Axis
            count={maxValue + 1}
            labels={yLabels}
            vertical />
          <Chart vertical>
            <MarkerLabel
              count={count}
              index={this.state.activeIndex}
              label={
                <Value
                  value={this.state.activeIndex ? `${values[this.state.activeIndex]}` : '0'}
                  style={!this.state.activeIndex ? { visibility: 'hidden' } : {}}
                />}
            />
            <Base />
            <Layers>
              <Grid
                rows={maxValue <= 1 ? 2 : (maxValue / 5) + 1}
                columns={count} />
              <Bar
                values={values}
                colorIndex="graph-1"
                activeIndex={this.state.activeIndex}
                max={maxValue} />
              <Marker
                colorIndex="graph-2"
                count={count}
                vertical
                index={this.state.activeIndex} />
              <HotSpots
                count={count}
                activeIndex={this.state.activeIndex}
                onActive={this.setActiveSpot} />
            </Layers>
            <Axis
              count={count}
              labels={xLabels} />
          </Chart>
        </Chart>
      </Box>
      <Box pad="medium">
        <AnnotatedMeter
          type="circle"
          series={dataForSun.source}
          max={dataForSun.max} />
      </Box>
      <Box pad="medium" style={{ overflow: 'auto', height: 400 }}>
        <List>
          {R.values(result.countableAnswers).map((answer, index) =>
            (<ListItem
              separator="horizontal"
              key={index}>
              <span>
                {`${answer.optionValue} ${answer.answerCount}`}
              </span>
            </ListItem>))}
        </List>
      </Box>
    </Box>);
  }
}

SelectionQuestion.propTypes = {
  result: PropTypes.object.isRequired,
};

export default SelectionQuestion;
