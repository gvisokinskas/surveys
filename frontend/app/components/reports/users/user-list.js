import React, { PropTypes } from 'react';
import R from 'ramda';

import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Title from 'grommet/components/Title';

import UserAnswers from './user-answers';

const UserList = ({ users, anonyms, surveyCode }) => (
  <Box direction="row" justify="between">
    {R.keys(users).length > 0 && <Box full="horizontal">
      <List selectable>
        {R.keys(users).map(user =>
          <UserAnswers
            key={user}
            user={{ username: user, code: users[user] }}
            surveyCode={surveyCode} />)}
      </List>
    </Box>}
    {R.keys(anonyms).length > 0 && <Box full="horizontal">
      <List selectable>
        {anonyms.map(code =>
          <UserAnswers
            key={code}
            user={{ code }}
            surveyCode={surveyCode} />)}
      </List>
    </Box>}
  </Box>
);

UserList.propTypes = {
  users: PropTypes.object.isRequired,
  anonyms: PropTypes.array.isRequired,
  surveyCode: PropTypes.string.isRequired,
};

export default UserList;
