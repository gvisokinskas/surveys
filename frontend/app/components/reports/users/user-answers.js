import React, { PropTypes, Component } from 'react';
import R from 'ramda';
import { connect } from 'react-redux';

import Box from 'grommet/components/Box';
import ListItem from 'grommet/components/ListItem';
import Layer from 'grommet/components/Layer';
import Title from 'grommet/components/Title';

import { getUserReport } from '../../../redux/actions/reports';

const mapDispatchToProps = (dispatch, { user, surveyCode }) => ({
  getUserAnswers: () => dispatch(getUserReport(surveyCode, user.code)),
});

const mapStateToProps = (state, { user }) => ({
  report: state.userReports[user.code],
});

class UserAnswers extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
    this.showAnswers = this.showAnswers.bind(this);
    this.hideAnswers = this.hideAnswers.bind(this);
  }

  showAnswers() {
    this.props.getUserAnswers();
    this.setState({ visible: true });
  }

  hideAnswers() {
    this.setState({ visible: false });
  }

  render() {
    const answers = (<Box>
      {this.props.report && this.props.report.questionReports &&
      this.props.report.questionReports.map((question, i) =>
        <Box pad="medium" key={i}>
          <Title>{question.questionLabel}</Title>
          {question.questionType === 'TEXT' && question.textAnswers[0]}

          {question.questionType !== 'TEXT'
            && R.filter(R.propEq('answerCount', 1), R.values(question.countableAnswers))
            .map(v => v.optionValue)
            .join(', ')}
        </Box>)}
    </Box>);

    return (<ListItem onClick={this.showAnswers}>
      {this.props.user.username || this.props.user.code}
      {this.state.visible && <Layer
        align="right"
        closer
        onClose={this.hideAnswers}>
        {answers}
      </Layer>}
    </ListItem>);
  }
}

UserAnswers.propTypes = {
  user: PropTypes.object.isRequired,
  getUserAnswers: PropTypes.func.isRequired,
  report: PropTypes.object,
};

UserAnswers.defaultProps = {
  report: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(UserAnswers);
