import React, { Component, PropTypes } from 'react';
import R from 'ramda';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Accordion from 'grommet/components/Accordion';
import AccordionPanel from 'grommet/components/AccordionPanel';
import Heading from 'grommet/components/Heading';
import Label from 'grommet/components/Label';
import Timestamp from 'grommet/components/Timestamp';
import EditIcon from 'grommet/components/icons/base/Edit';

import Question from './questions/question';
import UserList from './users/user-list';
import DeleteSurvey from '../generic/delete/delete-survey';
import DownloadButton from '../generic/download/download-button';

import { isCurrentUserAdmin } from '../../utils/user-check';


const ReportTable = ({ report, survey, currentUser }) => {
  const owned = survey.username === currentUser.sub;
  const allowModify = (owned || isCurrentUserAdmin(currentUser));

  const surveyReport = (<Box pad="large">
    <div>
      <Box direction="row" align="baseline">
        <Label style={{ marginRight: 10 }}>Last updated:</Label>
        <Timestamp value={new Date(report.lastAnswerDate)} />
        <Label style={{ marginLeft: 50 }}>Total: {report.answerCount}</Label>
        <div style={{ marginLeft: 'auto' }}>
          <DownloadButton survey={survey} />
        </div>
      </Box>
    </div>
    <Accordion openMulti animate={false}>
      {report.questionReports.map((re, index) =>
      (<AccordionPanel
        heading={re.questionLabel}
        key={index}>
        <Question
          result={re} />
      </AccordionPanel>))}
      <AccordionPanel
        heading={`${Object.keys(survey.answersByUser).length ? 'Registered answers' : 'Anonymous answers'}`}>
        <UserList
          users={survey.answersByUser}
          anonyms={survey.anonymousAnswers}
          surveyCode={survey.code} />
      </AccordionPanel>
    </Accordion>
  </Box>);

  const noReport = (<Box justify="center" direction="column" align="center">
    <Label>Nobody has answered the survey</Label>
    {allowModify && <Label>You still have the chance to edit it!</Label>}
    {allowModify && <Button href={`/surveys/edit/${survey.code}`} plain icon={<EditIcon />}>Edit</Button>}
    <DownloadButton survey={survey} />
  </Box>);

  return (<Box pad="large">
    <Heading style={{ display: 'flex', justifyContent: 'space-between' }}>
      {survey.label}
      {allowModify && <DeleteSurvey redirect surveyCode={survey.code} />}
    </Heading>
    {report.answerCount === 0 ? noReport : surveyReport}
  </Box>);
};

ReportTable.propTypes = {
  report: PropTypes.object.isRequired,
  survey: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
};

export default ReportTable;
