import { ROLES } from './constants';

export const isAdmin = user => user.authorities.findIndex(authority =>
  authority.authorityName === ROLES.ADMIN) !== -1;

export const isCurrentUserAdmin = user => user.roles.includes(ROLES.ADMIN);
