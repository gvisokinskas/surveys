export default (response) => {
  const o = {};
  response.errors.forEach((e) => {
    o[e.field] = e.defaultMessage.toLowerCase();
  });
  return o;
};
