export const BASE_URL = 'http://localhost:8080/api';
export const URL = 'http://localhost:8080';

export const ROLES = {
  ADMIN: 'ROLE_ADMIN',
};

export const QUESTION_TYPE = {
  TEXT: 'TEXT',
  SELECTION: 'SELECTION',
  RANGE: 'RANGE',
};
