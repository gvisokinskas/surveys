import { browserHistory } from 'react-router';
import R from 'ramda';
import { BASE_URL } from '../../utils/constants';

export const REQUEST_SURVEYS_ALL = 'REQUEST_SURVEYS_ALL';
export const REQUEST_SURVEY = 'REQUEST_SURVEY';
export const ADD_SURVEY = 'ADD_SURVEY';
export const DELETE_SURVEY = 'DELETE_SURVEY';

export function requestSurveysAll(data) {
  return {
    type: REQUEST_SURVEYS_ALL,
    data,
  };
}

export function requestSurvey(data) {
  return {
    type: REQUEST_SURVEY,
    data,
  };
}

export function addSurvey(data) {
  return {
    type: ADD_SURVEY,
    data,
  };
}

export function requestDeleteSurvey(index) {
  return {
    type: DELETE_SURVEY,
    index,
  };
}

export function fetchSurveys() {
  return function fetchAllSurveys(dispatch) {
    fetch(`${BASE_URL}/surveys/all`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then(json => dispatch(requestSurveysAll(json))));
  };
}

export function fetchSurvey(code) {
  return function fetchAllSurveys(dispatch) {
    fetch(`${BASE_URL}/surveys/${code}`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then(json => dispatch(requestSurvey(json)))
      .catch(() => {
        if (!response.ok) {
          browserHistory.push('/login');
        }
      }));
  };
}

export function fetchSurveyByAnswer(answerCode) {
  return function fetchAllSurveys(dispatch) {
    fetch(`${BASE_URL}/surveys/by-answer/${answerCode}`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then(json => dispatch(requestSurvey(json))));
  };
}

export function createSurvey(data) {
  const fixedData = R.clone(data);
  fixedData.pages.forEach((page) => {
    page.questions.forEach((question) => {
      if (question.conditions) {
        const fixedConditions = question.conditions.map(condition => ({
          questionCode: condition.questionCode.value,
          valueCode: condition.valueCode ? condition.valueCode : '',
        }));
        question.conditions = fixedConditions;
      }
    });
  });

  return function fetchCreateSurvey(dispatch) {
    return new Promise((resolve, reject) => {
      fetch(`${BASE_URL}/surveys`, {
        method: 'post',
        body: JSON.stringify(fixedData),
        headers: {
          Authorization: window.localStorage.token,
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (response.status === 200) {
          dispatch(addSurvey(data));
          resolve();
        } else if (response.status === 400) {
          reject();
        }
      });
    });
  };
}

export function editSurvey(data) {
  const fixedData = R.clone(data);
  fixedData.pages.forEach((page) => {
    page.questions.forEach((question) => {
      if (question.conditions) {
        const fixedConditions = question.conditions.map(condition => ({
          questionCode: condition.questionCode.value,
          valueCode: condition.valueCode ? condition.valueCode : '',
        }));
        question.conditions = fixedConditions;
      }
    });
  });

  return function fetcheditSurvey(dispatch) {
    return fetch(`${BASE_URL}/surveys`, {
      method: 'put',
      body: JSON.stringify(fixedData),
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    });
  };
}

export function deleteSurvey(surveyCode, redirect) {
  return function fetchdeleteSurvey(dispatch, getState) {
    const state = getState();
    const index = state.surveys.findIndex(survey => survey.code === surveyCode);
    fetch(`${BASE_URL}/surveys/${surveyCode}`, {
      method: 'delete',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (redirect) {
        browserHistory.push('/surveys');
      }
      dispatch(requestDeleteSurvey(index));
    });
  };
}
