import R from 'ramda';
import { BASE_URL } from '../../utils/constants';
import { getSurveyReports } from './reports';

export const ADD_SAVED_ANSWER = 'ADD_SAVED_ANSWER';

export function addSavedAnswer(answerCode, data) {
  return {
    type: ADD_SAVED_ANSWER,
    answerCode,
    data,
  };
}

export function answerSurvey(data, surveyCode, finished = true) {
  return function fetchAnswerSurvey(dispatch) {
    const answers = R.filter(a => a.value !== '', data.answers).map(answer => answer.transform
      ? { questionCode: answer.questionCode, value: [answer.value] }
      : { questionCode: answer.questionCode, value: answer.value });

    const fullData = { code: data.code, answers, surveyCode, finished };

    return new Promise((resolve, reject) => {
      fetch(`${BASE_URL}/answers`, {
        method: 'post',
        body: JSON.stringify(fullData),
        headers: {
          Authorization: window.localStorage.token,
          'Content-Type': 'application/json',
        },
      })
      .then((response) => {
        if (response.status === 200) {
          resolve(response.text());
        } else if (response.status === 400) {
          reject();
        }
      });
    });
  };
}

export function fetchAnswer(answerCode) {
  return function fetchSavedAnswer(dispatch) {
    fetch(`${BASE_URL}/answers/${answerCode}`, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(response => response.json()
    .then(json => dispatch(addSavedAnswer(answerCode, json))));
  };
}
