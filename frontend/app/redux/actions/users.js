import { BASE_URL } from '../../utils/constants';
import mapErrors from '../../utils/error-map';

export const REQUEST_USERS_ALL = 'REQUEST_USERS_ALL';
export const DISABLE_ENABLE_USER = 'DISABLE_ENABLE_USER';
export const REMOVE_USER = 'REMOVE_USER';
export const CREATE_USER = 'CREATE_USER';
export const REGISTRATION_FAILED = 'REGISTRATION_FAILED';
export const REGISTRATION_PASSED = 'REGISTRATION_PASSED';

export function requestUsersAll(data) {
  return {
    type: REQUEST_USERS_ALL,
    data,
  };
}

export function requestDisableEnableUser(index, enabled) {
  return {
    type: DISABLE_ENABLE_USER,
    index,
    enabled,
  };
}

export function requestRemoveUser(index) {
  return {
    type: REMOVE_USER,
    index,
  };
}

export function createUser(data) {
  return {
    type: CREATE_USER,
    data,
  };
}

export function registrationFailed(errorMessages) {
  return {
    type: REGISTRATION_FAILED,
    errorMessages,
  };
}

export function registrationPassed() {
  return {
    type: REGISTRATION_PASSED,
  };
}

export function fetchUsers() {
  return function fetchAllUsers(dispatch) {
    fetch(`${BASE_URL}/users/all`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then((json) => {
        dispatch(requestUsersAll(json));
      }));
  };
}

export function disableEnableUser(username, enabled) {
  return function fetchDisableEnableUser(dispatch, getState) {
    const state = getState();
    const index = state.users.findIndex(user => user.username === username);

    dispatch(requestDisableEnableUser(index, enabled));

    fetch(`${BASE_URL}/users/${username}/${enabled ? 'disable' : 'enable'}`, {
      method: 'post',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    });
  };
}

export function removeUser(username) {
  return function fetchRemoveUser(dispatch, getState) {
    const state = getState();
    const index = state.users.findIndex(user => user.username === username);

    dispatch(requestRemoveUser(index));

    fetch(`${BASE_URL}/users/${username}/remove`, {
      method: 'delete',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    });
  };
}

export function registerUser(data) {
  return function fetchRegister(dispatch) {
    fetch(`${BASE_URL}/users/register`, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json()
        .then((json) => {
          if (response.ok) {
            dispatch(registrationPassed());
            dispatch(createUser(json));
          } else {
            dispatch(registrationFailed(mapErrors(json)));
          }
        }));
  };
}
