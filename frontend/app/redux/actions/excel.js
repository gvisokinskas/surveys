import fetch from 'isomorphic-fetch';
import FileSaver from 'file-saver';
import { BASE_URL } from '../../utils/constants';

export const SUBMIT_EXCEL = 'SUBMIT_EXCEL';

function submitExcel(data) {
  return {
    type: SUBMIT_EXCEL,
    data,
  };
}

export function importSurvey(file) {
  return function postSurvey(dispatch) {
    const formData = new FormData();
    formData.append('excel', file);

    fetch(`${BASE_URL}/excel`, {
      method: 'post',
      headers: {
        Authorization: window.localStorage.token,
      },
      body: formData,
    })
      .then(res => dispatch(submitExcel(res.ok ? 'Survey processed successfully!' : 'There were errors processing survey!')));
  };
}

export function downloadSurvey({ code, label }) {
  return function getSurvey() {
    fetch(`${BASE_URL}/excel/${code}`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
      },
    })
      .then((res) => {
        res.blob().then((data) => {
          FileSaver.saveAs(data, `${label}.xlsx`);
        });
      });
  };
}
