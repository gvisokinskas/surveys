import { BASE_URL } from '../../utils/constants';

export const REQUEST_SURVEY_REPORTS = 'REQUEST_SURVEY_REPORTS';
export const REQUEST_USER_REPORT = 'REQUEST_USER_REPORT';

export function requestSurveyReports(surveyCode, data) {
  return {
    type: REQUEST_SURVEY_REPORTS,
    surveyCode,
    data,
  };
}

export function requestUserReport(answerCode, data) {
  return {
    type: REQUEST_USER_REPORT,
    answerCode,
    data,
  };
}

export function getSurveyReport(surveyCode) {
  return function fetchAllAnswers(dispatch) {
    fetch(`${BASE_URL}/reports/${surveyCode}`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then(json => dispatch(requestSurveyReports(surveyCode, json))));
  };
}

export function getUserReport(surveyCode, answerCode) {
  return function fetchAnswer(dispatch) {
    fetch(`${BASE_URL}/reports/${surveyCode}/${answerCode}`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then(json => dispatch(requestUserReport(answerCode, json))));
  };
}
