import { BASE_URL } from '../../utils/constants';

export const REQUEST_ALLOWED_EMAILS_ALL = 'REQUEST_ALLOWED_EMAILS_ALL';
export const ADD_ALLOWED_EMAIL = 'ADD_ALLOWED_EMAIL';
export const REMOVE_ALLOWED_EMAIL = 'REMOVED_ALLOWED_EMAIL';


export function requestAllowedEmailsAll(data) {
  return {
    type: REQUEST_ALLOWED_EMAILS_ALL,
    data,
  };
}

export function requestAddEmail(data) {
  return {
    type: ADD_ALLOWED_EMAIL,
    data,
  };
}

export function requestRemoveEmail(index) {
  return {
    type: REMOVE_ALLOWED_EMAIL,
    index,
  };
}

export function fetchAllowedEmails() {
  return function fetchAllEmails(dispatch) {
    fetch(`${BASE_URL}/allowed-emails`, {
      method: 'get',
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then((json) => {
        dispatch(requestAllowedEmailsAll(json));
      }));
  };
}

export function addAllowedEmail(email) {
  return function fetchAddEmail(dispatch) {
    fetch(`${BASE_URL}/allowed-emails`, {
      method: 'post',
      body: JSON.stringify(email),
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(dispatch(requestAddEmail(email)));
  };
}

export function removeEmail(email) {
  return function fetchRemoveEmail(dispatch, getState) {
    const state = getState();
    const index = state.allowedEmails.findIndex(e => e.value === email.value);
    fetch(`${BASE_URL}/allowed-emails`, {
      method: 'delete',
      body: JSON.stringify(email),
      headers: {
        Authorization: window.localStorage.token,
        'Content-Type': 'application/json',
      },
    })
    .then(dispatch(requestRemoveEmail(index)));
  };
}
