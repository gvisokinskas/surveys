import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';
import jwtDecode from 'jwt-decode';

import { BASE_URL } from '../../utils/constants';

export const AUTH_USER = 'AUTH_USER';
export const LOGOUT_USER = 'USER_LOGOUT';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const CLEAR_REGISTRATION_ERRORS = 'CLEAR_REGISTRATION_ERRORS';

export function authUser(data) {
  return {
    type: AUTH_USER,
    data,
  };
}

export function dismissUser() {
  return {
    type: LOGOUT_USER,
  };
}

export function loginFailed() {
  return {
    type: LOGIN_FAILED,
  };
}

export function clearErrors() {
  return {
    type: CLEAR_REGISTRATION_ERRORS,
  };
}

export function loginUser({ username, password }) {
  return function fetchLogin(dispatch) {
    fetch(`${BASE_URL}/auth/login`, {
      method: 'post',
      body: JSON.stringify({ username, password }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => response.json()
      .then((json) => {
        if (response.ok) {
          dispatch(authUser(jwtDecode(json.token)));
          window.localStorage.setItem('token', json.token);
          browserHistory.push('/surveys');
        } else {
          dispatch(loginFailed());
        }
      }));
  };
}

export function logoutUser() {
  return function logout(dispatch) {
    dispatch(dismissUser());
    window.localStorage.removeItem('token');
    browserHistory.push('/login');
  };
}

export function refreshToken() {
  return function refresh(dispatch) {
    const token = window.localStorage.token;

    if (token) {
      fetch(`${BASE_URL}/auth/refresh`, {
        method: 'get',
        headers: {
          Authorization: token,
        },
      }).then(response => response.json().then((json) => {
        if (response.ok) {
          dispatch(authUser(jwtDecode(json.token)));
          window.localStorage.setItem('token', json.token);
        }
      }));
    }
  };
}

export function initAuth() {
  return function auth(dispatch) {
    const token = window.localStorage.token;
    if (token) {
      const decoded = jwtDecode(token);

      const expires = decoded.exp * 1000; // expiration date is in seconds
      const now = new Date().getTime();
      const expired = now >= expires;

      if (expired) {
        window.localStorage.removeItem('token');
        dispatch(authUser(null));
      } else {
        dispatch(refreshToken());
        dispatch(authUser(decoded));
      }

      fetch(`${BASE_URL}/users/is-disabled/${decoded.sub}`, {
        method: 'get',
      }).then((r) => {
        r.text().then((res) => {
          const disabled = (res === 'true');
          if (disabled) {
            window.localStorage.removeItem('token');
            dispatch(authUser(null));
          }
        });
      });
    } else {
      dispatch(authUser(null));
    }
  };
}

export function clearRegistrationErrors() {
  return dispatch => dispatch(clearErrors());
}
