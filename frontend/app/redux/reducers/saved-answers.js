import update from 'react/lib/update';
import { ADD_SAVED_ANSWER } from '../actions/answers';

const DEFAULT_STATE = {};

export default function savedAnswers(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case ADD_SAVED_ANSWER:
      return update(state, {
        $set: { [action.answerCode]: action.data },
      });
    default:
      return state;
  }
}
