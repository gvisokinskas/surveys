import update from 'react/lib/update';
import { REQUEST_SURVEY_REPORTS } from '../actions/reports';

const DEFAULT_STATE = {};

export default function reports(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case REQUEST_SURVEY_REPORTS:
      return update(state, {
        $set: { [action.surveyCode]: action.data },
      });
    default:
      return state;
  }
}
