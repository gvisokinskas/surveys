import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import app from './app';
import users from './users';
import allowedEmails from './allowed-emails';
import surveys from './surveys';
import reports from './reports';
import userReports from './user-reports';
import savedAnswers from './saved-answers';
import excel from './excel';

import { LOGOUT_USER } from '../actions/app';

const appReducer = combineReducers({
  app,
  users,
  allowedEmails,
  surveys,
  reports,
  userReports,
  savedAnswers,
  excel,
  form: formReducer,
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT_USER) {
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};

export default rootReducer;
