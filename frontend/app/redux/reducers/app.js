import update from 'react/lib/update';
import { AUTH_USER, LOGIN_FAILED, CLEAR_REGISTRATION_ERRORS } from '../actions/app';
import { REGISTRATION_FAILED, REGISTRATION_PASSED } from '../actions/users';

const DEFAULT_STATE = {};

export default function app(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case AUTH_USER:
      return update(state, {
        currentUser: { $set: action.data },
        loginFailed: { $set: false },
      });
    case LOGIN_FAILED:
      return update(state, {
        loginFailed: { $set: true },
      });
    case REGISTRATION_FAILED:
      return update(state, {
        hideRegistrationModal: { $set: false },
        registrationErrors: { $set: action.errorMessages },
      });
    case REGISTRATION_PASSED:
      return update(state, {
        hideRegistrationModal: { $set: true },
        registrationErrors: { $set: {} },
      });
    case CLEAR_REGISTRATION_ERRORS:
      return update(state, {
        registrationErrors: { $set: {} },
      });
    default:
      return state;
  }
}
