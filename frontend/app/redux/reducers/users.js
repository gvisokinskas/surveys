import update from 'react/lib/update';

import { REQUEST_USERS_ALL, DISABLE_ENABLE_USER, REMOVE_USER, CREATE_USER } from '../actions/users';

const DEFAULT_STATE = [];

export default function users(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case REQUEST_USERS_ALL:
      return action.data;
    case DISABLE_ENABLE_USER:
      return update(state, {
        [action.index]: {
          enabled: { $set: !action.enabled },
        },
      });
    case REMOVE_USER:
      return update(state, { $splice: [[action.index, 1]] });
    case CREATE_USER:
      return update(state, { $push: [action.data] });
    default:
      return state;
  }
}
