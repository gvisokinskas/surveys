import update from 'react/lib/update';
import { REQUEST_SURVEYS_ALL, REQUEST_SURVEY, ADD_SURVEY, DELETE_SURVEY } from '../actions/surveys';

const DEFAULT_STATE = [];

export default function surveys(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case REQUEST_SURVEYS_ALL:
      return action.data;
    case REQUEST_SURVEY:
      if (!!state.findIndex(survey => survey.code === action.data.code)) {
        return update(state, { $push: [action.data] });
      }
    case ADD_SURVEY:
      return update(state, { $push: [action.data] });
    case DELETE_SURVEY:
      return update(state, { $splice: [[action.index, 1]] });
    default:
      return state;
  }
}
