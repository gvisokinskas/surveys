import update from 'react/lib/update';
import { SUBMIT_EXCEL } from '../actions/excel';

const DEFAULT_STATE = {};

export default function app(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case SUBMIT_EXCEL:
      return update(state, {
        uploadMessage: { $set: action.data },
        uploadTimestamp: { $set: new Date() },
      });
    default:
      return state;
  }
}
