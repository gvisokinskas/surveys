import update from 'react/lib/update';

import { REQUEST_ALLOWED_EMAILS_ALL, ADD_ALLOWED_EMAIL, REMOVE_ALLOWED_EMAIL } from '../actions/allowed-emails';

const DEFAULT_STATE = [];

export default function allowedEmails(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case REQUEST_ALLOWED_EMAILS_ALL:
      return action.data;
    case ADD_ALLOWED_EMAIL:
      return update(state, { $push: [action.data] });
    case REMOVE_ALLOWED_EMAIL:
      return update(state, { $splice: [[action.index, 1]] });
    default:
      return state;
  }
}
