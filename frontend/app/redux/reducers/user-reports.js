import update from 'react/lib/update';
import { REQUEST_USER_REPORT } from '../actions/reports';

const DEFAULT_STATE = {};

export default function userReports(state = DEFAULT_STATE, action) {
  switch (action.type) {
    case REQUEST_USER_REPORT:
      return update(state, {
        $set: { [action.answerCode]: action.data },
      });
    default:
      return state;
  }
}
