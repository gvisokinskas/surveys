import React from 'react';
import { connect } from 'react-redux';

export default function requireAuthentication(ComposedComponent) {
  const mapStateToProps = state => ({ authenticated: !!state.app.currentUser });

  class Authentication extends React.Component {

    componentWillMount() {
      if (!this.props.authenticated) {
        this.props.router.push('/login');
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authenticated) {
        this.props.router.push('/login');
      }
    }

    render() {
      if (this.props.authenticated) {
        return <ComposedComponent {...this.props} />;
      }
      return null;
    }
  }

  Authentication.propTypes = {
    authenticated: React.PropTypes.bool.isRequired,
    router: React.PropTypes.object.isRequired,
  };

  return connect(mapStateToProps)(Authentication);
}
