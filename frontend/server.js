const path = require('path');
const express = require('express');
const webpack = require('webpack');
const config = require('./webpack.config');
const request = require('request');
const chalk = require('chalk');

const app = express();
const compiler = webpack(config);
const registerRoutes = require('./mock-server/routes.js');
app.use(express.static(path.join(__dirname, '/dist/')));

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
}));

app.use(require('webpack-hot-middleware')(compiler));

registerRoutes(app);


app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(9000, 'localhost', (err) => {
    if (err) console.log(chalk.red(err));
    else {
        console.log(chalk.yellow('Listening at http://localhost:9000'));
    }
});
