package com.creative.button.surveys.service.dto.excel;

import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.security.JwtUser;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnswerDeserializationResult {
    private AnswerCollectionDTO answerCollectionDTO;
    private JwtUser jwtUser;
}
