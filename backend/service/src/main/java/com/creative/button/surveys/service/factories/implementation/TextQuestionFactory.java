package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.domain.questions.Text;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.questions.TextDTO;
import org.springframework.stereotype.Service;

@Service
public class TextQuestionFactory extends AbstractQuestionFactory {
    @Override
    public Question createQuestion(QuestionDTO questionDTO, Page page) {
        Text textQuestion = new Text();
        super.copyGenericProperties(textQuestion, questionDTO, page);
        return textQuestion;
    }

    @Override
    public QuestionDTO createQuestionDTO(Question question) {
        TextDTO textDTO = new TextDTO();
        super.copyGenericProperties(textDTO, question);
        return textDTO;
    }

    @Override
    public boolean canHandle(QuestionType questionType) {
        return questionType == QuestionType.TEXT;
    }
}
