package com.creative.button.surveys.service.dto.questions;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class OptionDTO {
    @NotNull
    @Size(min = 36, max = 36)
    private String code;

    @NotNull
    @Size(min = 1)
    private String value;
}
