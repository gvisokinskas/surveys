package com.creative.button.surveys.service.dto.generic;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthorityDTO {
    private String authorityName;
}
