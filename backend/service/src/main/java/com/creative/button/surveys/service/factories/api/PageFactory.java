package com.creative.button.surveys.service.factories.api;

import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.surveys.PageDTO;

public interface PageFactory {
    Page createPage(PageDTO pageDTO);

    PageDTO createPageDTO(Page page);
}
