package com.creative.button.surveys.service.services.api;

import com.creative.button.surveys.service.security.JwtUser;

import java.io.InputStream;

public interface ExcelService {
    InputStream export(String surveyCode);
    void importSurvey(InputStream inputStream, JwtUser jwtUser);
}
