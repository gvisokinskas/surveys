package com.creative.button.surveys.service.dto.questions;

import com.creative.button.surveys.domain.questions.Question;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
public class TextDTO extends QuestionDTO{
    public TextDTO() {
        this.setQuestionType(QuestionType.TEXT);
    }
}
