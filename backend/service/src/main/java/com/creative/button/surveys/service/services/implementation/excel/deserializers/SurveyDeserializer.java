package com.creative.button.surveys.service.services.implementation.excel.deserializers;

import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.excel.AnswerDeserializationResult;
import com.creative.button.surveys.service.dto.excel.PageDeserializationResult;
import com.creative.button.surveys.service.dto.excel.SurveyDeserializationResult;
import com.creative.button.surveys.service.dto.questions.ConditionDTO;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.security.JwtUserFactory;
import com.creative.button.surveys.service.services.implementation.excel.Constants;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Component
public class SurveyDeserializer {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PageDeserializer pageDeserializer;
    @Autowired
    private ConditionDeserializer conditionDeserializer;
    @Autowired
    private AnswerDeserializer answerDeserializer;

    public SurveyDeserializationResult deserialize(Workbook workbook, JwtUser jwtUser) {
        Sheet surveySheet = workbook.getSheet(Constants.SURVEY_SHEET);
        Row row = surveySheet.getRow(1);

        SurveyDTO surveyDTO = deserializeSurveyDTO(row);

        PageDeserializationResult pageDeserializationResult = pageDeserializer.deserialize(workbook);
        surveyDTO.setPages(pageDeserializationResult.getPages());

        // Conditions and answers can only be deserialized after every question is deserialized in order for references to be resolved
        deserializeConditions(workbook, surveyDTO, pageDeserializationResult.getReadableQuestionCodesToUUIDs());
        List<AnswerDeserializationResult> answers = answerDeserializer.deserialize(workbook, surveyDTO, pageDeserializationResult.getReadableQuestionCodesToUUIDs());

        return new SurveyDeserializationResult(surveyDTO, jwtUser, answers);
    }

    private JwtUser deserializeJwtUser(Row row) {
        Long userID = Long.valueOf(CellDeserializer.getStringCellValue(row.getCell(2)));
        User user = userRepository.findOne(userID);
        return user != null ? JwtUserFactory.create(user) : null;
    }

    private SurveyDTO deserializeSurveyDTO(Row row) {
        SurveyDTO surveyDTO = new SurveyDTO();

        surveyDTO.setCode(UUID.randomUUID().toString());
        surveyDTO.setLabel(CellDeserializer.getStringCellValue(row.getCell(0)));
        surveyDTO.setPublic(row.getCell(1).getBooleanCellValue());
        return surveyDTO;
    }

    private void deserializeConditions(Workbook workbook, SurveyDTO surveyDTO, Map<String, String> qcToUUIDs) {
        Map<String, List<ConditionDTO>> conditions = conditionDeserializer.deserialize(workbook, qcToUUIDs);
        surveyDTO.getPages().stream().map(p -> p.getQuestions().stream()).reduce(Stream::concat).orElseGet(Stream::empty).forEach(q -> {
            List<ConditionDTO> applicable = conditions.getOrDefault(q.getCode(), new ArrayList<>());
            q.setConditions(applicable);
        });
    }
}
