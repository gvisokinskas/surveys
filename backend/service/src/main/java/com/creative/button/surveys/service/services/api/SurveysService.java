package com.creative.button.surveys.service.services.api;

import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.security.JwtUser;

import java.util.List;

public interface SurveysService {
    SurveyDTO createSurvey(SurveyDTO survey, JwtUser jwtUser);

    SurveyDTO getSurvey(String surveyCode, JwtUser jwtUser);

    SurveyDTO getSurveyByAnswerCode(String answerCode);

    List<SurveyDTO> getSurveys(JwtUser jwtUser);

    SurveyDTO updateSurvey(SurveyDTO surveyDTO, JwtUser jwtUser);

    boolean deleteSurvey(String surveyCode, JwtUser jwtUser);

    boolean surveyWasUpdated(String surveyCode, Long currentVersion);
}
