package com.creative.button.surveys.service.dto.questions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SelectionDTO extends QuestionDTO {
    private boolean multiple;

    @Size(min = 1)
    @Valid
    private List<OptionDTO> options = new ArrayList<>();

    public SelectionDTO() {
        this.setQuestionType(QuestionType.SELECTION);
    }
}
