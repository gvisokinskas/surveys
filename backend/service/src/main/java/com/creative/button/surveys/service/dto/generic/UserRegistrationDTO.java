package com.creative.button.surveys.service.dto.generic;

import com.creative.button.surveys.service.validation.annotations.AllowedEmail;
import com.creative.button.surveys.service.validation.generic.GenericValidationService;
import com.creative.button.surveys.service.validation.annotations.UniqueEmail;
import com.creative.button.surveys.service.validation.annotations.UniqueUserName;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class UserRegistrationDTO {
    @NotNull
    @Size(min = 4, max = 50, message = "username must be between 4-50 symbols")
    @UniqueUserName
    private String username;

    @NotNull
    @Size(min = 8, message = "password must be at least 8 symbols")
    private String password;

    @NotNull
    @Size(min = 4, message = "at least 4 symbols must be entered")
    @Pattern(regexp = GenericValidationService.EMAIL_REGEX_STRING, message = "not a valid email address")
    @AllowedEmail
    @UniqueEmail
    private String email;

    public String getEmail() {
        return email.toLowerCase();
    }
}
