package com.creative.button.surveys.service.dto.questions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "questionType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RangeDTO.class, name = QuestionType.Constants.RANGE),
        @JsonSubTypes.Type(value = TextDTO.class, name = QuestionType.Constants.TEXT),
        @JsonSubTypes.Type(value = SelectionDTO.class, name = QuestionType.Constants.SELECTION)
})
public abstract class QuestionDTO {
    @NotNull
    @Size(min = 36, max = 36)
    private String code;

    @NotNull
    @Size(min = 4, max = 100)
    private String label;

    private boolean required;

    @Valid
    private List<ConditionDTO> conditions;
    private QuestionType questionType;
}
