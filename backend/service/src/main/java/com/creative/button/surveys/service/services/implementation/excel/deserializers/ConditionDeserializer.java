package com.creative.button.surveys.service.services.implementation.excel.deserializers;

import com.creative.button.surveys.service.dto.questions.ConditionDTO;
import com.creative.button.surveys.service.services.implementation.excel.Constants;
import lombok.AllArgsConstructor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ConditionDeserializer {
    public Map<String, List<ConditionDTO>> deserialize(Workbook workbook, Map<String, String> qcToUUIDs) {
        Sheet conditionSheet = workbook.getSheet(Constants.CONDITIONS);
        Map<String, List<ConditionDTO>> result = new HashMap<>();

        for (int i = 1; i <= conditionSheet.getLastRowNum(); i++) {
            RowDeserializationResult deserializationResult = deserialize(conditionSheet.getRow(i), qcToUUIDs);
            safePut(result, deserializationResult);
        }

        return result;
    }

    private RowDeserializationResult deserialize(Row row, Map<String, String> qcToUUIDs) {
        String questionCode = CellDeserializer.getStringCellValue(row.getCell(0));
        String dependingQuestionCode = CellDeserializer.getStringCellValue(row.getCell(1));
        String valueCode = CellDeserializer.getStringCellValue(row.getCell(2));

        ConditionDTO conditionDTO = new ConditionDTO();
        conditionDTO.setQuestionCode(qcToUUIDs.get(dependingQuestionCode));
        conditionDTO.setValueCode(valueCode);

        return new RowDeserializationResult(qcToUUIDs.get(questionCode), conditionDTO);
    }

    private void safePut(Map<String, List<ConditionDTO>> conditions, RowDeserializationResult result) {
        List<ConditionDTO> existing = conditions.getOrDefault(result.questionCode, new ArrayList<>());
        existing.add(result.conditionDTO);
        conditions.put(result.questionCode, existing);
    }

    @AllArgsConstructor
    private class RowDeserializationResult {
        private String questionCode;
        private ConditionDTO conditionDTO;
    }
}
