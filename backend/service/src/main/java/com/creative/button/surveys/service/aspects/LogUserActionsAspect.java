package com.creative.button.surveys.service.aspects;

import com.creative.button.surveys.domain.action.PerformedAction;
import com.creative.button.surveys.domain.action.PerformedActionRepository;
import com.creative.button.surveys.service.security.JwtUser;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Aspect
@Component
public class LogUserActionsAspect {

    @Value("${user.log.enabled:false}")
    private boolean userLogEnabled;

    @Autowired
    private PerformedActionRepository performedActionRepository;

    @AfterReturning(
            pointcut = "execution(public * com.creative.button.surveys.service.services.api.UsersService+.*(..))",
            returning = "res"
    )
    public void log(JoinPoint jp, Object res) {
        if (!userLogEnabled) return;

        if (res == null || (res instanceof Boolean && !((Boolean) res))) return;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) return;

        JwtUser user = (JwtUser) authentication.getPrincipal();
        String methodName = jp.getSignature().getName();

        PerformedAction performedAction = new PerformedAction();
        performedAction.setAdmin(user.isAdmin());
        performedAction.setDate(new Date());
        performedAction.setMethodName(methodName);
        performedAction.setUserName(user.getUsername());

        performedAction.setArguments(mapArgs(jp.getArgs()));

        performedActionRepository.save(performedAction);
    }

    private List<String> mapArgs(Object[] args) {
        return Stream.of(args).map(Object::toString).collect(Collectors.toList());
    }
}
