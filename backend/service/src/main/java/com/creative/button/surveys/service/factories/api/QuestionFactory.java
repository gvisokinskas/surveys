package com.creative.button.surveys.service.factories.api;

import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;

public interface QuestionFactory {
    Question createQuestion(QuestionDTO questionDTO, Page page);

    QuestionDTO createQuestionDTO(Question question);

    boolean canHandle(QuestionType questionType);
}
