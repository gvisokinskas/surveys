package com.creative.button.surveys.service.validation.annotations;

import com.creative.button.surveys.service.validation.validators.AllowedEmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AllowedEmailValidator.class)
@Documented
public @interface AllowedEmail {
    String message() default "Email is not allowed for registration";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}