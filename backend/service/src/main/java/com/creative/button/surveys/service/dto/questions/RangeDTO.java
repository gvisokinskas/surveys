package com.creative.button.surveys.service.dto.questions;

import com.creative.button.surveys.service.validation.annotations.RangeStartSmallerThanEnd;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;

@EqualsAndHashCode(callSuper = true)
@Data
@RangeStartSmallerThanEnd
public class RangeDTO extends QuestionDTO {

    @Min(0)
    private int rangeStart;

    @Min(1)
    private int rangeEnd;

    public RangeDTO() {
        this.setQuestionType(QuestionType.RANGE);
    }
}
