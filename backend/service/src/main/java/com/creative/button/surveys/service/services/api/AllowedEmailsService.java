package com.creative.button.surveys.service.services.api;

import com.creative.button.surveys.service.dto.generic.AllowedEmailDTO;

import java.util.List;

public interface AllowedEmailsService {
    List<AllowedEmailDTO> getAllowedEmails();

    boolean save(AllowedEmailDTO allowedEmailDTO);

    boolean remove(AllowedEmailDTO allowedEmailDTO);
}
