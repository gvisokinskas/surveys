package com.creative.button.surveys.service.services.api;

import com.creative.button.surveys.service.dto.reports.SurveyReport;
import com.creative.button.surveys.service.security.JwtUser;

public interface ReportsService {
    SurveyReport getSingleAnswerReport(String surveyCode, String answerCollectionCode, JwtUser jwtUser);

    SurveyReport getSurveyReport(String surveyCode, JwtUser jwtUser);
}
