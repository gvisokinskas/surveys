package com.creative.button.surveys.service.services.implementation.reports;

import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.answers.Answer;
import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.questions.*;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.reports.AnswerTuple;
import com.creative.button.surveys.service.dto.reports.QuestionReport;
import com.creative.button.surveys.service.dto.reports.SurveyReport;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SurveyReportVisitor implements SurveyVisitor {
    private final SurveyReport surveyReport;
    private final Map<String, List<Answer>> answersByQuestionCode;
    private final List<QuestionReport> questionReports = new ArrayList<>();

    public SurveyReportVisitor(List<AnswerCollection> answerCollections) {
        this.surveyReport = new SurveyReport(answerCollections.size(), answerCollections.stream().map(AnswerCollection::getSubmissionDate).max(Date::compareTo).orElse(null), new ArrayList<>());
        this.answersByQuestionCode = answerCollections.stream().map(answerCollection -> answerCollection.getAnswers().values().stream()).reduce(Stream::concat).orElseGet(Stream::empty).collect(Collectors.groupingBy(Answer::getQuestionCode));
    }

    @Override
    public void visit(Survey survey) {

    }

    @Override
    public void visit(Page page) {

    }

    @Override
    public void visit(Condition condition) {

    }
    
    @Override
    public void visit(Selection selection) {
        QuestionReport questionReport = getReport(selection, QuestionType.SELECTION);
        Map<String, AnswerTuple> countableAnswers = questionReport.getCountableAnswers();

        Map<String, Long> count = getAnswersStream(selection).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        selection.getOptions().forEach(option -> {
            countableAnswers.put(option.getValue(), new AnswerTuple(option.getValue(), count.getOrDefault(option.getValue(), 0L).intValue()));
        });
    }

    @Override
    public void visit(Text text) {
        QuestionReport questionReport = getReport(text, QuestionType.TEXT);
        questionReport.getTextAnswers().addAll(getAnswersStream(text).collect(Collectors.toList()));
    }

    @Override
    public void visit(Range range) {
        QuestionReport questionReport = getReport(range, QuestionType.RANGE);
        Map<String, AnswerTuple> countableAnswers = questionReport.getCountableAnswers();

        Map<String, Long> count = getAnswersStream(range).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for (int i = range.getStart(); i <= range.getEnd(); i++) {
            String optionValue = String.valueOf(i);
            countableAnswers.put(optionValue, new AnswerTuple(optionValue, count.getOrDefault(optionValue, 0L).intValue()));
        }
    }

    public SurveyReport getSurveyReport() {
        surveyReport.setQuestionReports(questionReports);
        return surveyReport;
    }

    private QuestionReport getReport(Question question, QuestionType questionType) {
        QuestionReport defaultReport = new QuestionReport();
        defaultReport.setQuestionLabel(question.getLabel());
        defaultReport.setQuestionType(questionType);
        defaultReport.setCountableAnswers(new HashMap<>());
        defaultReport.setTextAnswers(new ArrayList<>());
        questionReports.add(defaultReport);
        return defaultReport;
    }

    private Stream<String> getAnswersStream(Question question) {
        return answersByQuestionCode.getOrDefault(question.getCode(), new ArrayList<>()).stream().map(Answer::getValue).flatMap(List::stream);
    }
}
