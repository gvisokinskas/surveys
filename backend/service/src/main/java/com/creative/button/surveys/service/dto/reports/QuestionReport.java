package com.creative.button.surveys.service.dto.reports;

import com.creative.button.surveys.service.dto.questions.QuestionType;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class QuestionReport {
    private String questionLabel;
    private List<String> textAnswers;
    private Map<String, AnswerTuple> countableAnswers;
    private QuestionType questionType;
}
