package com.creative.button.surveys.service.factories.api;

import com.creative.button.surveys.domain.questions.Condition;
import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.service.dto.questions.ConditionDTO;

public interface ConditionFactory {
    Condition createCondition(ConditionDTO conditionDTO, Question question);
    ConditionDTO createConditionDTO(Condition condition);
}
