package com.creative.button.surveys.service.services.implementation.answers;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PrivateUnregisteredUserHandler extends AnswerHandler {

    @Autowired
    private PrivateRegisteredUserHandler privateRegisteredUserHandler;

    @Override
    public boolean canHandle(Long userId, Survey survey) {
        return userId == null && !survey.isPublic();
    }

    @Override
    protected String doHandleInternal(AnswerCollectionDTO answerCollectionDTO, Long userId, Survey survey, AnswerCollection unfinishedAnswer) {
        if (unfinishedAnswer != null) {
            return privateRegisteredUserHandler.doHandleInternal(answerCollectionDTO, unfinishedAnswer.getUserId(), survey, unfinishedAnswer);
        }

        return null;
    }

    @Override
    public int getOrder() {
        return 2;
    }
}
