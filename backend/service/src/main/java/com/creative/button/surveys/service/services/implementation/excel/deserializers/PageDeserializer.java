package com.creative.button.surveys.service.services.implementation.excel.deserializers;

import com.creative.button.surveys.service.dto.excel.PageDeserializationResult;
import com.creative.button.surveys.service.dto.surveys.PageDTO;
import com.creative.button.surveys.service.services.implementation.excel.Constants;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PageDeserializer {

    @Autowired
    private QuestionDeserializer questionDeserializer;

    public PageDeserializationResult deserialize(Workbook workbook) {
        Sheet pagesSheet = workbook.getSheet(Constants.PAGES_SHEET);
        List<PageDTO> result = new ArrayList<>();
        Map<String, String> qcToUUIDs = new HashMap<>();

        for (int i = 1; i <= pagesSheet.getLastRowNum(); i++) {
            result.add(deserialize(pagesSheet.getRow(i), workbook, qcToUUIDs));
        }

        return new PageDeserializationResult(result, qcToUUIDs);
    }

    private PageDTO deserialize(Row row, Workbook workbook, Map<String, String> qcToUUIDs) {
        PageDTO pageDTO = new PageDTO();

        String pageCode = CellDeserializer.getStringCellValue(row.getCell(0));
        pageDTO.setCode(UUID.randomUUID().toString());
        pageDTO.setLabel(CellDeserializer.getStringCellValue(row.getCell(1)));
        pageDTO.setQuestions(questionDeserializer.deserialize(workbook, pageCode, qcToUUIDs));

        return pageDTO;
    }
}
