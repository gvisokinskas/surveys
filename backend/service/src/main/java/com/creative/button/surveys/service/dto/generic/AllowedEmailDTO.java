package com.creative.button.surveys.service.dto.generic;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AllowedEmailDTO {
    private String value;

    public String getValue() {
        return value.toLowerCase();
    }
}
