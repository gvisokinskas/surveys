package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.surveys.SurveyRepository;
import com.creative.button.surveys.service.validation.annotations.SurveyExists;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SurveyExistsValidator implements ConstraintValidator<SurveyExists, String> {

    @Autowired
    private SurveyRepository surveyRepository;

    @Override
    public void initialize(SurveyExists constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return surveyRepository.findOne(value) != null;
    }
}
