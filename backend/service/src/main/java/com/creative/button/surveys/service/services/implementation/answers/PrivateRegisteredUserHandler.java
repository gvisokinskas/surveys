package com.creative.button.surveys.service.services.implementation.answers;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import org.springframework.stereotype.Component;

@Component
public class PrivateRegisteredUserHandler extends AnswerHandler {

    @Override
    public boolean canHandle(Long userId, Survey survey) {
        return userId != null && !survey.isPublic();
    }

    @Override
    protected String doHandleInternal(AnswerCollectionDTO answerCollectionDTO, Long userId, Survey survey, AnswerCollection unfinishedAnswer) {
        if (unfinishedAnswer != null && !userId.equals(unfinishedAnswer.getUserId())) {
            userId = unfinishedAnswer.getUserId();
        }

        AnswerCollection existingAnswer = answerCollectionRepository.findByUserIdAndSurvey(userId, survey);

        if (existingAnswer != null) {
            if (existingAnswer.isFinished()) {
                return null;
            }

            answerCollectionRepository.delete(existingAnswer);
        }

        AnswerCollection answerCollection = createNewAnswer(answerCollectionDTO, userId, survey);
        answerCollectionRepository.save(answerCollection);
        return answerCollection.getCode();
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
