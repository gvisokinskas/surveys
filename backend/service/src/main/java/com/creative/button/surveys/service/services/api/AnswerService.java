package com.creative.button.surveys.service.services.api;

import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.security.JwtUser;

public interface AnswerService {
    String saveAnswer(AnswerCollectionDTO answerCollectionDTO, JwtUser jwtUser);

    AnswerCollectionDTO getIncompleteAnswer(String answerCode, JwtUser jwtUser);
}
