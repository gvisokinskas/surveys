package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.validation.annotations.UniqueUserName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UsernameValidator implements ConstraintValidator<UniqueUserName, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(UniqueUserName constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return userRepository.findByUsername(value) == null;
    }
}
