package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.domain.questions.Selection;
import com.creative.button.surveys.domain.questions.SelectionOption;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.questions.OptionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.questions.SelectionDTO;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class SelectionQuestionFactory extends AbstractQuestionFactory {
    @Override
    public Question createQuestion(QuestionDTO questionDTO, Page page) {
        SelectionDTO selectionDTO = (SelectionDTO) questionDTO;
        Selection selectionQuestion = new Selection();
        super.copyGenericProperties(selectionQuestion, questionDTO, page);
        selectionQuestion.setMultiple(selectionDTO.isMultiple());
        selectionQuestion.setOptions(selectionDTO.getOptions().stream().map(this::createSelectionOption).collect(Collectors.toList()));
        return selectionQuestion;
    }

    @Override
    public QuestionDTO createQuestionDTO(Question question) {
        Selection selection = (Selection) question;
        SelectionDTO selectionDTO = new SelectionDTO();
        super.copyGenericProperties(selectionDTO, selection);
        selectionDTO.setMultiple(selection.isMultiple());
        selectionDTO.setOptions(selection.getOptions().stream().map(this::createSelectionOptionDTO).collect(Collectors.toList()));
        return selectionDTO;
    }

    @Override
    public boolean canHandle(QuestionType questionType) {
        return questionType == QuestionType.SELECTION;
    }

    private SelectionOption createSelectionOption(OptionDTO optionDTO) {
        SelectionOption selectionOption = new SelectionOption();
        selectionOption.setValue(optionDTO.getValue());
        selectionOption.setCode(optionDTO.getCode());
        return selectionOption;
    }

    private OptionDTO createSelectionOptionDTO(SelectionOption selectionOption) {
        OptionDTO selectionOptionDTO = new OptionDTO();
        selectionOptionDTO.setValue(selectionOption.getValue());
        selectionOptionDTO.setCode(selectionOption.getCode());
        return selectionOptionDTO;
    }
}
