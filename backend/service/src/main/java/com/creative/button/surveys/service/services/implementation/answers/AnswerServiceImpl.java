package com.creative.button.surveys.service.services.implementation.answers;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.answers.AnswerCollectionRepository;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.domain.surveys.SurveyRepository;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.factories.api.AnswerFactory;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private Collection<AnswerHandler> answerHandlers;
    @Autowired
    private AnswerCollectionRepository answerCollectionRepository;
    @Autowired
    private AnswerFactory answerFactory;
    @Autowired
    private SurveyRepository surveyRepository;

    @Override
    public String saveAnswer(AnswerCollectionDTO answerCollectionDTO, JwtUser jwtUser) {
        Survey survey = surveyRepository.findOne(answerCollectionDTO.getSurveyCode());

        if (answerCollectionDTO.isFinished()) {
            try {
                survey.accept(new AnswerValidationVisitor(answerCollectionDTO));
            } catch (SurveyValidationException e) {
                return null;
            }
        }

        for (AnswerHandler answerHandler : answerHandlers) {
            if (answerHandler.canHandle(jwtUser != null ? jwtUser.getId() : null, survey)) {
                return answerHandler.handle(answerCollectionDTO, jwtUser, survey);
            }
        }

        return null;
    }

    @Override
    public AnswerCollectionDTO getIncompleteAnswer(String answerCode, JwtUser jwtUser) {
        AnswerCollection answerCollection = answerCollectionRepository.findOne(answerCode);

        if (answerCollection == null || answerCollection.isFinished()) {
            return null;
        }

        return answerFactory.createAnswerCollectionDTO(answerCollection);
    }
}
