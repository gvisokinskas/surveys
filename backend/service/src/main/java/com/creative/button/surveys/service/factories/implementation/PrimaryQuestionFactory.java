package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.domain.questions.Range;
import com.creative.button.surveys.domain.questions.Selection;
import com.creative.button.surveys.domain.questions.Text;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.factories.api.QuestionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Primary
public class PrimaryQuestionFactory implements QuestionFactory {

    private static final Map<Class<? extends Question>, QuestionType> QUESTION_TYPE_MAP = new HashMap<Class<? extends Question>, QuestionType>() {{
        put(Text.class, QuestionType.TEXT);
        put(Range.class, QuestionType.RANGE);
        put(Selection.class, QuestionType.SELECTION);
    }};

    @Autowired
    private List<AbstractQuestionFactory> factories;

    @Override
    public Question createQuestion(QuestionDTO q, Page page) {
        return factories.stream().filter(f -> f.canHandle(q.getQuestionType())).findFirst().get().createQuestion(q, page);
    }

    @Override
    public QuestionDTO createQuestionDTO(Question q) {
        return factories.stream().filter(f -> f.canHandle(QUESTION_TYPE_MAP.get(q.getClass()))).findFirst().get().createQuestionDTO(q);
    }

    @Override
    public boolean canHandle(QuestionType questionType) {
        return true;
    }
}
