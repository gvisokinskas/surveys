package com.creative.button.surveys.service.services.implementation.reports;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.answers.AnswerCollectionRepository;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.domain.surveys.SurveyRepository;
import com.creative.button.surveys.service.dto.reports.SurveyReport;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportsServiceImpl implements ReportsService {

    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private AnswerCollectionRepository answerCollectionRepository;

    @Override
    public SurveyReport getSingleAnswerReport(String surveyCode, String answerCollectionCode, JwtUser jwtUser) {
        Survey survey = surveyRepository.findOne(surveyCode);
        if (survey == null) {
            return null;
        }

        if (!(survey.isPublic() || jwtUser.isAdmin())) {
            if (!survey.getUserId().equals(jwtUser.getId())) {
                return null;
            }
        }

        AnswerCollection answerCollection = answerCollectionRepository.findOne(answerCollectionCode);
        if (answerCollection == null || !answerCollection.isFinished()) {
            return null;
        }

        SurveyReportVisitor surveyReportVisitor = new SurveyReportVisitor(Collections.singletonList(answerCollection));
        survey.accept(surveyReportVisitor);

        return surveyReportVisitor.getSurveyReport();
    }

    @Override
    public SurveyReport getSurveyReport(String surveyCode, JwtUser jwtUser) {
        Survey survey = surveyRepository.findOne(surveyCode);
        if (survey == null) {
            return null;
        }

        if (!(survey.isPublic() || jwtUser.isAdmin())) {
            if (!survey.getUserId().equals(jwtUser.getId())) {
                return null;
            }
        }

        List<AnswerCollection> finishedAnswers = survey.getAnswers().stream().filter(AnswerCollection::isFinished).collect(Collectors.toList());
        SurveyReportVisitor surveyReportVisitor = new SurveyReportVisitor(finishedAnswers);
        survey.accept(surveyReportVisitor);
        return surveyReportVisitor.getSurveyReport();
    }
}
