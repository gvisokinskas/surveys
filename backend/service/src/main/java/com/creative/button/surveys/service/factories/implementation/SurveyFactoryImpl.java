package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.CodeBasedEntity;
import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.factories.api.PageFactory;
import com.creative.button.surveys.service.factories.api.SurveyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class SurveyFactoryImpl implements SurveyFactory {

    @Autowired
    private PageFactory pageFactory;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Survey createSurvey(SurveyDTO surveyDTO, Long userId) {
        Survey survey = new Survey();
        survey.setUserId(userId);
        survey.setLabel(surveyDTO.getLabel());
        survey.setAnswers(new ArrayList<>());
        survey.setPages(surveyDTO.getPages().stream().map(pageFactory::createPage).collect(Collectors.toList()));
        survey.setPublic(surveyDTO.isPublic());
        survey.setCode(surveyDTO.getCode());
        survey.setVersion(surveyDTO.getVersion() == null ? 1L : surveyDTO.getVersion());
        return survey;
    }

    @Override
    public SurveyDTO createSurveyDTO(Survey survey) {
        Map<Long, User> allUsers = userRepository.findAll().stream().collect(Collectors.toMap(User::getId, Function.identity()));

        SurveyDTO surveyDTO = new SurveyDTO();
        surveyDTO.setPages(survey.getPages().stream().map(pageFactory::createPageDTO).collect(Collectors.toList()));
        surveyDTO.setPublic(survey.isPublic());
        surveyDTO.setCode(survey.getCode());
        surveyDTO.setLabel(survey.getLabel());
        surveyDTO.setVersion(survey.getVersion());

        surveyDTO.setUsername(allUsers.containsKey(survey.getUserId()) ? allUsers.get(survey.getUserId()).getUsername() : "");

        Predicate<AnswerCollection> hasUserPredicate = a -> a.getUserId() != null && allUsers.keySet().contains(a.getUserId());
        Predicate<AnswerCollection> finished = AnswerCollection::isFinished;
        surveyDTO.setAnswersByUser(survey.getAnswers().stream().filter(finished).filter(hasUserPredicate).collect(Collectors.toMap(a -> allUsers.get(a.getUserId()).getUsername(), CodeBasedEntity::getCode)));
        surveyDTO.setAnonymousAnswers(survey.getAnswers().stream().filter(finished).filter(hasUserPredicate.negate()).map(CodeBasedEntity::getCode).collect(Collectors.toList()));

        return surveyDTO;
    }
}
