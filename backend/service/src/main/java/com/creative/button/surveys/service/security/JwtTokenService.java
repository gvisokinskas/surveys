package com.creative.button.surveys.service.security;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtTokenService {
    String getUsernameFromToken(String token);

    Boolean isTokenExpired(String token);

    String generateToken(UserDetails userDetails);

    String refreshToken(String token);

    Boolean validateToken(String token, UserDetails userDetails);
}
