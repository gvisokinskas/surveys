package com.creative.button.surveys.service.dto.excel;

import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.security.JwtUser;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class SurveyDeserializationResult {
    private SurveyDTO surveyDTO;
    private JwtUser jwtUser;
    private List<AnswerDeserializationResult> answers;
}
