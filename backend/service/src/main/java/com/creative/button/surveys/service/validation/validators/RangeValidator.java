package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.service.dto.questions.RangeDTO;
import com.creative.button.surveys.service.validation.annotations.RangeStartSmallerThanEnd;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RangeValidator implements ConstraintValidator<RangeStartSmallerThanEnd, RangeDTO> {
    @Override
    public void initialize(RangeStartSmallerThanEnd constraintAnnotation) {

    }

    @Override
    public boolean isValid(RangeDTO value, ConstraintValidatorContext context) {
        return value.getRangeStart() < value.getRangeEnd();
    }
}
