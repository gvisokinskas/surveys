package com.creative.button.surveys.service.validation.annotations;

import com.creative.button.surveys.service.validation.validators.RangeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RangeValidator.class)
@Documented
public @interface RangeStartSmallerThanEnd {
    String message() default "Range start should be smaller than range end";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
