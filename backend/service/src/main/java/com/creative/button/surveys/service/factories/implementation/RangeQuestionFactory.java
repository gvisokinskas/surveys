package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.domain.questions.Range;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.questions.RangeDTO;
import org.springframework.stereotype.Service;

@Service
public class RangeQuestionFactory extends AbstractQuestionFactory {
    @Override
    public Question createQuestion(QuestionDTO questionDTO, Page page) {
        RangeDTO rangeDTO = (RangeDTO) questionDTO;
        Range rangeQuestion = new Range();
        super.copyGenericProperties(rangeQuestion, questionDTO, page);
        rangeQuestion.setEnd(rangeDTO.getRangeEnd());
        rangeQuestion.setStart(rangeDTO.getRangeStart());
        return rangeQuestion;
    }

    @Override
    public QuestionDTO createQuestionDTO(Question question) {
        Range range = (Range) question;
        RangeDTO rangeDTO = new RangeDTO();
        super.copyGenericProperties(rangeDTO, range);
        rangeDTO.setRangeEnd(range.getEnd());
        rangeDTO.setRangeStart(range.getStart());
        return rangeDTO;
    }

    @Override
    public boolean canHandle(QuestionType questionType) {
        return questionType == QuestionType.RANGE;
    }
}
