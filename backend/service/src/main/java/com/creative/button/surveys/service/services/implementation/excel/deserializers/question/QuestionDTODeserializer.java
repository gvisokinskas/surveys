package com.creative.button.surveys.service.services.implementation.excel.deserializers.question;

import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;

import java.util.List;

public interface QuestionDTODeserializer {
    boolean canDeserialize(QuestionType questionType);

    QuestionDTO deserialize(List<String> options);
}
