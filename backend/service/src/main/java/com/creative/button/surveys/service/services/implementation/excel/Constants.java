package com.creative.button.surveys.service.services.implementation.excel;

public final class Constants {
    // General
    public static String CODE = "Code";
    public static String LABEL = "Label";
    public static String USER_ID = "User ID";
    public static String QUESTION_CODE = "Question Code";

    // Survey
    public static String SURVEY_SHEET = "Survey";
    public static String PUBLIC = "Public";

    // Pages
    public static String PAGES_SHEET = "Pages";

    // Questions
    public static String QUESTIONS_SHEET = "Questions";
    public static String REQUIRED = "Required";
    public static String PAGE_CODE = "Page Code";
    public static String TYPE = "Type";
    public static String OPTIONS = "Options";

    // Answers
    public static String ANSWERS_SHEET = "Answers";
    public static String FINISHED = "Finished";
    public static String SUBMISSION_DATE = "Submission Date";

    // Concrete answers
    public static String CONCRETE_ANSWERS_SHEET = "Concrete Answers";
    public static String ANSWER_CODE = "Answer Code";
    public static String VALUES = "Values";

    // Conditions
    public static String CONDITIONS = "Conditions";
    public static String BELONGS_TO_QUESTION = "Belongs To Question";
    public static String VALUE_CODE = "Value Code";
}
