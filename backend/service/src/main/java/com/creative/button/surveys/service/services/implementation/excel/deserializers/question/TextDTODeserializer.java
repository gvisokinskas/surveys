package com.creative.button.surveys.service.services.implementation.excel.deserializers.question;

import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.questions.TextDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TextDTODeserializer implements QuestionDTODeserializer {
    @Override
    public boolean canDeserialize(QuestionType questionType) {
        return questionType == QuestionType.TEXT;
    }

    @Override
    public QuestionDTO deserialize(List<String> options) {
        return new TextDTO();
    }
}
