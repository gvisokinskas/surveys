package com.creative.button.surveys.service.security;

import com.creative.button.surveys.domain.authorities.Authority;
import com.creative.button.surveys.domain.authorities.AuthorityName;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@AllArgsConstructor
public class JwtUser implements UserDetails {

    private final Collection<? extends GrantedAuthority> authorities;
    private Long id;
    private String username;
    private String password;
    private String email;
    private boolean enabled;

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isAdmin() {
        return authorities.stream().anyMatch(auth -> auth.getAuthority().equals(AuthorityName.ROLE_ADMIN.name()));
    }
}
