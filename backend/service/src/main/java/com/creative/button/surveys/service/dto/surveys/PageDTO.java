package com.creative.button.surveys.service.dto.surveys;

import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PageDTO {
    @NotNull
    @Size(min = 36, max = 36)
    private String code;

    @NotNull
    @Size(min = 4, max = 100)
    private String label;

    @NotNull
    @Valid
    private List<QuestionDTO> questions = new ArrayList<>();
}
