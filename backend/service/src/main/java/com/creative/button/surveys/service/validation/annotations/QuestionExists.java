package com.creative.button.surveys.service.validation.annotations;

import com.creative.button.surveys.service.validation.validators.QuestionExistsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = QuestionExistsValidator.class)
@Documented
public @interface QuestionExists {
    String message() default "Not existing question";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
