package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.questions.ConditionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.factories.api.ConditionFactory;
import com.creative.button.surveys.service.factories.api.QuestionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public abstract class AbstractQuestionFactory implements QuestionFactory {

    @Autowired
    private ConditionFactory conditionFactory;

    protected <T extends Question> void copyGenericProperties(T question, QuestionDTO questionDTO, Page page) {
        question.setCode(questionDTO.getCode());
        Collection<ConditionDTO> conditions = questionDTO.getConditions() != null ? questionDTO.getConditions() : new ArrayList<>();
        question.setConditions(conditions.stream().map(conditionDTO -> conditionFactory.createCondition(conditionDTO, question)).collect(Collectors.toList()));
        question.setLabel(questionDTO.getLabel());
        question.setPage(page);
        question.setRequired(questionDTO.isRequired());
    }

    protected <T extends QuestionDTO> void copyGenericProperties(T questionDTO, Question question) {
        questionDTO.setCode(question.getCode());
        questionDTO.setConditions(question.getConditions().stream().map(conditionFactory::createConditionDTO).collect(Collectors.toList()));
        questionDTO.setLabel(question.getLabel());
        questionDTO.setRequired(question.isRequired());
    }
}
