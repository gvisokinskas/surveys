package com.creative.button.surveys.service.validation.generic;

import org.springframework.stereotype.Service;

@Service
public class GenericValidationServiceImpl implements GenericValidationService {

    @Override
    public boolean isEmailValid(String email) {
        return email != null && EMAIL_REGEX_PATTERN.matcher(email).matches();
    }
}
