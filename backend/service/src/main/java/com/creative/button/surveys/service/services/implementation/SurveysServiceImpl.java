package com.creative.button.surveys.service.services.implementation;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.answers.AnswerCollectionRepository;
import com.creative.button.surveys.domain.questions.QuestionRepository;
import com.creative.button.surveys.domain.surveys.PageRepository;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.domain.surveys.SurveyRepository;
import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.factories.api.SurveyFactory;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.security.JwtUserFactory;
import com.creative.button.surveys.service.services.api.SurveysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SurveysServiceImpl implements SurveysService {

    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PageRepository pageRepository;
    @Autowired
    private AnswerCollectionRepository answerCollectionRepository;

    @Autowired
    private SurveyFactory surveyFactory;

    @Override
    public SurveyDTO createSurvey(SurveyDTO survey, JwtUser jwtUser) {
        if (partsOfSurveyExist(survey)) {
            return null;
        }
        Survey domainSurvey = surveyFactory.createSurvey(survey, jwtUser != null ? jwtUser.getId() : null);
        surveyRepository.save(domainSurvey);
        return surveyFactory.createSurveyDTO(domainSurvey);
    }

    @Override
    public SurveyDTO getSurvey(String surveyCode, JwtUser jwtUser) {
        Survey domainSurvey = surveyRepository.findOne(surveyCode);
        if (domainSurvey == null || (jwtUser == null && !domainSurvey.isPublic())) {
            return null;
        }
        return surveyFactory.createSurveyDTO(domainSurvey);
    }

    @Override
    public SurveyDTO getSurveyByAnswerCode(String answerCode) {
        AnswerCollection answerCollection = answerCollectionRepository.findOne(answerCode);
        if (answerCollection == null) {
            return null;
        }
        return surveyFactory.createSurveyDTO(answerCollection.getSurvey());
    }

    @Override
    public List<SurveyDTO> getSurveys(JwtUser jwtUser) {
        if (jwtUser.isAdmin()) {
            return surveyRepository.findAll().stream().map(surveyFactory::createSurveyDTO).collect(Collectors.toList());
        }
        return surveyRepository.findByUserIdOrIsPublic(jwtUser.getId(), true).stream().map(surveyFactory::createSurveyDTO).collect(Collectors.toList());
    }

    @Override
    public SurveyDTO updateSurvey(SurveyDTO surveyDTO, JwtUser jwtUser) {
        Survey survey = surveyRepository.findOne(surveyDTO.getCode());

        if (survey == null || survey.getAnswers().size() != 0 || (!survey.getUserId().equals(jwtUser.getId()) && !jwtUser.isAdmin())) {
            return null;
        }

        surveyRepository.delete(survey);

        surveyDTO.setVersion(surveyDTO.getVersion() + 1);

        User user = userRepository.findOne(survey.getUserId());
        if (user != null) {
            return createSurvey(surveyDTO, JwtUserFactory.create(user));
        }
        return createSurvey(surveyDTO, jwtUser);
    }

    @Override
    public boolean deleteSurvey(String surveyCode, JwtUser jwtUser) {
        Survey survey = surveyRepository.findOne(surveyCode);

        if (survey == null || (!survey.getUserId().equals(jwtUser.getId()) && !jwtUser.isAdmin())) {
            return false;
        }

        surveyRepository.delete(survey);

        return true;
    }

    @Override
    public boolean surveyWasUpdated(String surveyCode, Long currentVersion) {
        Survey survey = surveyRepository.findOne(surveyCode);
        return survey == null || !Objects.equals(survey.getVersion(), currentVersion);
    }

    private boolean partsOfSurveyExist(SurveyDTO surveyDTO) {
        boolean surveyExists = surveyRepository.findOne(surveyDTO.getCode()) != null;
        boolean pagesExist = surveyDTO.getPages().stream().anyMatch(p -> pageRepository.findOne(p.getCode()) != null);
        boolean questionsExist = surveyDTO.getPages().stream().anyMatch(p -> p.getQuestions().stream().anyMatch(q -> questionRepository.findOne(q.getCode()) != null));

        return surveyExists || pagesExist || questionsExist;
    }
}