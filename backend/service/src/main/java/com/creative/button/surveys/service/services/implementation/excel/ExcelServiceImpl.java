package com.creative.button.surveys.service.services.implementation.excel;

import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.domain.surveys.SurveyRepository;
import com.creative.button.surveys.service.dto.excel.SurveyDeserializationResult;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.AnswerService;
import com.creative.button.surveys.service.services.api.ExcelService;
import com.creative.button.surveys.service.services.api.SurveysService;
import com.creative.button.surveys.service.services.implementation.answers.AnswerValidationVisitor;
import com.creative.button.surveys.service.services.implementation.answers.SurveyValidationException;
import com.creative.button.surveys.service.services.implementation.excel.deserializers.SurveyDeserializer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.*;
import java.util.Set;

@Service
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private SurveyDeserializer surveyDeserializer;

    @Autowired
    private SurveysService surveysService;
    @Autowired
    private AnswerService answerService;

    @Autowired
    private Validator validator;

    @Override
    public InputStream export(String surveyCode) {
        Survey survey = surveyRepository.getOne(surveyCode);

        if (survey == null) {
            return new ByteArrayInputStream(new byte[0]);
        }

        ExportingVisitor exportingVisitor = new ExportingVisitor(new XSSFWorkbook());

        survey.accept(exportingVisitor);
        exportingVisitor.exportAnswers(survey.getAnswers());

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            exportingVisitor.getWorkbook().write(os);
            os.close();
            return new ByteArrayInputStream(os.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void importSurvey(InputStream inputStream, JwtUser jwtUser) {
        try (InputStream is = new BufferedInputStream(inputStream)) {
            Workbook workbook = WorkbookFactory.create(is);
            SurveyDeserializationResult result = surveyDeserializer.deserialize(workbook, jwtUser);

            Set<ConstraintViolation<SurveyDTO>> violations = validator.validate(result.getSurveyDTO());
            if (violations.size() > 0) {
                throw new SurveyValidationException();
            }

            surveysService.createSurvey(result.getSurveyDTO(), result.getJwtUser());

            Survey domainSurvey = surveyRepository.getOne(result.getSurveyDTO().getCode());
            try {
                result.getAnswers().forEach(a -> domainSurvey.accept(new AnswerValidationVisitor(a.getAnswerCollectionDTO())));
            } catch (SurveyValidationException e) {
                surveyRepository.delete(domainSurvey);
                throw e;
            }

            result.getAnswers().forEach(a -> answerService.saveAnswer(a.getAnswerCollectionDTO(), a.getJwtUser()));
        } catch (IOException | InvalidFormatException e) {
            e.printStackTrace();
        }
    }
}
