package com.creative.button.surveys.service.factories.api;

import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;

public interface SurveyFactory {
    Survey createSurvey(SurveyDTO surveyDTO, Long userId);

    SurveyDTO createSurveyDTO(Survey survey);
}
