package com.creative.button.surveys.service.services.implementation.answers;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import org.springframework.stereotype.Component;

@Component
public class PublicHandler extends AnswerHandler {

    @Override
    public boolean canHandle(Long userId, Survey survey) {
        return survey.isPublic();
    }

    @Override
    public String doHandleInternal(AnswerCollectionDTO answerCollectionDTO, Long userId, Survey survey, AnswerCollection unfinishedAnswer) {
        AnswerCollection answerCollection = createNewAnswer(answerCollectionDTO, null, survey);
        answerCollectionRepository.save(answerCollection);
        return answerCollection.getCode();
    }

    @Override
    public int getOrder() {
        return 3;
    }
}
