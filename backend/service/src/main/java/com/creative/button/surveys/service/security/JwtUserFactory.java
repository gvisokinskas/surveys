package com.creative.button.surveys.service.security;

import com.creative.button.surveys.domain.authorities.Authority;
import com.creative.button.surveys.domain.users.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class JwtUserFactory {

    private JwtUserFactory() {

    }

    public static JwtUser create(User user) {
        return new JwtUser(mapToGrantedAuthorities(user.getAuthorities()), user.getId(), user.getUsername(), user.getPassword(), user.getEmail(), !user.isDisabled());
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName().name()))
                .collect(Collectors.toList());
    }
}
