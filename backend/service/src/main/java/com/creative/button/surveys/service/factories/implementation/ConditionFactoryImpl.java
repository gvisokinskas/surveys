package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.questions.Condition;
import com.creative.button.surveys.domain.questions.Question;
import com.creative.button.surveys.service.dto.questions.ConditionDTO;
import com.creative.button.surveys.service.factories.api.ConditionFactory;
import org.springframework.stereotype.Service;

@Service
public class ConditionFactoryImpl implements ConditionFactory {
    @Override
    public Condition createCondition(ConditionDTO conditionDTO, Question question) {
        Condition condition = new Condition();
        condition.setQuestionCode(conditionDTO.getQuestionCode());
        condition.setValueCode(conditionDTO.getValueCode());
        condition.setQuestion(question);
        return condition;
    }

    @Override
    public ConditionDTO createConditionDTO(Condition condition) {
        ConditionDTO conditionDTO = new ConditionDTO();
        conditionDTO.setQuestionCode(condition.getQuestionCode());
        conditionDTO.setValueCode(condition.getValueCode());
        return conditionDTO;
    }
}
