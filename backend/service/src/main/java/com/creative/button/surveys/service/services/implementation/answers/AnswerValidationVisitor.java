package com.creative.button.surveys.service.services.implementation.answers;

import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.questions.*;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.dto.answers.AnswerDTO;
import org.apache.poi.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AnswerValidationVisitor implements SurveyVisitor {
    private final Map<String, List<String>> codeToAnswerMap;

    public AnswerValidationVisitor(AnswerCollectionDTO answerCollectionDTO) {
        this.codeToAnswerMap = answerCollectionDTO.getAnswers().stream().collect(Collectors.toMap(AnswerDTO::getQuestionCode, AnswerDTO::getValue));
    }

    @Override
    public void visit(Survey survey) {

    }

    @Override
    public void visit(Page page) {

    }

    @Override
    public void visit(Selection selection) {
        List<String> options = selection.getOptions().stream().map(SelectionOption::getValue).collect(Collectors.toList());
        List<String> answers = safeGetAnswers(selection);
        validateRequired(selection, answers);

        if (!selection.isMultiple() && answers.size() > 1) {
            throw new SurveyValidationException();
        }

        if (answers.stream().anyMatch(a -> !options.contains(a))) {
            throw new SurveyValidationException();
        }
    }

    @Override
    public void visit(Text text) {
        List<String> answers = safeGetAnswers(text);
        validateRequired(text, answers);

        if (answers.size() > 1) {
            throw new SurveyValidationException();
        }
    }

    @Override
    public void visit(Range range) {
        List<String> options = IntStream.range(range.getStart(), range.getEnd() + 1).mapToObj(String::valueOf).collect(Collectors.toList());
        List<String> answers = safeGetAnswers(range);
        validateRequired(range, answers);

        if (answers.size() > 1) {
            throw new SurveyValidationException();
        }

        if (answers.stream().anyMatch(a -> !options.contains(a))) {
            throw new SurveyValidationException();
        }
    }

    @Override
    public void visit(Condition condition) {
        Question appliedOn = condition.getQuestion();

        List<String> dependingAnswer = codeToAnswerMap.getOrDefault(condition.getQuestionCode(), new ArrayList<>());
        String valueCode = condition.getValueCode();
        boolean satisfied = valueCode == null || valueCode.isEmpty() || dependingAnswer.contains(valueCode);

        if (!satisfied && codeToAnswerMap.containsKey(appliedOn.getCode())) {
            throw new SurveyValidationException();
        }
    }

    private List<String> safeGetAnswers(Question question) {
        return codeToAnswerMap.getOrDefault(question.getCode(), new ArrayList<>());
    }

    private void validateRequired(Question question, List<String> answers) {
        if (question.isRequired() && answers.isEmpty()) {
            throw new SurveyValidationException();
        }
    }
}
