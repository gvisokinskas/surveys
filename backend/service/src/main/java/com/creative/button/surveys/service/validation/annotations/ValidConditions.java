package com.creative.button.surveys.service.validation.annotations;

import com.creative.button.surveys.service.validation.validators.ConditionValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ConditionValidator.class)
@Documented
public @interface ValidConditions {
    String message() default "Condition is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}