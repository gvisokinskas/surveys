package com.creative.button.surveys.service.services.implementation.answers;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.answers.AnswerCollectionRepository;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.factories.api.AnswerFactory;
import com.creative.button.surveys.service.security.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public abstract class AnswerHandler implements Ordered {

    @Autowired
    private AnswerFactory answerFactory;
    @Autowired
    AnswerCollectionRepository answerCollectionRepository;

    public abstract boolean canHandle(Long userId, Survey survey);

    public String handle(AnswerCollectionDTO answerCollectionDTO, JwtUser jwtUser, Survey survey) {
        AnswerCollection unfinishedAnswer = answerCollectionRepository.findOne(answerCollectionDTO.getCode());

        if (unfinishedAnswer != null) {
            if (!unfinishedAnswer.getSurvey().equals(survey)) {
                return null;
            }

            if (unfinishedAnswer.isFinished()) {
                return null;
            }

            answerCollectionRepository.delete(unfinishedAnswer);
        }

        return doHandleInternal(answerCollectionDTO, safeGetUserId(jwtUser), survey, unfinishedAnswer);
    }

    protected abstract String doHandleInternal(AnswerCollectionDTO answerCollectionDTO, Long userId, Survey survey, AnswerCollection unfinishedAnswer);

    protected AnswerCollection createNewAnswer(AnswerCollectionDTO answerCollectionDTO, Long userId, Survey survey) {
        AnswerCollection answerCollection = answerFactory.createAnswerCollection(answerCollectionDTO);
        answerCollection.setSubmissionDate(answerCollectionDTO.getSubmissionDate() != null ? answerCollectionDTO.getSubmissionDate() : new Date());
        answerCollection.setUserId(userId);
        answerCollection.setSurvey(survey);
        return answerCollection;
    }

    private Long safeGetUserId(JwtUser jwtUser) {
        return jwtUser != null ? jwtUser.getId() : null;
    }
}
