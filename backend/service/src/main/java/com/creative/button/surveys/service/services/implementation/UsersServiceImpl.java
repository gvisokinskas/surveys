package com.creative.button.surveys.service.services.implementation;

import com.creative.button.surveys.domain.authorities.Authority;
import com.creative.button.surveys.domain.authorities.AuthorityName;
import com.creative.button.surveys.domain.authorities.AuthorityRepository;
import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.generic.RegisteredUserDTO;
import com.creative.button.surveys.service.dto.generic.UserRegistrationDTO;
import com.creative.button.surveys.service.services.api.UsersService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public RegisteredUserDTO registerUser(UserRegistrationDTO userRegistrationDTO) {
        User domainUser = new User();

        BeanUtils.copyProperties(userRegistrationDTO, domainUser, "password");
        domainUser.setPassword(passwordEncoder.encode(userRegistrationDTO.getPassword()));

        Authority userAuthority = authorityRepository.findByName(AuthorityName.ROLE_USER);
        domainUser.getAuthorities().add(userAuthority);

        userRepository.save(domainUser);

        return new RegisteredUserDTO(domainUser);
    }

    @Override
    public List<RegisteredUserDTO> findAllUsers() {
        return userRepository.findAll().stream().map(RegisteredUserDTO::new).collect(Collectors.toList());
    }

    @Override
    public boolean removeUser(String username) {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            return false;
        }

        userRepository.delete(user);
        return true;
    }

    @Override
    public boolean disableUser(String username) {
        return setDisabled(username, true);
    }

    @Override
    public boolean enableUser(String username) {
        return setDisabled(username, false);
    }

    @Override
    public boolean isUserDisabled(String username) {
        User user = userRepository.findByUsername(username);
        return user == null || user.isDisabled();
    }

    private boolean setDisabled(String username, boolean value) {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            return false;
        }

        user.setDisabled(value);

        userRepository.save(user);

        return true;
    }
}
