package com.creative.button.surveys.service.services.implementation.excel.deserializers.question;

import com.creative.button.surveys.service.dto.questions.QuestionType;
import org.springframework.stereotype.Component;

@Component
public class MultipleSelectionDTODeserializer extends SelectionDTODeserializer {
    @Override
    public boolean canDeserialize(QuestionType questionType) {
        return questionType == QuestionType.MULTIPLE;
    }

    @Override
    protected boolean multiple() {
        return true;
    }
}
