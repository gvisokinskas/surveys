package com.creative.button.surveys.service.dto.surveys;

import com.creative.button.surveys.service.validation.annotations.ValidConditions;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@ValidConditions
@JsonIgnoreProperties(value = {"username", "answersByUser", "anonymousAnswers"}, allowGetters=true)
public class SurveyDTO {
    @NotNull
    @Size(min = 36, max = 36)
    private String code;

    private boolean isPublic;

    @NotNull
    @Size(min = 4, max = 100)
    private String label;

    @NotNull
    @Valid
    private List<PageDTO> pages = new ArrayList<>();

    private String username;

    private Long version;

    private Map<String, String> answersByUser;

    private List<String> anonymousAnswers;
}
