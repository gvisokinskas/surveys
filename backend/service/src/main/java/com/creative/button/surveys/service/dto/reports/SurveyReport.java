package com.creative.button.surveys.service.dto.reports;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SurveyReport {
    private int answerCount;
    private Date lastAnswerDate;
    private List<QuestionReport> questionReports;

    public SurveyReport(int answerCount, Date lastAnswerDate, List<QuestionReport> questionReports) {
        this.answerCount = answerCount;
        this.lastAnswerDate = lastAnswerDate;
        this.questionReports = questionReports;
    }
}
