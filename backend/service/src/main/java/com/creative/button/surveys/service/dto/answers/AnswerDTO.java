package com.creative.button.surveys.service.dto.answers;

import com.creative.button.surveys.service.validation.annotations.QuestionExists;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.List;

@Data
@NoArgsConstructor
public class AnswerDTO {
    @QuestionExists
    private String questionCode;

    @Size(min = 1)
    private List<String> value;
}
