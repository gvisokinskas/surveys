package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.questions.*;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.questions.ConditionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.surveys.PageDTO;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.factories.api.QuestionFactory;
import com.creative.button.surveys.service.validation.annotations.ValidConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class ConditionValidator implements ConstraintValidator<ValidConditions, SurveyDTO> {

    @Autowired
    private QuestionFactory questionFactory;

    @Override
    public void initialize(ValidConditions constraintAnnotation) {

    }

    @Override
    public boolean isValid(SurveyDTO value, ConstraintValidatorContext context) {
        Map<String, QuestionDTO> questions = value.getPages().stream()
                .map(PageDTO::getQuestions).flatMap(Collection::stream).collect(Collectors.toMap(QuestionDTO::getCode, Function.identity()));
        return questions.values().stream().allMatch(q -> isValid(q, questions));
    }

    private boolean isValid(QuestionDTO questionDTO, Map<String, QuestionDTO> allQuestions) {
        return getConditions(questionDTO).stream().allMatch(c -> isValid(c, allQuestions));
    }

    private List<ConditionDTO> getConditions(QuestionDTO questionDTO) {
        return questionDTO.getConditions() == null ? new ArrayList<>() : questionDTO.getConditions();
    }

    private boolean isValid(ConditionDTO conditionDTO, Map<String, QuestionDTO> allQuestions) {
        String questionCode = conditionDTO.getQuestionCode();
        Question question = questionFactory.createQuestion(allQuestions.get(questionCode), null);
        ConditionValidationVisitor conditionValidationVisitor = new ConditionValidationVisitor(conditionDTO);
        question.accept(conditionValidationVisitor);
        return conditionValidationVisitor.isValid;
    }

    private static class ConditionValidationVisitor implements SurveyVisitor {
        private ConditionDTO conditionDTO;
        private boolean isValid = true;

        private ConditionValidationVisitor(ConditionDTO conditionDTO) {
            this.conditionDTO = conditionDTO;
        }

        @Override
        public void visit(Survey survey) {

        }

        @Override
        public void visit(Page page) {

        }

        @Override
        public void visit(Selection selection) {
            if (!isEmptyValueCode()) {
                isValid = selection.getOptions().stream().map(SelectionOption::getValue).anyMatch(opt -> opt.equals(conditionDTO.getValueCode()));
            }
        }

        @Override
        public void visit(Text text) {
            if (!isEmptyValueCode()) {
                isValid = false;
            }
        }

        @Override
        public void visit(Range range) {
            IntStream allowedValues = IntStream.range(range.getStart(), range.getEnd() + 1);
            if (!isEmptyValueCode()) {
                isValid = allowedValues.anyMatch(i -> Integer.valueOf(conditionDTO.getValueCode()).equals(i));
            }
        }

        @Override
        public void visit(Condition condition) {
        }

        private boolean isEmptyValueCode() {
            return conditionDTO.getValueCode() == null || conditionDTO.getValueCode().trim().equals("");
        }
    }
}
