package com.creative.button.surveys.service.factories.api;

import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;

public interface AnswerFactory {
    AnswerCollection createAnswerCollection(AnswerCollectionDTO answerCollectionDTO);
    AnswerCollectionDTO createAnswerCollectionDTO(AnswerCollection answerCollection);
}
