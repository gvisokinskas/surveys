package com.creative.button.surveys.service.dto.excel;

import com.creative.button.surveys.service.dto.surveys.PageDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class PageDeserializationResult {
    private List<PageDTO> pages;
    private Map<String, String> readableQuestionCodesToUUIDs = new HashMap<>();
}
