package com.creative.button.surveys.service.validation.generic;

import java.util.regex.Pattern;

public interface GenericValidationService {
    String EMAIL_REGEX_STRING = ".+@.+\\.[a-z]+";
    Pattern EMAIL_REGEX_PATTERN = Pattern.compile(EMAIL_REGEX_STRING);

    boolean isEmailValid(String email);
}
