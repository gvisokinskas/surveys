package com.creative.button.surveys.service.dto.generic;

import com.creative.button.surveys.domain.users.User;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class RegisteredUserDTO {
    private String username;
    private String email;
    private boolean enabled;
    private List<AuthorityDTO> authorities;

    public RegisteredUserDTO(User user) {
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.enabled = !user.isDisabled();
        this.authorities = user.getAuthorities().stream().map(authority -> new AuthorityDTO(authority.getName().name())).collect(Collectors.toList());
    }
}
