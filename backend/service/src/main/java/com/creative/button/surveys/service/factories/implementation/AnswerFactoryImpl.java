package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.answers.Answer;
import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.dto.answers.AnswerDTO;
import com.creative.button.surveys.service.factories.api.AnswerFactory;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AnswerFactoryImpl implements AnswerFactory {
    @Override
    public AnswerCollection createAnswerCollection(AnswerCollectionDTO answerCollectionDTO) {
        AnswerCollection answerCollection = new AnswerCollection();
        answerCollection.setFinished(answerCollectionDTO.isFinished());
        answerCollection.setAnswers(answerCollectionDTO.getAnswers().stream().map(this::createAnswer).collect(Collectors.toMap(Answer::getQuestionCode, a -> a)));
        answerCollection.setCode(answerCollectionDTO.getCode());
        return answerCollection;
    }

    @Override
    public AnswerCollectionDTO createAnswerCollectionDTO(AnswerCollection answerCollection) {
        AnswerCollectionDTO answerCollectionDTO = new AnswerCollectionDTO();
        answerCollectionDTO.setSurveyCode(answerCollection.getSurvey().getCode());
        answerCollectionDTO.setFinished(answerCollection.isFinished());
        answerCollectionDTO.setAnswers(answerCollection.getAnswers().values().stream().map(this::createAnswerDTO).collect(Collectors.toList()));
        answerCollectionDTO.setCode(answerCollection.getCode());
        return answerCollectionDTO;
    }

    private Answer createAnswer(AnswerDTO answerDTO) {
        Answer answer = new Answer();
        answer.setQuestionCode(answerDTO.getQuestionCode());
        answer.setValue(answerDTO.getValue());
        return answer;
    }

    private AnswerDTO createAnswerDTO(Answer answer) {
        AnswerDTO answerDTO = new AnswerDTO();
        answerDTO.setQuestionCode(answer.getQuestionCode());
        answerDTO.setValue(answer.getValue());
        return answerDTO;
    }
}
