package com.creative.button.surveys.service.dto.reports;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnswerTuple {
    private String optionValue;
    private int answerCount;
}
