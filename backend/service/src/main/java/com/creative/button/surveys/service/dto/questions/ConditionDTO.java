package com.creative.button.surveys.service.dto.questions;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class ConditionDTO {
    @NotNull
    @Size(min = 36, max = 36)
    private String questionCode;

    @NotNull
    private String valueCode;
}
