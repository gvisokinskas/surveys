package com.creative.button.surveys.service.services.api;

import com.creative.button.surveys.service.dto.generic.RegisteredUserDTO;
import com.creative.button.surveys.service.dto.generic.UserRegistrationDTO;

import java.util.List;

public interface UsersService {
    RegisteredUserDTO registerUser(UserRegistrationDTO userRegistrationDTO);

    List<RegisteredUserDTO> findAllUsers();

    boolean removeUser(String username);

    boolean disableUser(String username);

    boolean enableUser(String username);

    boolean isUserDisabled(String username);
}
