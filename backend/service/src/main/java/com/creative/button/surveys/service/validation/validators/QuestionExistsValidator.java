package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.questions.QuestionRepository;
import com.creative.button.surveys.service.validation.annotations.QuestionExists;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuestionExistsValidator implements ConstraintValidator<QuestionExists, String> {

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public void initialize(QuestionExists constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return questionRepository.findOne(value) != null;
    }
}
