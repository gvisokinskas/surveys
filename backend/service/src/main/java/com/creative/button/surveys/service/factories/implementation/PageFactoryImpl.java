package com.creative.button.surveys.service.factories.implementation;

import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.service.dto.surveys.PageDTO;
import com.creative.button.surveys.service.factories.api.PageFactory;
import com.creative.button.surveys.service.factories.api.QuestionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class PageFactoryImpl implements PageFactory {

    @Autowired
    private QuestionFactory questionFactory;

    @Override
    public Page createPage(PageDTO pageDTO) {
        Page page = new Page();
        page.setCode(pageDTO.getCode());
        page.setLabel(pageDTO.getLabel());
        page.setQuestions(pageDTO.getQuestions().stream().map(q -> questionFactory.createQuestion(q, page)).collect(Collectors.toList()));
        return page;
    }

    @Override
    public PageDTO createPageDTO(Page page) {
        PageDTO pageDTO = new PageDTO();
        pageDTO.setCode(page.getCode());
        pageDTO.setLabel(page.getLabel());
        pageDTO.setQuestions(page.getQuestions().stream().map(q -> questionFactory.createQuestionDTO(q)).collect(Collectors.toList()));
        return pageDTO;
    }
}
