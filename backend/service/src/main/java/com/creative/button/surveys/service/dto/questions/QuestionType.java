package com.creative.button.surveys.service.dto.questions;

public enum QuestionType {
    RANGE(Constants.RANGE), SELECTION(Constants.SELECTION), TEXT(Constants.TEXT), MULTIPLE(Constants.MULTIPLE);

    QuestionType(String questionTypeString) {
    }

    public static class Constants {
        public static final String RANGE = "RANGE";
        public static final String SELECTION = "SELECTION";
        public static final String TEXT = "TEXT";
        public static final String MULTIPLE = "MULTIPLE";
    }
}
