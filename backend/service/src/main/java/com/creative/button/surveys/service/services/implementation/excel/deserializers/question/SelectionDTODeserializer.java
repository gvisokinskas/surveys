package com.creative.button.surveys.service.services.implementation.excel.deserializers.question;

import com.creative.button.surveys.service.dto.questions.OptionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.questions.SelectionDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class SelectionDTODeserializer implements QuestionDTODeserializer {
    @Override
    public boolean canDeserialize(QuestionType questionType) {
        return questionType == QuestionType.SELECTION;
    }

    @Override
    public QuestionDTO deserialize(List<String> options) {
        List<OptionDTO> normalized = options.stream().map(o -> {
            OptionDTO optionDTO = new OptionDTO();
            optionDTO.setCode(UUID.randomUUID().toString());
            optionDTO.setValue(o);
            return optionDTO;
        }).collect(Collectors.toList());

        SelectionDTO selectionDTO = new SelectionDTO();
        selectionDTO.setMultiple(multiple());
        selectionDTO.setOptions(normalized);

        return selectionDTO;
    }

    protected boolean multiple() {
        return false;
    }
}
