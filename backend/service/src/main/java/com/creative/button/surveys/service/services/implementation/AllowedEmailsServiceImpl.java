package com.creative.button.surveys.service.services.implementation;

import com.creative.button.surveys.domain.emails.AllowedEmail;
import com.creative.button.surveys.domain.emails.AllowedEmailRepository;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.generic.AllowedEmailDTO;
import com.creative.button.surveys.service.services.api.AllowedEmailsService;
import com.creative.button.surveys.service.validation.generic.GenericValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AllowedEmailsServiceImpl implements AllowedEmailsService {

    @Autowired
    private AllowedEmailRepository allowedEmailRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GenericValidationService genericValidationService;

    @Override
    public List<AllowedEmailDTO> getAllowedEmails() {
        return allowedEmailRepository.findAll().stream().map(AllowedEmail::getValue).map(AllowedEmailDTO::new).collect(Collectors.toList());
    }

    @Override
    public boolean save(AllowedEmailDTO allowedEmailDTO) {
        String email = allowedEmailDTO.getValue();

        if (!genericValidationService.isEmailValid(allowedEmailDTO.getValue()) || allowedEmailRepository.findOne(email) != null) {
            return false;
        }

        this.allowedEmailRepository.save(new AllowedEmail(email));
        return true;
    }

    @Override
    public boolean remove(AllowedEmailDTO allowedEmailDTO) {
        String email = allowedEmailDTO.getValue();

        if (allowedEmailRepository.findOne(email) == null || userRepository.findByEmail(email) != null) {
            return false;
        }

        this.allowedEmailRepository.delete(email);
        return true;
    }
}
