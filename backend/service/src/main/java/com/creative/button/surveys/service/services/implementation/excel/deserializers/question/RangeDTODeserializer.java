package com.creative.button.surveys.service.services.implementation.excel.deserializers.question;

import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.dto.questions.RangeDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RangeDTODeserializer implements QuestionDTODeserializer {
    @Override
    public boolean canDeserialize(QuestionType questionType) {
        return questionType == QuestionType.RANGE;
    }

    @Override
    public QuestionDTO deserialize(List<String> options) {
        int start = Integer.valueOf(options.get(0));
        int end = Integer.valueOf(options.get(1));

        RangeDTO rangeDTO = new RangeDTO();
        rangeDTO.setRangeStart(start);
        rangeDTO.setRangeEnd(end);

        return rangeDTO;
    }
}
