package com.creative.button.surveys.service.services.implementation.excel;

import com.creative.button.surveys.domain.CodeBasedEntity;
import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.answers.Answer;
import com.creative.button.surveys.domain.answers.AnswerCollection;
import com.creative.button.surveys.domain.questions.*;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.domain.surveys.Survey;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import org.apache.poi.ss.usermodel.*;

import java.util.*;
import java.util.stream.Stream;

public class ExportingVisitor implements SurveyVisitor {
    private final Workbook workbook;

    private final Sheet surveySheet;
    private final Sheet pageSheet;
    private final Sheet questionsSheet;
    private final Sheet answersSheet;
    private final Sheet concreteAnswersSheet;
    private final Sheet conditionsSheet;

    private final CellStyle headerCellStyle;
    private final CellStyle contentCellStyle;

    private final Map<CodeBasedEntity, Integer> readableCodeMaps = new HashMap<>();

    public ExportingVisitor(Workbook workbook) {
        this.workbook = workbook;

        this.surveySheet = workbook.createSheet(Constants.SURVEY_SHEET);
        this.pageSheet = workbook.createSheet(Constants.PAGES_SHEET);
        this.questionsSheet = workbook.createSheet(Constants.QUESTIONS_SHEET);
        this.conditionsSheet = workbook.createSheet(Constants.CONDITIONS);
        this.answersSheet = workbook.createSheet(Constants.ANSWERS_SHEET);
        this.concreteAnswersSheet = workbook.createSheet(Constants.CONCRETE_ANSWERS_SHEET);

        this.headerCellStyle = createHeaderCellStyle();
        this.contentCellStyle = createContentCellStyle();

        writeHeader(surveySheet, Constants.LABEL, Constants.PUBLIC, Constants.USER_ID);
        writeHeader(pageSheet, Constants.CODE, Constants.LABEL);
        writeHeader(questionsSheet, Constants.CODE, Constants.LABEL, Constants.REQUIRED, Constants.PAGE_CODE, Constants.TYPE, Constants.OPTIONS);
        writeHeader(conditionsSheet, Constants.BELONGS_TO_QUESTION, Constants.QUESTION_CODE, Constants.VALUE_CODE);
        writeHeader(answersSheet, Constants.CODE, Constants.USER_ID, Constants.FINISHED, Constants.SUBMISSION_DATE);
        writeHeader(concreteAnswersSheet, Constants.ANSWER_CODE, Constants.QUESTION_CODE, Constants.VALUES);
    }

    @Override
    public void visit(Survey survey) {
        writeContent(surveySheet, survey.getLabel(), survey.isPublic(), survey.getUserId());
    }

    @Override
    public void visit(Page page) {
        int readableCode = pageSheet.getLastRowNum() + 1;
        readableCodeMaps.put(page, readableCode);

        writeContent(pageSheet, readableCode, page.getLabel());
    }

    @Override
    public void visit(Selection selection) {
        if (selection.isMultiple()) {
            writeQuestion(selection, QuestionType.MULTIPLE, selection.getOptions().stream().map(SelectionOption::getValue).toArray(String[]::new));
        } else {
            writeQuestion(selection, QuestionType.SELECTION, selection.getOptions().stream().map(SelectionOption::getValue).toArray(String[]::new));
        }
    }

    @Override
    public void visit(Text text) {
        writeQuestion(text, QuestionType.TEXT);
    }

    @Override
    public void visit(Range range) {
        writeQuestion(range, QuestionType.RANGE, String.valueOf(range.getStart()), String.valueOf(range.getEnd()));
    }

    @Override
    public void visit(Condition condition) {
        writeContent(conditionsSheet, readableCodeMaps.get(condition.getQuestion()), getReadableQuestionCode(condition.getQuestionCode()), condition.getValueCode());
    }

    public void exportAnswers(List<AnswerCollection> answerCollection) {
        answerCollection.forEach(this::exportAnswer);
    }

    public Workbook getWorkbook() {
        int max = 0;

        for (int i = 0; i <= questionsSheet.getLastRowNum(); i++) {
            Row r = questionsSheet.getRow(i);
            int maxCell = r.getLastCellNum();
            if (maxCell > max) {
                max = maxCell;
            }
        }

        for (int i = 0; i < max; i++) {
            pageSheet.autoSizeColumn(i);
            surveySheet.autoSizeColumn(i);
            questionsSheet.autoSizeColumn(i);
            answersSheet.autoSizeColumn(i);
            concreteAnswersSheet.autoSizeColumn(i);
            conditionsSheet.autoSizeColumn(i);
        }

        return workbook;
    }

    private void exportAnswer(AnswerCollection answerCollection) {
        int readableCode = answersSheet.getLastRowNum() + 1;
        readableCodeMaps.put(answerCollection, readableCode);

        writeContent(answersSheet, readableCode, answerCollection.getUserId(), answerCollection.isFinished(), answerCollection.getSubmissionDate());
        answerCollection.getAnswers().values().forEach(a -> exportAnswerValue(a, answerCollection));
    }

    private void exportAnswerValue(Answer answer, AnswerCollection answerCollection) {
        Object[] generic = {readableCodeMaps.get(answerCollection), getReadableQuestionCode(answer.getQuestionCode())};
        Object[] arguments = merge(generic, answer.getValue().toArray());

        writeContent(concreteAnswersSheet, arguments);
    }

    private int getReadableQuestionCode(String questionCode) {
        return readableCodeMaps.entrySet().stream().filter(e -> e.getKey() instanceof Question && e.getKey().getCode().equals(questionCode)).findFirst().orElseThrow(IllegalArgumentException::new).getValue();
    }

    private CellStyle createHeaderCellStyle() {
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(headerFont);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        return cellStyle;
    }

    private CellStyle createContentCellStyle() {
        return workbook.createCellStyle();
    }

    private void writeHeader(Sheet sheet, Object... values) {
        writeValues(sheet, 0, headerCellStyle, values);
    }

    private void writeQuestion(Question question, QuestionType questionType, String... options) {
        int readableCode = questionsSheet.getLastRowNum() + 1;
        readableCodeMaps.put(question, readableCode);

        Object[] generic = {readableCode, question.getLabel(), question.isRequired(), readableCodeMaps.get(question.getPage()), questionType};
        Object[] arguments = merge(generic, options);
        writeContent(questionsSheet, arguments);
    }

    private void writeContent(Sheet sheet, Object... values) {
        int rowNum = sheet.getLastRowNum();
        writeValues(sheet, ++rowNum, contentCellStyle, values);
    }

    private void writeValues(Sheet sheet, int rowNum, CellStyle cellStyle, Object... values) {
        Row row = sheet.createRow(rowNum);
        int cIdx = 0;

        for (Object value : values) {
            Cell cell = row.createCell(cIdx);
            if (value != null){
                if (value instanceof Boolean) cell.setCellValue((boolean) value);
                else if (value instanceof Date) cell.setCellValue((Date) value);
                else cell.setCellValue(value.toString());
            } else {
                cell.setCellValue("");
            }
            cell.setCellStyle(cellStyle);
            cIdx++;
        }
    }

    private Object[] merge(Object[] a, Object[] b) {
        return Stream.concat(Arrays.stream(a), Arrays.stream(b))
                .toArray(Object[]::new);
    }
}
