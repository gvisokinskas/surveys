package com.creative.button.surveys.service.services.implementation.excel.deserializers;

import com.creative.button.surveys.service.dto.questions.QuestionDTO;
import com.creative.button.surveys.service.dto.questions.QuestionType;
import com.creative.button.surveys.service.services.implementation.excel.Constants;
import com.creative.button.surveys.service.services.implementation.excel.deserializers.question.QuestionDTODeserializer;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class QuestionDeserializer {

    @Autowired
    private Collection<QuestionDTODeserializer> questionDeserializers;

    public List<QuestionDTO> deserialize(Workbook workbook, String pageCode, Map<String, String> qcToUUIDs) {
        Sheet questionsSheet = workbook.getSheet(Constants.QUESTIONS_SHEET);
        List<QuestionDTO> result = new ArrayList<>();

        for (int i = 1; i <= questionsSheet.getLastRowNum() && questionsSheet.getRow(i) != null; i++) {
            QuestionDTO question = deserialize(questionsSheet.getRow(i), pageCode, qcToUUIDs);
            if (question != null) {
                result.add(question);
            }
        }

        return result;
    }

    private QuestionDTO deserialize(Row row, String pageCode, Map<String, String> qcToUUIDs) {
        String rowPageCode = CellDeserializer.getStringCellValue(row.getCell(3));

        if (!rowPageCode.equals(pageCode)) {
            return null;
        }

        QuestionType questionType = QuestionType.valueOf(CellDeserializer.getStringCellValue(row.getCell(4)));
        List<String> options = getOptions(row);
        QuestionDTO questionDTO = getDeserializer(questionType).deserialize(options);

        String uuid = UUID.randomUUID().toString();
        String code = CellDeserializer.getStringCellValue(row.getCell(0));
        String label = CellDeserializer.getStringCellValue(row.getCell(1));
        boolean isRequired = row.getCell(2).getBooleanCellValue();

        questionDTO.setCode(uuid);
        questionDTO.setLabel(label);
        questionDTO.setRequired(isRequired);
        qcToUUIDs.put(code, uuid);

        return questionDTO;
    }

    private List<String> getOptions(Row row) {
        List<String> options = new ArrayList<>();
        for (int i = 5; i < row.getLastCellNum(); i++) {
            options.add(CellDeserializer.getStringCellValue(row.getCell(i)));
        }
        return options;
    }

    private QuestionDTODeserializer getDeserializer(QuestionType questionType) {
        return questionDeserializers.stream().filter(q -> q.canDeserialize(questionType)).findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
