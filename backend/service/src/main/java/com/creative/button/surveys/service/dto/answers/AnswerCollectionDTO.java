package com.creative.button.surveys.service.dto.answers;

import com.creative.button.surveys.service.validation.annotations.SurveyExists;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class AnswerCollectionDTO {
    @SurveyExists
    private String surveyCode;

    private boolean finished;

    @Valid
    private List<AnswerDTO> answers;

    @NotNull
    @Size(min = 36, max = 36)
    private String code;

    @JsonIgnore
    private Date submissionDate;
}
