package com.creative.button.surveys.service.services.implementation.excel.deserializers;

import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.dto.answers.AnswerDTO;
import com.creative.button.surveys.service.dto.excel.AnswerDeserializationResult;
import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.security.JwtUserFactory;
import com.creative.button.surveys.service.services.implementation.excel.Constants;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class AnswerDeserializer {

    @Autowired
    private UserRepository userRepository;

    public List<AnswerDeserializationResult> deserialize(Workbook workbook, SurveyDTO surveyDTO, Map<String, String> qcToUUIDs) {
        Sheet answerSheet = workbook.getSheet(Constants.ANSWERS_SHEET);
        List<AnswerDeserializationResult> result = new ArrayList<>();

        for (int i = 1; i <= answerSheet.getLastRowNum(); i++) {
            AnswerDeserializationResult deserializationResult = deserialize(answerSheet.getRow(i), workbook, qcToUUIDs);
            deserializationResult.getAnswerCollectionDTO().setSurveyCode(surveyDTO.getCode());
            result.add(deserializationResult);
        }

        return result;
    }

    private AnswerDeserializationResult deserialize(Row row, Workbook workbook, Map<String, String> qcToUUIDs) {
        AnswerCollectionDTO answerCollectionDTO = new AnswerCollectionDTO();

        String code = CellDeserializer.getStringCellValue(row.getCell(0));
        answerCollectionDTO.setCode(UUID.randomUUID().toString());
        answerCollectionDTO.setFinished(row.getCell(2).getBooleanCellValue());
        answerCollectionDTO.setSubmissionDate(row.getCell(3).getDateCellValue());
        answerCollectionDTO.setAnswers(deserializeAnswers(workbook, code, qcToUUIDs));

        String userID = CellDeserializer.getStringCellValue(row.getCell(1));
        Long userIDLong = tryParseUserID(userID);
        JwtUser jwtUser = null;
        if (userIDLong != null) {
            User user = userRepository.findOne(userIDLong);

            if (user != null) {
                jwtUser = JwtUserFactory.create(user);
            }
        }

        return new AnswerDeserializationResult(answerCollectionDTO, jwtUser);
    }

    private List<AnswerDTO> deserializeAnswers(Workbook workbook, String answerCode, Map<String, String> qcToUUIDs) {
        Sheet concreteAnswersSheet = workbook.getSheet(Constants.CONCRETE_ANSWERS_SHEET);
        List<AnswerDTO> result = new ArrayList<>();

        for (int i = 1; i <= concreteAnswersSheet.getLastRowNum(); i++) {
            AnswerDTO answerDTO = deserialize(concreteAnswersSheet.getRow(i), answerCode, qcToUUIDs);
            if (answerDTO != null) {
                result.add(answerDTO);
            }
        }

        return result;
    }

    private AnswerDTO deserialize(Row row, String answerCode, Map<String, String> qcToUUIDs) {
        String rowAnswerCode = CellDeserializer.getStringCellValue(row.getCell(0));
        if (!rowAnswerCode.equals(answerCode)) {
            return null;
        }

        String questionCode = CellDeserializer.getStringCellValue(row.getCell(1));
        List<String> values = new ArrayList<>();
        for (int i = 2; i < row.getLastCellNum(); i++) {
            values.add(CellDeserializer.getStringCellValue(row.getCell(i)));
        }

        AnswerDTO answerDTO = new AnswerDTO();
        answerDTO.setValue(values);
        answerDTO.setQuestionCode(qcToUUIDs.get(questionCode));
        return answerDTO;
    }

    private Long tryParseUserID(String userId) {
        try {
            return Long.valueOf(userId);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
