package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.emails.AllowedEmailRepository;
import com.creative.button.surveys.service.validation.annotations.AllowedEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class AllowedEmailValidator implements ConstraintValidator<AllowedEmail, String> {

    @Autowired
    private AllowedEmailRepository allowedEmailRepository;

    @Override
    public void initialize(AllowedEmail constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return allowedEmailRepository.findOne(value) != null;
    }
}
