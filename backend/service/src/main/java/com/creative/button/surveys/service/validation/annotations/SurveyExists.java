package com.creative.button.surveys.service.validation.annotations;

import com.creative.button.surveys.service.validation.validators.SurveyExistsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SurveyExistsValidator.class)
@Documented
public @interface SurveyExists {
    String message() default "Not existing survey";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
