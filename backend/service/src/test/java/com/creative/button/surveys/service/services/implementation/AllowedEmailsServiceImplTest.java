package com.creative.button.surveys.service.services.implementation;

import com.creative.button.surveys.domain.emails.AllowedEmail;
import com.creative.button.surveys.domain.emails.AllowedEmailRepository;
import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import com.creative.button.surveys.service.dto.generic.AllowedEmailDTO;
import com.creative.button.surveys.service.validation.generic.GenericValidationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class AllowedEmailsServiceImplTest {

    @InjectMocks
    private AllowedEmailsServiceImpl emailsService;

    @Mock
    private GenericValidationService genericValidationService;
    @Mock
    private AllowedEmailRepository allowedEmailRepository;
    @Mock
    private UserRepository userRepository;

    @Before
    public void prepareMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void allowedEmailsShouldBeAvailable() {
        List<AllowedEmail> expectedCollection = asList(new AllowedEmail(), new AllowedEmail());
        when(allowedEmailRepository.findAll()).thenReturn(expectedCollection);

        Collection<AllowedEmailDTO> result = emailsService.getAllowedEmails();

        assertThat(result, hasSize(2));
    }

    @Test
    public void dtoShouldNotBeSavedIfItIsNotValid() {
        AllowedEmailDTO dto = new AllowedEmailDTO("a");
        when(genericValidationService.isEmailValid("a")).thenReturn(false);

        boolean result = emailsService.save(dto);

        assertThat(result, is(false));
    }

    @Test
    public void dtoShouldNotBeSavedIfItAlreadyExists() {
        AllowedEmailDTO dto = new AllowedEmailDTO("a");
        when(genericValidationService.isEmailValid("a")).thenReturn(true);
        when(allowedEmailRepository.findOne("a")).thenReturn(new AllowedEmail());

        boolean result = emailsService.save(dto);

        assertThat(result, is(false));
    }

    @Test
    public void dtoShouldBeSavedIfItDoesNotExistAndHasValidEmail() {
        AllowedEmailDTO dto = new AllowedEmailDTO("a");
        when(genericValidationService.isEmailValid("a")).thenReturn(true);
        when(allowedEmailRepository.findOne("a")).thenReturn(null);

        boolean result = emailsService.save(dto);

        assertThat(result, is(true));
    }

    @Test
    public void dtoShouldNotBeRemovedIfItDoesNotExist() {
        AllowedEmailDTO dto = new AllowedEmailDTO("a");
        when(allowedEmailRepository.findOne("a")).thenReturn(null);

        boolean result = emailsService.remove(dto);

        assertThat(result, is(false));
    }

    @Test
    public void dtoShouldBeRemovedIfItExists() {
        AllowedEmailDTO dto = new AllowedEmailDTO("a");
        when(allowedEmailRepository.findOne("a")).thenReturn(new AllowedEmail());

        boolean result = emailsService.remove(dto);

        assertThat(result, is(true));
    }

    @Test
    public void emailShouldNotBeRemovedWhenUserHasIt() {
        AllowedEmailDTO dto = new AllowedEmailDTO("a");
        when(allowedEmailRepository.findOne("a")).thenReturn(new AllowedEmail());
        when(userRepository.findByEmail("a")).thenReturn(new User());

        boolean result = emailsService.remove(dto);

        assertThat(result, is(false));
    }
}