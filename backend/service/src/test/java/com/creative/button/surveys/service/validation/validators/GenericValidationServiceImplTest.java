package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.service.validation.generic.GenericValidationService;
import com.creative.button.surveys.service.validation.generic.GenericValidationServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class GenericValidationServiceImplTest {

    private GenericValidationService genericValidationService;

    @Before
    public void setUp() {
        this.genericValidationService = new GenericValidationServiceImpl();
    }

    @Test
    public void simpleEmailShouldBeValid() {
        assertThat(genericValidationService.isEmailValid("aaa@aaa.aa"), is(true));
    }

    @Test
    public void anyOtherStringShouldNotBeValid() {
        assertThat(genericValidationService.isEmailValid("aaaaaa.aa"), is(false));
    }

    @Test
    public void nullShouldNotBeValid() {
        assertThat(genericValidationService.isEmailValid(null), is(false));
    }

    @Test
    public void emptyStringShouldNotBeValid() {
        assertThat(genericValidationService.isEmailValid(""), is(false));
    }
}