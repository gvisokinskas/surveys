package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.users.User;
import com.creative.button.surveys.domain.users.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class UsernameValidatorTest {

    @InjectMocks
    private UsernameValidator usernameValidator;

    @Mock
    private UserRepository userRepository;

    @Before
    public void prepareMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void usernameShouldBeUniqueIfItDoesNotExist() {
        String username = "aaa";
        when(userRepository.findByUsername(username)).thenReturn(null);

        assertThat(usernameValidator.isValid(username, null), is(true));
    }

    @Test
    public void validationShouldFailIfUsernameExists() {
        String username = "aaa";
        when(userRepository.findByUsername(username)).thenReturn(new User());

        assertThat(usernameValidator.isValid(username, null), is(false));
    }
}