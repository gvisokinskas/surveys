package com.creative.button.surveys.service.validation.validators;

import com.creative.button.surveys.domain.emails.AllowedEmail;
import com.creative.button.surveys.domain.emails.AllowedEmailRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class AllowedEmailValidatorTest {

    @InjectMocks
    private AllowedEmailValidator allowedEmailValidator;

    @Mock
    private AllowedEmailRepository allowedEmailRepository;

    @Before
    public void prepareMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void emailShouldBeValidIfItExists() {
        when(allowedEmailRepository.findOne("email")).thenReturn(new AllowedEmail());

        assertThat(allowedEmailValidator.isValid("email", null), is(true));
    }

    @Test
    public void emailShouldNotBeValidIfItDoesNotExist() {
        when(allowedEmailRepository.findOne("email")).thenReturn(null);

        assertThat(allowedEmailValidator.isValid("email", null), is(false));

    }
}