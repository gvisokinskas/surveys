package com.creative.button.surveys.domain.questions;

import com.creative.button.surveys.domain.SurveyVisitor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Text extends Question {
    @Override
    protected void acceptLocal(SurveyVisitor surveyVisitor) {
        surveyVisitor.visit(this);
    }
}
