package com.creative.button.surveys.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.UniqueConstraint;
import java.util.UUID;

@Data
@MappedSuperclass
@EqualsAndHashCode
public abstract class CodeBasedEntity {
    @Id
    private String code = UUID.randomUUID().toString();
}
