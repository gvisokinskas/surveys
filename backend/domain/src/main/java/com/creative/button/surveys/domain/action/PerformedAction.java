package com.creative.button.surveys.domain.action;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class PerformedAction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String userName;

    @Column
    private String methodName;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column
    private boolean isAdmin;

    @ElementCollection
    private List<String> arguments;
}
