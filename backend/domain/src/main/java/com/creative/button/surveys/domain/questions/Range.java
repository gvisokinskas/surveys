package com.creative.button.surveys.domain.questions;

import com.creative.button.surveys.domain.SurveyVisitor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Range extends Question {
    @Column(nullable = false)
    private int start;

    @Column(nullable = false)
    private int end;

    @Override
    protected void acceptLocal(SurveyVisitor surveyVisitor) {
        surveyVisitor.visit(this);
    }
}
