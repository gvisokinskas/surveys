package com.creative.button.surveys.domain.surveys;

import com.creative.button.surveys.domain.CodeBasedEntity;
import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.Visitable;
import com.creative.button.surveys.domain.questions.Question;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Page extends CodeBasedEntity implements Visitable {
    @Column(nullable = false)
    private String label;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "page", fetch = FetchType.EAGER)
    private List<Question> questions;

    @Override
    public void accept(SurveyVisitor surveyVisitor) {
        surveyVisitor.visit(this);
        for (Question question : questions) {
            question.accept(surveyVisitor);
        }
    }
}
