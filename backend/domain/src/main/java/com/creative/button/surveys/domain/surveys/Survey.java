package com.creative.button.surveys.domain.surveys;

import com.creative.button.surveys.domain.CodeBasedEntity;
import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.Visitable;
import com.creative.button.surveys.domain.answers.AnswerCollection;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Survey extends CodeBasedEntity implements Visitable {

    private static final String JOIN_COLUMN_NAME = "SURVEY_CODE";

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "survey")
    private List<AnswerCollection> answers;

    @Column(nullable = false)
    private boolean isPublic;

    @Column(nullable = false)
    private Long userId;

    @Column(nullable = false)
    private String label;

    @Version
    @Column(nullable = false)
    private Long version;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = JOIN_COLUMN_NAME)
    private List<Page> pages;

    public void accept(SurveyVisitor surveyVisitor) {
        surveyVisitor.visit(this);
        for (Page page : pages) {
            page.accept(surveyVisitor);
        }
    }
}