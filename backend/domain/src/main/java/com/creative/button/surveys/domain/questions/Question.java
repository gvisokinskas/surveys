package com.creative.button.surveys.domain.questions;

import com.creative.button.surveys.domain.CodeBasedEntity;
import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.Visitable;
import com.creative.button.surveys.domain.surveys.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class Question extends CodeBasedEntity implements Visitable {
    private static final String JOIN_COLUMN_NAME = "PAGE_CODE";

    @Column(nullable = false)
    private String label;

    @Column(nullable = false)
    private boolean required;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question", fetch = FetchType.EAGER)
    private List<Condition> conditions;

    @ManyToOne
    @JoinColumn(name = JOIN_COLUMN_NAME)
    private Page page;

    protected abstract void acceptLocal(SurveyVisitor surveyVisitor);

    @Override
    public void accept(SurveyVisitor surveyVisitor) {
        acceptLocal(surveyVisitor);
        conditions.forEach(c -> c.accept(surveyVisitor));
    }
}
