package com.creative.button.surveys.domain.surveys;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface SurveyRepository extends JpaRepository<Survey, String> {
    Collection<Survey> findByUserIdOrIsPublic(Long userId, boolean isPublic);
}
