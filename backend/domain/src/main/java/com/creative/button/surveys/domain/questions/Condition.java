package com.creative.button.surveys.domain.questions;

import com.creative.button.surveys.domain.SurveyVisitor;
import com.creative.button.surveys.domain.Visitable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class Condition implements Visitable {
    private static final String JOIN_COLUMN_NAME = "FOR_QUESTION_CODE";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String questionCode;

    @Column
    private String valueCode;

    @ManyToOne
    @JoinColumn(name = JOIN_COLUMN_NAME)
    private Question question;

    @Override
    public void accept(SurveyVisitor surveyVisitor) {
        surveyVisitor.visit(this);
    }
}
