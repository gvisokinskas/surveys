package com.creative.button.surveys.domain.questions;

import com.creative.button.surveys.domain.CodeBasedEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class SelectionOption extends CodeBasedEntity {
    @Column(nullable = false)
    private String value;
}
