package com.creative.button.surveys.domain.questions;

import com.creative.button.surveys.domain.SurveyVisitor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Selection extends Question {
    private static final String JOIN_COLUMN_NAME = "SELECTION_CODE";

    @Column(nullable = false)
    private boolean multiple;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = JOIN_COLUMN_NAME)
    private List<SelectionOption> options;

    @Override
    protected void acceptLocal(SurveyVisitor surveyVisitor) {
        surveyVisitor.visit(this);
    }
}
