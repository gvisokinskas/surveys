package com.creative.button.surveys.domain.authorities;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}