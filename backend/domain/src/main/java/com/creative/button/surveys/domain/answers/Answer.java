package com.creative.button.surveys.domain.answers;

import com.creative.button.surveys.domain.CodeBasedEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Answer extends CodeBasedEntity {
    @Column(nullable = false)
    private String questionCode;

    @ElementCollection
    private List<String> value;
}
