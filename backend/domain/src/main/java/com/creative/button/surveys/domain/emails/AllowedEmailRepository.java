package com.creative.button.surveys.domain.emails;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AllowedEmailRepository extends JpaRepository<AllowedEmail, String> {

}
