package com.creative.button.surveys.domain.answers;

import com.creative.button.surveys.domain.CodeBasedEntity;
import com.creative.button.surveys.domain.surveys.Survey;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class AnswerCollection extends CodeBasedEntity {
    private static final String JOIN_COLUMN_NAME = "ANSWER_COLLECTION_CODE";

    @Column
    private Long userId;

    @OneToMany(cascade = CascadeType.ALL)
    @MapKey(name = "questionCode")
    @JoinColumn(name = JOIN_COLUMN_NAME)
    private Map<String, Answer> answers;

    @Column(nullable = false)
    private boolean finished;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date submissionDate;

    @ManyToOne(optional = false)
    private Survey survey;
}
