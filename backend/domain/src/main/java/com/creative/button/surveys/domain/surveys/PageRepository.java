package com.creative.button.surveys.domain.surveys;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PageRepository extends JpaRepository<Page, String> {
}
