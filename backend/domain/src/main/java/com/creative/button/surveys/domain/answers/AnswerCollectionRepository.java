package com.creative.button.surveys.domain.answers;

import com.creative.button.surveys.domain.surveys.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerCollectionRepository extends JpaRepository<AnswerCollection, String> {
    AnswerCollection findByUserIdAndSurvey(Long userId, Survey survey);
}
