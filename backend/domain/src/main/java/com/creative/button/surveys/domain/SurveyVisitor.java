package com.creative.button.surveys.domain;

import com.creative.button.surveys.domain.questions.Condition;
import com.creative.button.surveys.domain.questions.Range;
import com.creative.button.surveys.domain.questions.Selection;
import com.creative.button.surveys.domain.questions.Text;
import com.creative.button.surveys.domain.surveys.Page;
import com.creative.button.surveys.domain.surveys.Survey;

public interface SurveyVisitor {
    void visit(Survey survey);

    void visit(Page page);

    void visit(Selection selection);

    void visit(Text text);

    void visit(Range range);

    void visit(Condition condition);
}
