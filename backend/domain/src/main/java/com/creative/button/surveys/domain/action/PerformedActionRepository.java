package com.creative.button.surveys.domain.action;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PerformedActionRepository extends JpaRepository<PerformedAction, Long> {
}
