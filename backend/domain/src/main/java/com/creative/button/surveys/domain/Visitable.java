package com.creative.button.surveys.domain;

public interface Visitable {
    void accept(SurveyVisitor surveyVisitor);
}
