-- noinspection SqlResolveForFile

INSERT INTO authority (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO authority (id, name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO allowed_email (value) VALUES ('admin@admin.com');
INSERT INTO allowed_email (value) VALUES ('user@user.com');
INSERT INTO allowed_email (value) VALUES ('new@new.com');

INSERT INTO user (id, username, password, email, disabled) VALUES (1, 'admin', '$2a$04$1SXILeeCjaIfLJoKNAFpJOvuCKCCMJo5XDzJzLdzss7yGkRGYtVU6', 'admin@admin.com', FALSE);
INSERT INTO user (id, username, password, email, disabled) VALUES (2, 'user', '$2a$04$Y7O41PcDXHeTTIq8jobKN.Y3bzWzSSrhxW.PoNyZvs.oqRJaMY91C', 'user@user.com', FALSE);

INSERT INTO user_authority (user_id, authority_id) VALUES (1, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (1, 2);
INSERT INTO user_authority (user_id, authority_id) VALUES (2, 1);


------SURVEY 1
INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('561a630a-c602-4339-8cd7-b85609220ae6', true, 1, 'General Event Feedback', 1);


INSERT INTO page (code, label, survey_code) VALUES ('3927db49-59d6-4d67-a767-de85385c99a5', 'General impression', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO question (code, label, required, page_code) VALUES ('d19e1be0-2dd5-4227-b73a-925da6db1ed2', 'How likely is it that you would recommend the event?', true, '3927db49-59d6-4d67-a767-de85385c99a5');
INSERT INTO range (code, start, end) VALUES ('d19e1be0-2dd5-4227-b73a-925da6db1ed2', 1, 10);
INSERT INTO question (code, label, required, page_code) VALUES ('16ba2365-c03d-429a-872a-1b2c8c93986b', 'Overall, how would you rate the event?', true, '3927db49-59d6-4d67-a767-de85385c99a5');
INSERT INTO selection (code, multiple) VALUES ('16ba2365-c03d-429a-872a-1b2c8c93986b', false);
INSERT INTO selection_option (code, value, selection_code) VALUES ('4401cbbf-5a9b-4ef8-b986-38b11e4b0d87', 'Excellent', '16ba2365-c03d-429a-872a-1b2c8c93986b');
INSERT INTO selection_option (code, value, selection_code) VALUES ('b39ac2c0-0954-4de2-b82d-4d40493cb82b', 'Very Good', '16ba2365-c03d-429a-872a-1b2c8c93986b');
INSERT INTO selection_option (code, value, selection_code) VALUES ('7261a8d4-ec11-45b8-a565-8aacc6fba690', 'Average', '16ba2365-c03d-429a-872a-1b2c8c93986b');
INSERT INTO selection_option (code, value, selection_code) VALUES ('8599aaf5-aa3a-4003-92c9-8c6d6345de3b', 'Not so good', '16ba2365-c03d-429a-872a-1b2c8c93986b');
INSERT INTO question (code, label, required, page_code) VALUES ('9122c723-ce36-40e9-ae0a-d664b92b76d5', 'What did you like about the event?', false, '3927db49-59d6-4d67-a767-de85385c99a5');
INSERT INTO text (code) VALUES ('9122c723-ce36-40e9-ae0a-d664b92b76d5');

INSERT INTO question (code, label, required, page_code) VALUES ('c075e21a-e3af-4175-a25a-3dfbffd69d05', 'From where you got information about the event', false, '3927db49-59d6-4d67-a767-de85385c99a5');
INSERT INTO selection (code, multiple) VALUES ('c075e21a-e3af-4175-a25a-3dfbffd69d05', true);
INSERT INTO selection_option (code, value, selection_code) VALUES ('72d35fe5-6951-4f22-9e94-d052fb641975', 'Friends', 'c075e21a-e3af-4175-a25a-3dfbffd69d05');
INSERT INTO selection_option (code, value, selection_code) VALUES ('d181cdd5-1f3e-4146-ab94-6142117a7e9d', 'Colleagues', 'c075e21a-e3af-4175-a25a-3dfbffd69d05');
INSERT INTO selection_option (code, value, selection_code) VALUES ('e1a3c473-fd8b-4b92-9882-c696a335d5b5', 'Internet', 'c075e21a-e3af-4175-a25a-3dfbffd69d05');
INSERT INTO selection_option (code, value, selection_code) VALUES ('66805f0d-e228-4b4f-b52f-9314cada0166', 'Other', 'c075e21a-e3af-4175-a25a-3dfbffd69d05');

INSERT INTO question (code, label, required, page_code) VALUES ('829ab804-22a0-4d24-b2ca-5d953d691995', 'Please, specify', false, '3927db49-59d6-4d67-a767-de85385c99a5');
INSERT INTO text (code) VALUES ('829ab804-22a0-4d24-b2ca-5d953d691995');
INSERT INTO condition (id, question_code, value_code, for_question_code) VALUES (1, 'c075e21a-e3af-4175-a25a-3dfbffd69d05', 'Other', '829ab804-22a0-4d24-b2ca-5d953d691995');


INSERT INTO page (code, label, survey_code) VALUES ('0f5aebd5-5a61-4c21-82ac-9688e5c22ca7', 'Staff', '561a630a-c602-4339-8cd7-b85609220ae6');

INSERT INTO question (code, label, required, page_code) VALUES ('f614a5e8-0de0-4bbe-9856-7a74bf22c697', 'How friendly was the staff', false, '0f5aebd5-5a61-4c21-82ac-9688e5c22ca7');
INSERT INTO selection (code, multiple) VALUES ('f614a5e8-0de0-4bbe-9856-7a74bf22c697', false);
INSERT INTO selection_option (code, value, selection_code) VALUES ('e89903ad-84b6-4134-9295-4267cebbd34f', 'Very helpful', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697');
INSERT INTO selection_option (code, value, selection_code) VALUES ('8b2be8e5-f344-406f-9403-3ff806990932', 'Somewhat helpful', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697');
INSERT INTO selection_option (code, value, selection_code) VALUES ('dad41ee4-4f5a-4008-b8ec-d1ca81d2b6b5', 'Not so helpful', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('c388006f-797e-4744-b51f-91ef52e0b076', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('8acadbd1-b3c1-437a-9dcd-5088e8d95418', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', 'c388006f-797e-4744-b51f-91ef52e0b076');
INSERT INTO answer_value (answer_code, value) VALUES ('8acadbd1-b3c1-437a-9dcd-5088e8d95418', '1');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('5fb709ad-dcd7-4f0a-91a3-a9796c05e5f2', '16ba2365-c03d-429a-872a-1b2c8c93986b', 'c388006f-797e-4744-b51f-91ef52e0b076');
INSERT INTO answer_value (answer_code, value) VALUES ('5fb709ad-dcd7-4f0a-91a3-a9796c05e5f2', 'Not so good');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('3678734f-cde9-41b5-95f1-dd0b5240a280', '9122c723-ce36-40e9-ae0a-d664b92b76d5', 'c388006f-797e-4744-b51f-91ef52e0b076');
INSERT INTO answer_value (answer_code, value) VALUES ('3678734f-cde9-41b5-95f1-dd0b5240a280', 'I hated everything. Food was awfull. Staff was horrible. I lost my wallet, I think somebody stole it');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('3b49439f-a8f5-400f-a6ed-a53d88f9809a', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', 'c388006f-797e-4744-b51f-91ef52e0b076');
INSERT INTO answer_value (answer_code, value) VALUES ('3b49439f-a8f5-400f-a6ed-a53d88f9809a', 'Friends');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('9011f95f-4f9c-4ab2-a03f-a9f69aa983c8', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', 'c388006f-797e-4744-b51f-91ef52e0b076');
INSERT INTO answer_value (answer_code, value) VALUES ('9011f95f-4f9c-4ab2-a03f-a9f69aa983c8', 'Not so helpful');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('b7181947-9d06-40e1-9a07-83be5c5520f7', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('d4d7557e-f262-47c0-b35b-db24d9b7801a', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', 'b7181947-9d06-40e1-9a07-83be5c5520f7');
INSERT INTO answer_value (answer_code, value) VALUES ('d4d7557e-f262-47c0-b35b-db24d9b7801a', '5');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('aa08d9d8-ada3-47b6-b030-a8b0c7e49956', '16ba2365-c03d-429a-872a-1b2c8c93986b', 'b7181947-9d06-40e1-9a07-83be5c5520f7');
INSERT INTO answer_value (answer_code, value) VALUES ('aa08d9d8-ada3-47b6-b030-a8b0c7e49956', 'Average');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('b7251b0c-d64d-4f53-8c0d-3f929fa9bec6', '9122c723-ce36-40e9-ae0a-d664b92b76d5', 'b7181947-9d06-40e1-9a07-83be5c5520f7');
INSERT INTO answer_value (answer_code, value) VALUES ('b7251b0c-d64d-4f53-8c0d-3f929fa9bec6', 'It was okey. Pretty good food.');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('43d8cbf3-f648-4953-a0cc-c3fc723e2230', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', 'b7181947-9d06-40e1-9a07-83be5c5520f7');
INSERT INTO answer_value (answer_code, value) VALUES ('43d8cbf3-f648-4953-a0cc-c3fc723e2230', 'Friends');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('bc06f1ba-a2d8-4a2d-b2d5-872b35b2d55e', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', 'b7181947-9d06-40e1-9a07-83be5c5520f7');
INSERT INTO answer_value (answer_code, value) VALUES ('bc06f1ba-a2d8-4a2d-b2d5-872b35b2d55e', 'Somewhat helpful');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('211f8daf-1201-4531-bca5-4072ac2adac2', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('05b7f8d7-8d46-480f-aa4f-985963571c0d', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', '211f8daf-1201-4531-bca5-4072ac2adac2');
INSERT INTO answer_value (answer_code, value) VALUES ('05b7f8d7-8d46-480f-aa4f-985963571c0d', '10');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('93155c3e-30c9-487b-ab9d-83c6d0c97d5e', '16ba2365-c03d-429a-872a-1b2c8c93986b', '211f8daf-1201-4531-bca5-4072ac2adac2');
INSERT INTO answer_value (answer_code, value) VALUES ('93155c3e-30c9-487b-ab9d-83c6d0c97d5e', 'Excellent');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('1f34c67c-cb94-4129-9b51-ee30cb587977', '9122c723-ce36-40e9-ae0a-d664b92b76d5', '211f8daf-1201-4531-bca5-4072ac2adac2');
INSERT INTO answer_value (answer_code, value) VALUES ('1f34c67c-cb94-4129-9b51-ee30cb587977', 'I loved it. It was most amazing event ever.');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('b49433cb-b80f-4f23-8c31-6d66d91300cf', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', '211f8daf-1201-4531-bca5-4072ac2adac2');
INSERT INTO answer_value (answer_code, value) VALUES ('b49433cb-b80f-4f23-8c31-6d66d91300cf', 'Colleagues');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('50e2a869-acc1-4b4d-8271-bf5a44bb5a08', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', '211f8daf-1201-4531-bca5-4072ac2adac2');
INSERT INTO answer_value (answer_code, value) VALUES ('50e2a869-acc1-4b4d-8271-bf5a44bb5a08', 'Very helpful');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('b1bfd08d-3014-4ccc-9cb4-60e90fca417c', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('dad6c472-f718-4b8e-a425-a547f65a4583', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', 'b1bfd08d-3014-4ccc-9cb4-60e90fca417c');
INSERT INTO answer_value (answer_code, value) VALUES ('dad6c472-f718-4b8e-a425-a547f65a4583', '10');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('4514dc3c-2c72-4b8c-83a0-f6604b977e34', '16ba2365-c03d-429a-872a-1b2c8c93986b', 'b1bfd08d-3014-4ccc-9cb4-60e90fca417c');
INSERT INTO answer_value (answer_code, value) VALUES ('4514dc3c-2c72-4b8c-83a0-f6604b977e34', 'Excellent');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('d0ee2732-71cb-4bb1-96e8-f2ba5e5d1e7d', '9122c723-ce36-40e9-ae0a-d664b92b76d5', 'b1bfd08d-3014-4ccc-9cb4-60e90fca417c');
INSERT INTO answer_value (answer_code, value) VALUES ('d0ee2732-71cb-4bb1-96e8-f2ba5e5d1e7d', 'Most amazing event I have ever saw. I loved food so much.');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('b4e97a66-30f1-4c22-bdd5-0d002fc4133c', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', 'b1bfd08d-3014-4ccc-9cb4-60e90fca417c');
INSERT INTO answer_value (answer_code, value) VALUES ('b4e97a66-30f1-4c22-bdd5-0d002fc4133c', 'Colleagues');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('72b0f42d-533e-40b0-8a5b-0f341bfb9d8c', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', 'b1bfd08d-3014-4ccc-9cb4-60e90fca417c');
INSERT INTO answer_value (answer_code, value) VALUES ('72b0f42d-533e-40b0-8a5b-0f341bfb9d8c', 'Very helpful');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('733d8b16-b415-43b1-99a5-da68fd25b2f1', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('2365bb11-4358-41ef-a843-6239e283012a', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', '733d8b16-b415-43b1-99a5-da68fd25b2f1');
INSERT INTO answer_value (answer_code, value) VALUES ('2365bb11-4358-41ef-a843-6239e283012a', '10');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('3ecb2b1d-2ba7-45ee-afd4-c158e060df4a', '16ba2365-c03d-429a-872a-1b2c8c93986b', '733d8b16-b415-43b1-99a5-da68fd25b2f1');
INSERT INTO answer_value (answer_code, value) VALUES ('3ecb2b1d-2ba7-45ee-afd4-c158e060df4a', 'Excellent');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('90c349dd-442d-46e1-aac3-4ae29c67bf0e', '9122c723-ce36-40e9-ae0a-d664b92b76d5', '733d8b16-b415-43b1-99a5-da68fd25b2f1');
INSERT INTO answer_value (answer_code, value) VALUES ('90c349dd-442d-46e1-aac3-4ae29c67bf0e', 'I loved food and staff was very lovely also');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('52dc40e9-f4f9-434e-b889-a25f30c36b5f', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', '733d8b16-b415-43b1-99a5-da68fd25b2f1');
INSERT INTO answer_value (answer_code, value) VALUES ('52dc40e9-f4f9-434e-b889-a25f30c36b5f', 'Colleagues');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('3a565338-4770-486a-96fe-7432a6e5e2bd', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', '733d8b16-b415-43b1-99a5-da68fd25b2f1');
INSERT INTO answer_value (answer_code, value) VALUES ('3a565338-4770-486a-96fe-7432a6e5e2bd', 'Very helpful');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('18c2e564-0172-4d1e-ae63-c5d5f2b7666f', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('18c2e564-0172-4d1e-ae63-c5d5f2b7666f', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', '18c2e564-0172-4d1e-ae63-c5d5f2b7666f');
INSERT INTO answer_value (answer_code, value) VALUES ('18c2e564-0172-4d1e-ae63-c5d5f2b7666f', '9');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('2cc48b18-66fe-45f8-bad4-34afa119a19c', '16ba2365-c03d-429a-872a-1b2c8c93986b', '18c2e564-0172-4d1e-ae63-c5d5f2b7666f');
INSERT INTO answer_value (answer_code, value) VALUES ('2cc48b18-66fe-45f8-bad4-34afa119a19c', 'Very Good');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('6fee1634-33ac-4bc4-9cc2-140fb15739a6', '9122c723-ce36-40e9-ae0a-d664b92b76d5', '18c2e564-0172-4d1e-ae63-c5d5f2b7666f');
INSERT INTO answer_value (answer_code, value) VALUES ('6fee1634-33ac-4bc4-9cc2-140fb15739a6', 'Very good and great time');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('3ace0cba-dbc8-41ce-8740-c08645f241f0', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', '18c2e564-0172-4d1e-ae63-c5d5f2b7666f');
INSERT INTO answer_value (answer_code, value) VALUES ('3ace0cba-dbc8-41ce-8740-c08645f241f0', 'Colleagues');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('31c7d74e-9b8d-4857-acc4-cf9b0ac77836', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', '18c2e564-0172-4d1e-ae63-c5d5f2b7666f');
INSERT INTO answer_value (answer_code, value) VALUES ('31c7d74e-9b8d-4857-acc4-cf9b0ac77836', 'Very helpful');

INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89', null, true, '2001-09-11', '561a630a-c602-4339-8cd7-b85609220ae6');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('cdfe3760-13d1-45aa-9bbd-36bf97cad1aa', 'd19e1be0-2dd5-4227-b73a-925da6db1ed2', 'ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89');
INSERT INTO answer_value (answer_code, value) VALUES ('cdfe3760-13d1-45aa-9bbd-36bf97cad1aa', '8');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('881d0980-15a5-4e6a-a45c-b8fc40282a1a', '16ba2365-c03d-429a-872a-1b2c8c93986b', 'ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89');
INSERT INTO answer_value (answer_code, value) VALUES ('881d0980-15a5-4e6a-a45c-b8fc40282a1a', 'Very Good');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('6df94305-b792-4ed4-9fb7-6e2341f4da4d', '9122c723-ce36-40e9-ae0a-d664b92b76d5', 'ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89');
INSERT INTO answer_value (answer_code, value) VALUES ('6df94305-b792-4ed4-9fb7-6e2341f4da4d', 'A lot of great activities to spend time. Awsome evening event');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('e5006d0f-78d4-412e-89a0-d772428a9cbc', 'c075e21a-e3af-4175-a25a-3dfbffd69d05', 'ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89');
INSERT INTO answer_value (answer_code, value) VALUES ('e5006d0f-78d4-412e-89a0-d772428a9cbc', 'Other');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('da2ef69d-d290-4398-9ba3-de36d5bcb2e1', '829ab804-22a0-4d24-b2ca-5d953d691995', 'ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89');
INSERT INTO answer_value (answer_code, value) VALUES ('da2ef69d-d290-4398-9ba3-de36d5bcb2e1', 'Poster');
INSERT INTO answer (code, question_code, answer_collection_code) VALUES ('e1c82c48-395c-4ddf-9c6d-a0648ed6ffe1', 'f614a5e8-0de0-4bbe-9856-7a74bf22c697', 'ec552cbc-c64c-4ad9-a1fc-baa2f86fcb89');
INSERT INTO answer_value (answer_code, value) VALUES ('e1c82c48-395c-4ddf-9c6d-a0648ed6ffe1', 'Very helpful');

-- SURVEY 2
INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('f7f68c4d-1004-4422-b439-247bddf9fda9', false, 1, 'Fruits', 1);

INSERT INTO page (code, label, survey_code) VALUES ('081c42f6-32cd-4d91-a2dc-d679712ce27c', 'page #1', 'f7f68c4d-1004-4422-b439-247bddf9fda9');
INSERT INTO question (code, label, required, page_code) VALUES ('2e8fdbfa-07e0-431f-a6f8-3f6ca0265805', 'How do you feel about apples', true, '081c42f6-32cd-4d91-a2dc-d679712ce27c');
INSERT INTO text (code) VALUES ('2e8fdbfa-07e0-431f-a6f8-3f6ca0265805');
INSERT INTO question (code, label, required, page_code) VALUES ('e85a6b98-83a2-48fd-8dde-328349c4b128', 'How many apples do eat a week', true, '081c42f6-32cd-4d91-a2dc-d679712ce27c');
INSERT INTO range (code, start, end) VALUES ('e85a6b98-83a2-48fd-8dde-328349c4b128', 1, 5);

INSERT INTO page (code, label, survey_code) VALUES ('5532bedb-515e-467c-b27a-fb97706ddf27', 'page #2', 'f7f68c4d-1004-4422-b439-247bddf9fda9');
INSERT INTO question (code, label, required, page_code) VALUES ('3add3d55-a09a-4114-b69d-3b6ab1d6d709', 'What is your favourite fruit', true, '5532bedb-515e-467c-b27a-fb97706ddf27');
INSERT INTO selection (code, multiple) VALUES ('3add3d55-a09a-4114-b69d-3b6ab1d6d709', false);
INSERT INTO selection_option (code, value, selection_code) VALUES ('b31bea1d-ddd5-4bfd-a2e2-0d57d8335bbe', 'Apple', '3add3d55-a09a-4114-b69d-3b6ab1d6d709');
INSERT INTO selection_option (code, value, selection_code) VALUES ('afde7175-64ed-4677-ad7e-abbb9204fbc8', 'Banana', '3add3d55-a09a-4114-b69d-3b6ab1d6d709');
INSERT INTO selection_option (code, value, selection_code) VALUES ('153adf3c-64dc-42b5-8935-ac91c72c8ef6', 'Pear', '3add3d55-a09a-4114-b69d-3b6ab1d6d709');

----OTHER SURVEYS

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('4dca60f4-85b4-4c4e-8ea5-3331d35f8b50', false, 2, 'Vegetables', 1);
INSERT INTO answer_collection (code, user_id, finished, submission_date, survey_code) VALUES ('09a4e8aa-4919-4f13-88ea-91cf80ef4eee', 1, true, '2001-09-11', '4dca60f4-85b4-4c4e-8ea5-3331d35f8b50');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('3def1c0e-0278-493e-9abf-7865e173e7d5', false, 2, 'Sport', 1);
INSERT INTO page (code, label, survey_code) VALUES ('ab98663a-728d-4da3-bebd-25b0a0601fb6', 'page #1', '3def1c0e-0278-493e-9abf-7865e173e7d5');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('229c1dfe-01d0-41d7-bd1a-0c6bce17b6dc', false, 2, 'Lunch on thursday', 1);
INSERT INTO page (code, label, survey_code) VALUES ('019c8dff-3b99-45ba-b06e-8ab56b11d2ea', 'page #1', '229c1dfe-01d0-41d7-bd1a-0c6bce17b6dc');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('55b2a41f-076f-4feb-a0d3-4f3a0ec151a7', false, 2, 'Event on Christmas', 1);
INSERT INTO page (code, label, survey_code) VALUES ('02ec9bde-c030-4f03-a423-b6deef088dcd', 'page #1', '55b2a41f-076f-4feb-a0d3-4f3a0ec151a7');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('9ebb7382-7529-409d-bf29-a1ce117151fb', false, 2, 'Seminar', 1);
INSERT INTO page (code, label, survey_code) VALUES ('2869f271-bc9b-426d-8a9e-c850866e5ca8', 'page #1', '9ebb7382-7529-409d-bf29-a1ce117151fb');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('846203cf-d4bc-4551-b19f-89e4e2376da4', false, 2, 'Quality of workplace', 1);
INSERT INTO page (code, label, survey_code) VALUES ('087813e5-a76b-476b-8bdd-d34d0a1feb61', 'page #1', '846203cf-d4bc-4551-b19f-89e4e2376da4');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('6f0dfcb8-d153-4e9d-881f-62ad1da24e91', false, 2, 'Falimy', 1);
INSERT INTO page (code, label, survey_code) VALUES ('752f3084-12f2-44b0-845b-0bfda09ff5a5', 'page #1', '6f0dfcb8-d153-4e9d-881f-62ad1da24e91');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('a3a31ece-5bfd-45d9-b9da-966f91e7346f', false, 2, 'Improvements in the office', 1);
INSERT INTO page (code, label, survey_code) VALUES ('d076417a-6a50-44ca-9134-642894a6c94b', 'page #1', 'a3a31ece-5bfd-45d9-b9da-966f91e7346f');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('bb3c261e-b305-432c-bb2c-6c8a36446fe6', false, 2, 'Some gossips', 1);
INSERT INTO page (code, label, survey_code) VALUES ('0c3ec8ce-cc7f-41cf-b976-8591a84cac59', 'page #1', 'bb3c261e-b305-432c-bb2c-6c8a36446fe6');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('98e5b5b0-8b5c-4622-a817-b1477c9039b0', false, 2, 'Hotel Feedback', 1);
INSERT INTO page (code, label, survey_code) VALUES ('7fec3f2f-8aa3-4d5e-a62f-6c55e2a9de9c', 'page #1', '98e5b5b0-8b5c-4622-a817-b1477c9039b0');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('f1849ab0-1bca-4c5f-bd11-48cc290f1e30', false, 2, 'Smoking survey', 1);
INSERT INTO page (code, label, survey_code) VALUES ('ae5b53ff-efd3-48d3-830c-da83a1fd09ab', 'page #1', 'f1849ab0-1bca-4c5f-bd11-48cc290f1e30');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('10568921-ea5a-4725-b1d8-23bf1b5d6fcb', false, 2, 'Parental support', 1);
INSERT INTO page (code, label, survey_code) VALUES ('3d33b7d5-e137-4f06-8f5a-e768d462cc63', 'page #1', '10568921-ea5a-4725-b1d8-23bf1b5d6fcb');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('057ecc2f-375e-4156-b37f-5a9f644e327b', false, 2, 'Vacation travel system feedback', 1);
INSERT INTO page (code, label, survey_code) VALUES ('d1f6c5ad-a186-49f6-a3bd-d0b692ee6350', 'page #1', '057ecc2f-375e-4156-b37f-5a9f644e327b');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('15cc3dd3-92ed-4b64-9883-92ca433b5063', false, 2, 'Website feedback', 1);
INSERT INTO page (code, label, survey_code) VALUES ('4fef81cc-1f49-4ee2-958e-ac28ae168d51', 'page #1', '15cc3dd3-92ed-4b64-9883-92ca433b5063');

INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('ba0d0184-4a01-432d-b456-f114e107001c', false, 2, 'Volunteering', 1);
INSERT INTO page (code, label, survey_code) VALUES ('805040ec-51fd-4d4d-b3e8-9c837a2ad293', 'page #1', 'ba0d0184-4a01-432d-b456-f114e107001c');
--
-- INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('29d9a56c-51fb-49d3-b4ce-339775aaf365', false, 2, 'Brand Awareness Survey', 1);
--
-- INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('4eca1303-b8c3-4c6b-bc71-2ffdf07640ea', false, 2, 'Music Listening Survey', 1);
-- INSERT INTO survey (code, is_public, user_id, label, version) VALUES ('880d4afc-4bb0-46bf-a093-928e17ee8b54', false, 2, 'Online Social Networking Survey', 1);

