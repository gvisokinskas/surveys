package com.creative.button.surveys.application.controllers;

import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@RestController
@PreAuthorize("hasRole('USER')")
@RequestMapping("excel")
public class ExcelController {

    @Autowired
    private ExcelService excelService;

    @GetMapping(path = "{surveyCode}")
    public ResponseEntity<Resource> exportSurvey(@PathVariable("surveyCode") String surveyCode) throws IOException {
        InputStream is = excelService.export(surveyCode);

        return ResponseEntity
                .ok()
                .contentLength(is.available())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(is));
    }

    @PostMapping
    public ResponseEntity uploadSurvey(MultipartFile excel, @AuthenticationPrincipal JwtUser jwtUser) throws IOException {
        excelService.importSurvey(excel.getInputStream(), jwtUser);
        return ResponseEntity.ok().build();
    }
}
