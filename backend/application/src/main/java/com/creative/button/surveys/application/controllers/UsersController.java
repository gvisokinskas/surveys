package com.creative.button.surveys.application.controllers;

import com.creative.button.surveys.service.services.api.UsersService;
import com.creative.button.surveys.service.dto.generic.RegisteredUserDTO;
import com.creative.button.surveys.service.dto.generic.UserRegistrationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

import static com.creative.button.surveys.application.StaticResponseMapper.mapBooleanToResponse;

@RestController
@RequestMapping("users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping("is-disabled/{username}")
    public boolean isDisabled(@PathVariable String username) {
        return usersService.isUserDisabled(username);
    }

    @PostMapping("register")
    public RegisteredUserDTO registerUser(@RequestBody @Valid UserRegistrationDTO user) {
        return usersService.registerUser(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("all")
    public Collection<RegisteredUserDTO> getAllUsers() {
        return usersService.findAllUsers();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("{username}/remove")
    public ResponseEntity removeUser(@PathVariable String username) {
        return mapBooleanToResponse(usersService.removeUser(username));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("{username}/disable")
    public ResponseEntity disableUser(@PathVariable String username) {
        return mapBooleanToResponse(usersService.disableUser(username));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("{username}/enable")
    public ResponseEntity enableUser(@PathVariable String username) {
        return mapBooleanToResponse(usersService.enableUser(username));
    }
}