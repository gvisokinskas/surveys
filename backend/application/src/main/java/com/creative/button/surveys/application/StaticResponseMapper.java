package com.creative.button.surveys.application;

import org.springframework.http.ResponseEntity;

public final class StaticResponseMapper {
    public static ResponseEntity mapBooleanToResponse(boolean value) {
        return value ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }
}
