package com.creative.button.surveys.application.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.creative.button")
@EntityScan(basePackages = "com.creative.button")
public class JpaRepositoryConfiguration {
}
