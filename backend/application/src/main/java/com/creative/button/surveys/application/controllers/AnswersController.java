package com.creative.button.surveys.application.controllers;

import com.creative.button.surveys.service.dto.answers.AnswerCollectionDTO;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("answers")
public class AnswersController {

    @Autowired
    private AnswerService answerService;

    @PostMapping
    public ResponseEntity<String> saveAnswer(@RequestBody @Valid AnswerCollectionDTO answerCollectionDTO, @AuthenticationPrincipal JwtUser jwtUser) {
        String answerCode = answerService.saveAnswer(answerCollectionDTO, jwtUser);
        return answerCode != null ? ResponseEntity.ok(answerCode) : ResponseEntity.badRequest().build();
    }

    @GetMapping("{answerCode}")
    public ResponseEntity<AnswerCollectionDTO> getIncompleteAnswer(@PathVariable("answerCode") String answerCode, @AuthenticationPrincipal JwtUser jwtUser) {
        AnswerCollectionDTO answerCollectionDTO = answerService.getIncompleteAnswer(answerCode, jwtUser);
        return answerCollectionDTO != null ? ResponseEntity.ok(answerCollectionDTO) : ResponseEntity.badRequest().build();
    }
}
