package com.creative.button.surveys.application.controllers;

import com.creative.button.surveys.service.dto.reports.SurveyReport;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("reports/{surveyCode}")
public class ReportsController {

    @Autowired
    private ReportsService reportsService;

    @PreAuthorize("hasRole('USER')")
    @GetMapping
    public ResponseEntity<SurveyReport> getSurveyReport(@PathVariable("surveyCode") String surveyCode, @AuthenticationPrincipal JwtUser jwtUser) {
        SurveyReport surveyReport = reportsService.getSurveyReport(surveyCode, jwtUser);
        return surveyReport != null ? ResponseEntity.ok(surveyReport) : ResponseEntity.badRequest().build();
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("{answerCode}")
    public ResponseEntity<SurveyReport> getSingleAnswerReport(@PathVariable("surveyCode") String surveyCode, @PathVariable("answerCode") String answerCode, @AuthenticationPrincipal JwtUser jwtUser) {
        SurveyReport singleAnswerReport = reportsService.getSingleAnswerReport(surveyCode, answerCode, jwtUser);
        return singleAnswerReport != null ? ResponseEntity.ok(singleAnswerReport) : ResponseEntity.badRequest().build();
    }
}
