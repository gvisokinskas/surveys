package com.creative.button.surveys.application.configuration;

import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class OverrideConfig  {
//    Uncomment this to get bean override
//    @Bean
//    public SurveysService surveysService() {
//        return new SurveysService() {
//            @Override
//            public SurveyDTO createSurvey(SurveyDTO survey, JwtUser jwtUser) {
//                return null;
//            }
//
//            @Override
//            public SurveyDTO getSurvey(String surveyCode, JwtUser jwtUser) {
//                return null;
//            }
//
//            @Override
//            public SurveyDTO getSurveyByAnswerCode(String answerCode) {
//                return null;
//            }
//
//            @Override
//            public List<SurveyDTO> getSurveys(JwtUser jwtUser) {
//                return null;
//            }
//
//            @Override
//            public SurveyDTO updateSurvey(SurveyDTO surveyDTO, JwtUser jwtUser) {
//                return null;
//            }
//
//            @Override
//            public boolean deleteSurvey(String surveyCode, JwtUser jwtUser) {
//                return false;
//            }
//
//            @Override
//            public boolean surveyWasUpdated(String surveyCode, Long currentVersion) {
//                return false;
//            }
//        };
//    }
}
