package com.creative.button.surveys.application.controllers;

import com.creative.button.surveys.service.services.api.AllowedEmailsService;
import com.creative.button.surveys.service.dto.generic.AllowedEmailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.creative.button.surveys.application.StaticResponseMapper.mapBooleanToResponse;

@RestController
@RequestMapping("allowed-emails")
public class AllowedEmailsController {

    @Autowired
    private AllowedEmailsService allowedEmailsService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public Collection<AllowedEmailDTO> allEmails() {
        return allowedEmailsService.getAllowedEmails();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity addEmail(@RequestBody AllowedEmailDTO email) {
        return mapBooleanToResponse(allowedEmailsService.save(email));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping
    public ResponseEntity removeEmail(@RequestBody AllowedEmailDTO email) {
        return mapBooleanToResponse(allowedEmailsService.remove(email));
    }
}
