package com.creative.button.surveys.application.configuration;

import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Component
public class NotFoundErrorResolver implements ErrorViewResolver {
    @Override
    public ModelAndView resolveErrorView(HttpServletRequest request,
                                         HttpStatus status, Map<String, Object> model) {
        if (status == HttpStatus.NOT_FOUND || (model.containsKey("status") && (int) model.get("status") == 999)) {
            ModelAndView indexView = new ModelAndView("forward:/");
            indexView.setStatus(HttpStatus.OK);
            return indexView;
        }

        return null;
    }
}