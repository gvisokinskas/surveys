package com.creative.button.surveys.application.controllers;

import com.creative.button.surveys.service.dto.surveys.SurveyDTO;
import com.creative.button.surveys.service.security.JwtUser;
import com.creative.button.surveys.service.services.api.SurveysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

import static com.creative.button.surveys.application.StaticResponseMapper.mapBooleanToResponse;

@RestController
@RequestMapping("surveys")
public class SurveysController {

    @Autowired
    private SurveysService surveysService;

    @GetMapping("{surveyCode}")
    public ResponseEntity<SurveyDTO> getSurvey(@PathVariable("surveyCode") String surveyCode, @AuthenticationPrincipal JwtUser activeUser) {
        SurveyDTO survey = surveysService.getSurvey(surveyCode, activeUser);

        if (survey == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(survey);
    }

    @GetMapping("by-answer/{answerCode}")
    public ResponseEntity<SurveyDTO> getSurveyByAnswerCode(@PathVariable("answerCode") String answerCode) {
        SurveyDTO survey = surveysService.getSurveyByAnswerCode(answerCode);

        if (survey == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(survey);
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("all")
    public Collection<SurveyDTO> all(@AuthenticationPrincipal JwtUser activeUser) {
        return surveysService.getSurveys(activeUser);
    }

    @PreAuthorize("hasRole('USER')")
    @PostMapping
    public ResponseEntity<SurveyDTO> createSurvey(@RequestBody @Valid SurveyDTO survey, @AuthenticationPrincipal JwtUser activeUser) {
        SurveyDTO created = surveysService.createSurvey(survey, activeUser);

        if (created == null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.ok(created);
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping
    public ResponseEntity<SurveyDTO> updateSurvey(@RequestBody @Valid SurveyDTO survey, @AuthenticationPrincipal JwtUser activeUser) {
        if (surveysService.surveyWasUpdated(survey.getCode(), survey.getVersion())) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).build();
        }

        SurveyDTO surveyDTO = surveysService.updateSurvey(survey, activeUser);

        if (surveyDTO != null) {
            return ResponseEntity.ok(survey);
        } else {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
    }

    @PreAuthorize("hasRole('USER')")
    @DeleteMapping("{surveyCode}")
    public ResponseEntity deleteSurvey(@PathVariable("surveyCode") String surveyCode, @AuthenticationPrincipal JwtUser activeUser) {
        return mapBooleanToResponse(surveysService.deleteSurvey(surveyCode, activeUser));
    }
}
