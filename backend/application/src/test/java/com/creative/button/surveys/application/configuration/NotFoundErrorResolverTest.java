package com.creative.button.surveys.application.configuration;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.HashMap;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class NotFoundErrorResolverTest {
    private NotFoundErrorResolver notFoundErrorResolver;

    @Before
    public void setUp() {
        this.notFoundErrorResolver = new NotFoundErrorResolver();
    }

    @Test
    public void notFoundStatusShouldBeResolvedToHomePage() {
        ModelAndView result = notFoundErrorResolver.resolveErrorView(null, HttpStatus.NOT_FOUND, new HashMap<>());

        assertThat(result, is(not(nullValue())));
        assertThat(result.getViewName(), is("forward:/"));
    }

    @Test
    public void shouldRedirectToHomeForErrorPage() {
        ModelAndView result = notFoundErrorResolver.resolveErrorView(null, HttpStatus.NOT_FOUND, Collections.singletonMap("status", 999));

        assertThat(result, is(not(nullValue())));
        assertThat(result.getViewName(), is("forward:/"));
    }

    @Test
    public void shouldNotRedirectWhenOtherErrorsOccur() {
        ModelAndView result = notFoundErrorResolver.resolveErrorView(null, HttpStatus.I_AM_A_TEAPOT, new HashMap<>());

        assertThat(result, is(nullValue()));
    }
}